
import numpy as np

import jax.numpy as jnp
import jraph
import jax.random as jrandom
import jax.tree_util as tree
import jax
import flax 
import haiku as hk
from typing import Any, Callable, Dict, List, Optional, Tuple
import optax
import os
import itertools
import matplotlib.pyplot as plt 
from jraph._src import models
import jaxopt



from jax.lib import xla_bridge
print('Using platform: ', xla_bridge.get_backend().platform)

os.environ["XLA_PYTHON_CLIENT_PREALLOCATE"]="false"
os.environ["XLA_PYTHON_CLIENT_MEM_FRACTION"]=".90"
os.environ["XLA_PYTHON_CLIENT_ALLOCATOR"]="platform"


arrays_GAT = np.load('arrays_layer_nhit.npy', allow_pickle = True)

X_nhit = arrays_GAT[0]  #96493
Y_nhit = arrays_GAT[1]*1e2
Z_nhit = arrays_GAT[2]
ID_nhit = arrays_GAT[3]

arrays_MLP = np.load('arrays_layer_nhit_internal.npy', allow_pickle = True)
X_tot = arrays_MLP[0]
Y_tot = arrays_MLP[1]
Z_tot = arrays_MLP[2]
ID_tot = arrays_MLP[3]

start_training = 0
end_training= 80
batch_size = 16
steps = 150

strip_error = 0.0005 / jnp.sqrt(12)

arrays= np.asarray((X_nhit[start_training:end_training], Y_nhit[start_training:end_training], Z_nhit[start_training:end_training], ID_nhit[start_training:end_training]))
arrays_inner = np.asarray((X_tot[start_training:end_training], Y_tot[start_training:end_training], Z_tot[start_training:end_training], ID_tot[start_training:end_training]))

GAT_layers = [2.495 , 2.505, 2.605, 2.705, 2.805, 4.205 , 4.305 ,4.405, 4.505]
MLP_layers = [0.405, 0.505, 0.605, 0.705, 0.805, 0.905, 1.005, 1.105]
outer_layers = [0.405, 4.505]

print('Events for training: ', arrays.shape)

## functions for graph creation ##

#function that compute the node number (hit number) for each graph (event) in the batch
def get_node_number(arrays, batch_size):
    node_numbers = []
    x = arrays[0]
    
    for i in range(batch_size):
        
        xx = x[i]
        node_numbers.append([xx.shape[0]])
    max_value = np.max(node_numbers)
    #max_value = 384
    
    return node_numbers, max_value

#function that returns the n_nodes and n_edges vectors required for defining the GraphTuple
#n_nodes vector is like ([a],[b],[c]) with a,b,c nodes numbers for each graph in the batch
#n_edges vector is like ([a**2], [b**2], [c**2]) where each element is the number of edges for the graph (fully connected)

def get_nodes_edges_per_event(arrays, batch_size):
    data_array = get_node_number(arrays, batch_size)[0]

    hits_per_event = data_array
    edges_per_event = [[nhits[0]**2] for nhits in data_array]
    return hits_per_event, edges_per_event


#function that creates for each graoh in the batch the vectors defining senders and receivers
#explicitly batched graph is used so all the vectors are padded to the dimension of the bigger one 

def create_senders_receivers(arrays, batch_size):
    #nhits = get_nodes_edges_per_event(arrays, batch_size)[0]
    nhits = get_nodes_edges_per_event(arrays, batch_size)[0]
    senders = []
    receivers = []
    for nhit in nhits:
        n = nhit[0]
        s = jnp.tile(np.arange(n), n).tolist()
        r = jnp.repeat(np.arange(n), n).tolist()
        
        senders.append(s)
        receivers.append(r)

    padded_senders = []
    padded_receivers = []
    #max_value = 384
    max_value = get_node_number(arrays, batch_size)[1]

    for sender in senders:
        pad_s = sender + [-1] * (max_value**2 - len(sender))
        padded_senders.append(pad_s)

    for receiver in receivers:
        pad_r = receiver + [-1] * (max_value**2 - len(receiver))
        padded_receivers.append(pad_r)

    return padded_senders, padded_receivers



#function that decorates the target nodes with only the particle id (muon == -13.)
def decorate_nodes_truth(arrays, batch_size):
    nodes = []
    #max_value = 384
    max_value = get_node_number(arrays, batch_size)[1]    
    
    for i in range (0, batch_size):        
        id = np.asarray(arrays[3][i])
        id = jnp.where(jnp.absolute(id) == 13, 1, 0)
        padded_id =  jnp.pad(id, (0, max_value - len(id)), mode='constant')
        node_features = jnp.stack((padded_id), axis=-1)
        nodes.append(node_features)

    return nodes

#function that decorates the input nodes with (y,z) coordinates
def decorate_nodes(arrays,batch_size):
    nodes = []
    max_value = get_node_number(arrays, batch_size)[1]   
    #max_value = 384 
    for i in range (0, batch_size):
        y = np.asarray(arrays[1][i])
        z = np.asarray(arrays[2][i])
        padded_y =  jnp.pad(y, (0, max_value - len(y)), mode='constant')
        padded_z =  jnp.pad(z, (0, max_value - len(z)), mode='constant')
      
        node_features = jnp.stack((padded_y,padded_z), axis=-1)
        nodes.append(node_features)

    return nodes

def GetGraphs(arrays: np.ndarray, batch_size : int) -> jraph.GraphsTuple:
    graph = jraph.GraphsTuple(
        n_node=jnp.array(get_nodes_edges_per_event(arrays, batch_size)[0]), 
        n_edge=jnp.array(get_nodes_edges_per_event(arrays, batch_size)[1]), 
        nodes=jnp.array(decorate_nodes(arrays, batch_size)), 
        edges=None, 
        globals=None,  
        senders=jnp.array(create_senders_receivers(arrays, batch_size)[0]), 
        receivers=jnp.array(create_senders_receivers(arrays, batch_size)[1]))
    
    graph_truth = jraph.GraphsTuple(
        n_node=jnp.array(get_nodes_edges_per_event(arrays, batch_size)[0]), 
        n_edge=jnp.array(get_nodes_edges_per_event(arrays, batch_size)[1]), 
        nodes=jnp.array(decorate_nodes_truth(arrays, batch_size)), 
        edges=None, 
        globals=None,  
        senders=jnp.array(create_senders_receivers(arrays, batch_size)[0]), 
        receivers=jnp.array(create_senders_receivers(arrays, batch_size)[1]))
    
    return (graph, graph_truth)


## network definition ##

from jraph._src import utils

def add_self_edges_fn(receivers: jnp.ndarray, senders: jnp.ndarray,
                      total_num_nodes: int) -> Tuple[jnp.ndarray, jnp.ndarray]:
  """Adds self edges. Assumes self edges are not in the graph yet."""
  receivers = jnp.concatenate((receivers, jnp.arange(total_num_nodes)), axis=0)
  senders = jnp.concatenate((senders, jnp.arange(total_num_nodes)), axis=0)
  return receivers, senders

#################
# GAT implementation adapted from https://github.com/deepmind/jraph/blob/master/jraph/_src/models.py#L442.
def GAT(attention_query_fn: Callable,
        attention_logit_fn: Callable,
        node_update_fn: Optional[Callable] = None,
        add_self_edges: bool = True) -> Callable:
  
  # pylint: disable=g-long-lambda
  if node_update_fn is None:
    # By default, apply the leaky relu and then concatenate the heads on the
    # feature axis.
    node_update_fn = lambda x: jnp.reshape(
        jax.nn.leaky_relu(x), (x.shape[0], -1))

  def _ApplyGAT(graph: jraph.GraphsTuple) -> jraph.GraphsTuple:
    """Applies a Graph Attention layer."""
    nodes, edges, receivers, senders, _, _, _ = graph
    # Equivalent to the sum of n_node, but statically known.
    try:
      sum_n_node = nodes.shape[0]
    except IndexError:
      raise IndexError('GAT requires node features')

    # Pass nodes through the attention query function to transform
    # node features, e.g. with an MLP.
    nodes = attention_query_fn(nodes)

    total_num_nodes = tree.tree_leaves(nodes)[0].shape[0]
    if add_self_edges:
      # We add self edges to the senders and receivers so that each node
      # includes itself in aggregation.
      receivers, senders = add_self_edges_fn(receivers, senders,
                                             total_num_nodes)

    # We compute the softmax logits using a function that takes the
    # embedded sender and receiver attributes.
    sent_attributes = nodes[senders]
    received_attributes = nodes[receivers]
    att_softmax_logits = attention_logit_fn(sent_attributes,
                                            received_attributes, edges)

    # Compute the attention softmax weights on the entire tree.
    att_weights = jraph.segment_softmax(
        att_softmax_logits, segment_ids=receivers, num_segments=sum_n_node)

    # Apply attention weights.
    messages = sent_attributes * att_weights
    # Aggregate messages to nodes.
    nodes = jax.ops.segment_sum(messages, receivers, num_segments=sum_n_node)

    # Apply an update function to the aggregated messages.
    nodes = node_update_fn(nodes)

    return graph._replace(nodes=nodes)

  # pylint: enable=g-long-lambda
  return _ApplyGAT



def gat_definition(graph_outer: jraph.GraphsTuple, graph_inner: jraph.GraphsTuple) -> (jraph.GraphsTuple, jraph.GraphsTuple):

  def _attention_query_fn1(node_features):
        return hk.nets.MLP([4, 8, 16, 32, 64, 128, 256, 512])(node_features)
  
  def _attention_logit_fn1(senders, receivers, edges):
        del edges
        feat = jnp.concatenate((senders, receivers), axis=-1)
        return hk.nets.MLP([512])(jax.nn.leaky_relu(hk.nets.MLP([512, 256, 128])(feat)))

  gn = GAT(
      attention_query_fn=_attention_query_fn1,
      attention_logit_fn=_attention_logit_fn1,
      node_update_fn=hk.nets.MLP([512, 256, 128]),
      add_self_edges=True)
  
  graph_outer = gn(graph_outer)

  def _attention_query_fn2(node_features):
        return hk.nets.MLP([128, 256, 512])(node_features)
  
  def _attention_logit_fn2(senders, receivers, edges):
        del edges
        feat = jnp.concatenate((senders, receivers), axis=-1)
        return hk.nets.MLP([128, 64, 32, 16, 8, 4, 2, 1])(jax.nn.leaky_relu(hk.nets.MLP([512, 256,  128])(feat)))


  gn = GAT(
      attention_query_fn=_attention_query_fn2,
      attention_logit_fn=_attention_logit_fn2,
      node_update_fn=hk.nets.MLP([512, 256, 128, 64, 32, 16, 8, 4, 2, 1]),
      add_self_edges=True)
  
  graph_outer = gn(graph_outer)

  def _attention_query_fn3(node_features):
        return hk.nets.MLP([3, 6, 12, 24, 48, 128, 256])(node_features)
  
  def _attention_logit_fn3(senders, receivers, edges):
        del edges
        feat = jnp.concatenate((senders, receivers), axis=-1)
        return hk.nets.MLP([64, 32, 16, 8, 4, 2, 1])(jax.nn.leaky_relu(hk.nets.MLP([256, 128, 64])(feat)))

  gn = GAT(
      attention_query_fn=_attention_query_fn3,
      attention_logit_fn=_attention_logit_fn3,
      node_update_fn=hk.nets.MLP([256, 128, 64, 32, 32, 16, 8, 4, 2, 1]),
      add_self_edges=True)
  graph_inner = gn(graph_inner)
 
  return graph_outer, graph_inner

#################
print('Graph definition')


def build_toy_graph(batch, n) -> jraph.GraphsTuple:
    """Define a four node graph, each node has a scalar as its feature."""

    # create new node features vector with 40 elements all with dimension 3 (x,y,z) = (1,1,1)
    node_features = jnp.ones((batch, n, 3))

    # define senders as a list of integer numbers between 0 and n-1 repeated batch times
    senders = jnp.tile(jnp.arange(n), (batch, 1))

    # new receivers vector with 40 elements with ordered numbers between 0 and 39
    receivers = jnp.tile(jnp.arange(n), (batch, 1))

    edges = None

    n_node = jnp.array([n]) * jnp.ones((batch, 1))
    n_edge = jnp.array([n]) * jnp.ones((batch, 1))

    # Optionally you can add `global` information, such as a graph label.
    global_context = None  # Same feature dims as nodes and edges.
    graph = jraph.GraphsTuple(
            nodes=node_features,
            edges=edges,
            senders=senders,
            receivers=receivers,
            n_node=n_node,
            n_edge=n_edge,
            globals=global_context
    )
    return graph

dummy_graph = build_toy_graph(batch_size, 40)
print('Graph defined !')

graph = GetGraphs(arrays, batch_size)[0]
graph_truth = GetGraphs(arrays,batch_size)[1]

def DataLoader(arrays, batch_size, *, key):
    dataset_size = arrays[0].shape[0]
    #print('n_events: ', dataset_size, ', batch_size: ', batch_size)
        
    (key,) = jrandom.split(key, 1)
    start = 0
    end = batch_size
    while end <= dataset_size:
        
        yield tuple(GetGraphs(arrays[:,start:end], batch_size))
        start = end
        end = start + batch_size

## network inizialization ##
print('Network inizialization') 
network = hk.without_apply_rng(hk.transform(hk.vmap(gat_definition, split_rng=False)))
params = network.init(jax.random.PRNGKey(42), graph, dummy_graph)
print('Network inizialized ! ') 


# Define your variable learning rate function

opt_init, opt_update = optax.adam(1e-2)
opt_state = opt_init(params)



def shifted_sigmoid(x):
    return 1 / (1 + jnp.exp(-100 * (x - 0.5)))

def cluster_hits(input_graph, output_graph):
    coordinates = input_graph.nodes
    predictions = output_graph.nodes
    
    y_input = coordinates[:,0]
    z_input = coordinates[:,1]
    #all_weights = jnp.absolute(predictions).ravel() 
    all_weights = shifted_sigmoid(jnp.absolute(predictions)).ravel()
    real_weights = jnp.where(y_input != 5., all_weights, 0)
    
    layers = [2.495 , 2.505, 2.605, 2.705, 2.805, 4.205 , 4.305 ,4.405, 4.505]    
    clustered_y = []
    weights_layer_sum = []
    rms_y_layers = []

    y_input = y_input/1e2
    for i in range(len(layers)):

      weights_layer = jnp.where(z_input == layers[i], real_weights, 0)    
      y = jnp.where(z_input==layers[i], y_input, 0) 
      weights_layer = jnp.where(weights_layer == 1., 1, 0)
      weighted_y = weights_layer*y  

      sum_w = jnp.sum(weights_layer)        
      y_mean = jnp.sum(weighted_y) / sum_w

      clustered_y.append(y_mean)
      weights_layer_sum.append(sum_w)
      layer_rms = jnp.sqrt(jnp.sum((weights_layer*((y - y_mean)))**2) / jnp.sum(weights_layer))   
      layer_rms = jnp.where(jnp.nan_to_num(layer_rms) != 0, layer_rms, 1)
      rms_y_layers.append(layer_rms)
      
    rms_y_layers = jnp.asarray(rms_y_layers)
    return clustered_y, layers, weights_layer_sum, rms_y_layers

@jax.jit
def circle_residuals_jax(params, x, y, w, err):
    """Compute residuals for circle fitting using JAX."""
    A, D, theta = params
    err = strip_error* jnp.ones_like(x)
    P = A * (x**2 + y**2) + jnp.sqrt((1 + 4*A*D)) * (x * jnp.cos(theta) + y * jnp.sin(theta)) + D
       
    residuals = w * (2*P/(1+jnp.sqrt(1+4*A*P)))
    return jnp.sum(w*(residuals/err)**2)
@jax.jit
def fit_circle_least_squares_jax(x, y, w, err, initial_params=None):
    """Fit a circle to points using JAX optimization."""
  

    if initial_params is None:
        # Use the centroid and average distance as initial parameters
        x0 = jnp.mean(x)
        y0 = jnp.mean(y)
        r = jnp.mean(jnp.sqrt((x - x0)**2 + (y - y0)**2))
        a0 = 1 / 2*r
        d0 = 0
        theta0 = 0 
        initial_params = jnp.array([a0, d0 ,theta0])

    # Define the cost function without calling it
    err = strip_error* jnp.ones_like(x)
    cost_function = lambda params: circle_residuals_jax(params, x, y, w, err)

    # Use JAX's minimize function with the L-BFGS-B optimizer
    solver = jaxopt.LBFGS(fun=cost_function, verbose=False)
    res = solver.run(initial_params)

    # Extract optimized parameters
    optimized_params = res.params

    # Print the value of the cost function with optimized parameters
    #print("Value of the cost function:", circle_residuals_jax(optimized_params, x, y, w, err))
    chisq = circle_residuals_jax(optimized_params, x, y, w, err)
    return optimized_params, chisq

def final_params(r):
  A3 = r[0]
  D3 = r[1]
  theta3 = r[2]

  B3 = jnp.sqrt(jnp.absolute(1+4*A3*D3))*jnp.cos(theta3)
  C3 = jnp.sqrt(jnp.absolute(1+4*A3*D3))*jnp.sin(theta3)

  fitted_xcenter = -B3/(2*A3)
  fitted_ycenter = -C3/(2*A3)
  fitted_radius = 1/(2*jnp.abs(A3))
  return fitted_xcenter, fitted_ycenter, fitted_radius


def inner_input_preprocessing(MLP_arrays, batch_size):
    coordinates = []
    labels = []

    max = get_node_number(MLP_arrays, batch_size)[1]

    for i in range (0, batch_size):
        
        y = jnp.asarray(MLP_arrays[1][i])
        z = jnp.asarray(MLP_arrays[2][i])
        id = jnp.asarray(MLP_arrays[3][i])
        id = jnp.where(jnp.absolute(id) == 13, 1, 0)

        pad_y =  jnp.pad(y, (0, max - len(y)), mode='constant')
        pad_z =  jnp.pad(z, (0, max - len(z)), mode='constant')
      
        padded_y = jnp.where(pad_y!=0, pad_y, 5)
        padded_z = jnp.where(pad_y!=0, pad_z, 5)
        padded_id =  jnp.pad(id, (0, max - len(id)), mode='constant')

        #funzioni per clustering, estrapolazione 
        input_coords = jnp.stack((padded_y,padded_z), axis=-1)
        truth_labels = jnp.stack((padded_id), axis=-1) 

        coordinates.append(input_coords)
        labels.append(truth_labels)

    return jnp.asarray(coordinates), jnp.asarray(labels)

@jax.jit
def cartesian_to_polar_GAT(y_cluster, fitted_xcenter, fitted_ycenter):
    x = jnp.asarray(y_cluster)
    y = jnp.asarray(GAT_layers)
    dx = x - fitted_xcenter
    dy = y - fitted_ycenter

    theta = jnp.nan_to_num(jnp.arctan2(dy, dx))   
    theta_ref = jnp.arctan2(fitted_xcenter, fitted_ycenter)
    theta = jnp.where(theta < theta_ref, theta + 2*jnp.pi, theta)

    return jnp.sum(theta)/jnp.count_nonzero(theta), jnp.arctan2(dy, dx)

@jax.jit
def find_nearest_y_points(z_layer, y_layer, y_hits_layer, z_layers_hits, labels):
    num_points=5
    selected_y = []
    selected_z = []
    selected_labels = []
    selected_y_distances = []
    for i in range(len(z_layer)):
        z = z_layer[i]
        y = y_layer[i]   
        y_layer_hits = jnp.where((z_layers_hits == z), y_hits_layer, 5)
        y_diffs = y_layer_hits - y
        abs_y_diffs = jnp.absolute(y_diffs)
        sorted_indices = jnp.argsort(abs_y_diffs)

        sorted_array = y_layer_hits[sorted_indices]
        sorted_distances = y_diffs[sorted_indices]            
        
        sorted_label = labels[sorted_indices]

        selected_y_values = sorted_array[:num_points]
        selected_label = sorted_label[:num_points]      
        selected_z_values = z * jnp.ones_like(selected_y_values)
        selected_y_diffs = sorted_distances[:num_points]
       
        
        selected_y.append(selected_y_values)
        selected_z.append(selected_z_values)
        selected_labels.append(selected_label)
        selected_y_distances.append(selected_y_diffs)  
   

    return selected_y, selected_z, selected_labels, selected_y_distances

@jax.jit
def from_fit_to_mlp_input(MLP_coordinates, MLP_labels, fitted_xcenter, fitted_ycenter, fitted_radius, y_cluster):
    theta_mean, theta = cartesian_to_polar_GAT(y_cluster, fitted_xcenter, fitted_ycenter)
    extrapolated_theta_MLP_layers = jnp.arcsin((jnp.asarray(MLP_layers) - fitted_ycenter[:,jnp.newaxis]) / fitted_radius[:,jnp.newaxis])
    mean_extrap_theta_MLP_layers = jnp.mean(extrapolated_theta_MLP_layers, axis=1)
    theta_outer_layers = jnp.arcsin((jnp.asarray(outer_layers) - fitted_ycenter[:,jnp.newaxis]) / fitted_radius[:,jnp.newaxis])
    delta_theta_outer_layers = jnp.absolute(theta_outer_layers[:,0] - theta_outer_layers[:,1])
    good_happy_theta_inner_layers = jnp.where((jnp.absolute(mean_extrap_theta_MLP_layers - theta_mean) < 2 * delta_theta_outer_layers)[:,jnp.newaxis], extrapolated_theta_MLP_layers, jnp.pi - extrapolated_theta_MLP_layers)
    extrapolated_y_MLP_fromtheta = fitted_xcenter + fitted_radius * jnp.sin(good_happy_theta_inner_layers)
    # fitted_xcenter[:,jnp.newaxis] + fitted_radius[:,jnp.newaxis] * jnp.cos(good_happy_theta_inner_layers)  
    MLP_layers_vec = jnp.asarray(MLP_layers) #jnp.tile(jnp.asarray(MLP_layers), (batch_size, 1))
    y_mlp_input = MLP_coordinates[:,0]
    z_mlp_input = MLP_coordinates[:,1]
    mlp_labels = MLP_labels
   
    ymlp, zmlp, lmlp , ydiffmlp = find_nearest_y_points(MLP_layers_vec, extrapolated_y_MLP_fromtheta.ravel(), y_mlp_input, z_mlp_input, mlp_labels)
    return ymlp, zmlp, lmlp, extrapolated_y_MLP_fromtheta, ydiffmlp


## loss definition ##




def prediction_loss(params, input_graph, target_graph, input_inner):
            
    
            output_graph, output_inner = network.apply(params, input_graph, input_inner)        
            input_mask = input_graph.nodes[:,:,0]    
            radius = jnp.reshape(output_graph.nodes, (output_graph.nodes.shape[0], output_graph.nodes.shape[1]))        
            id_mask = target_graph.nodes
            loss = (radius - id_mask)**2
            loss_realhits = jnp.where(input_mask != 5, loss, 0)  
            loss_outer = jnp.sum(loss_realhits.ravel()) / jnp.count_nonzero(loss_realhits.ravel())

            y_cluster_outer , z_cluster_outer, w_cluster_outer, error_cluster_outer = jax.vmap(cluster_hits)(input_graph, output_graph)
            y_cluster_outer = jnp.array(y_cluster_outer).T
            z_cluster_outer = jnp.array(z_cluster_outer).T
            w_cluster_outer = jnp.array(w_cluster_outer).T

            w_cluster_outer = jnp.where(w_cluster_outer >= 1, 1, 0)
            y_cluster_outer = jnp.nan_to_num(y_cluster_outer)

            outer_fit_params, chisq = jax.vmap(fit_circle_least_squares_jax)(y_cluster_outer, z_cluster_outer, w_cluster_outer, error_cluster_outer)
            xc_outer, yc_outer, r_outer = jax.vmap(final_params)(outer_fit_params)

            xc_outer = xc_outer[:,jnp.newaxis]
            yc_outer = yc_outer[:,jnp.newaxis]
            r_outer = r_outer[:,jnp.newaxis]

            inner_coordinates = inner_input_preprocessing(arrays_inner, batch_size)[0]
            inner_labels = inner_input_preprocessing(arrays_inner, batch_size)[1]

            y_inner, z_inner, label_inner, extrap_inner, deltay_inner = jax.vmap(from_fit_to_mlp_input)(inner_coordinates, inner_labels, xc_outer, yc_outer, r_outer, y_cluster_outer)
            y_inner = jnp.asarray(y_inner).transpose(1,0,2)
            z_inner = jnp.asarray(z_inner).transpose(1,0,2)
            label_inner = jnp.asarray(label_inner).transpose(1,0,2)
            extrap_inner = extrap_inner[:,:,jnp.newaxis]
            deltay_inner = jnp.asarray(deltay_inner).transpose(1,0,2)
            
            #replace to the dummy_input_graph features the stacked coordinates (y_inner, z_inner, deltay_inner)
            input_inner_graph = dummy_graph._replace(nodes = jnp.stack((y_inner, z_inner, deltay_inner), axis=-1))
            output_graph_inner = network.apply(params, input_graph, input_inner_graph)[1]
            
            radius_inner = jnp.reshape(output_graph_inner.nodes, (output_graph_inner.nodes.shape[0], output_graph_inner.nodes.shape[1] * output_graph_inner.nodes.shape[2]))
            id_mask_inner = jnp.reshape(label_inner, (label_inner.shape[0], label_inner.shape[1]*label_inner.shape[2]))
            loss_inner = (radius_inner - id_mask_inner)**2
            loss_inner = jnp.sum(loss_inner.ravel()) / jnp.count_nonzero(loss_inner.ravel())
            
            final_loss = jnp.where(step >  50, loss_outer + loss_inner, loss_outer)
            return final_loss
             
            
def prediction_loss_value(params, input_graph, target_graph, input_inner):
            
    
            output_graph, output_inner = network.apply(params, input_graph, input_inner)        
            input_mask = input_graph.nodes[:,:,0]    
            radius = jnp.reshape(output_graph.nodes, (output_graph.nodes.shape[0], output_graph.nodes.shape[1]))        
            id_mask = target_graph.nodes
            loss = (radius - id_mask)**2
            loss_realhits = jnp.where(input_mask != 5, loss, 0)  
            loss_outer = jnp.sum(loss_realhits.ravel()) / jnp.count_nonzero(loss_realhits.ravel())

            y_cluster_outer , z_cluster_outer, w_cluster_outer, error_cluster_outer = jax.vmap(cluster_hits)(input_graph, output_graph)
            y_cluster_outer = jnp.array(y_cluster_outer).T
            z_cluster_outer = jnp.array(z_cluster_outer).T
            w_cluster_outer = jnp.array(w_cluster_outer).T

            w_cluster_outer = jnp.where(w_cluster_outer >= 1, 1, 0)
            y_cluster_outer = jnp.nan_to_num(y_cluster_outer)

            outer_fit_params, chisq = jax.vmap(fit_circle_least_squares_jax)(y_cluster_outer, z_cluster_outer, w_cluster_outer, error_cluster_outer)
            xc_outer, yc_outer, r_outer = jax.vmap(final_params)(outer_fit_params)

            xc_outer = xc_outer[:,jnp.newaxis]
            yc_outer = yc_outer[:,jnp.newaxis]
            r_outer = r_outer[:,jnp.newaxis]

            inner_coordinates = inner_input_preprocessing(arrays_inner, batch_size)[0]
            inner_labels = inner_input_preprocessing(arrays_inner, batch_size)[1]

            y_inner, z_inner, label_inner, extrap_inner, deltay_inner = jax.vmap(from_fit_to_mlp_input)(inner_coordinates, inner_labels, xc_outer, yc_outer, r_outer, y_cluster_outer)
            y_inner = jnp.asarray(y_inner).transpose(1,0,2)
            z_inner = jnp.asarray(z_inner).transpose(1,0,2)
            label_inner = jnp.asarray(label_inner).transpose(1,0,2)
            extrap_inner = extrap_inner[:,:,jnp.newaxis]
            deltay_inner = jnp.asarray(deltay_inner).transpose(1,0,2)
            
            #replace to the dummy_input_graph features the stacked coordinates (y_inner, z_inner, deltay_inner)
            input_inner_graph = dummy_graph._replace(nodes = jnp.stack((y_inner, z_inner, deltay_inner), axis=-1))
            output_graph_inner = network.apply(params, input_graph, input_inner_graph)[1]
            
            radius_inner = jnp.reshape(output_graph_inner.nodes, (output_graph_inner.nodes.shape[0], output_graph_inner.nodes.shape[1] * output_graph_inner.nodes.shape[2]))
            id_mask_inner = jnp.reshape(label_inner, (label_inner.shape[0], label_inner.shape[1]*label_inner.shape[2]))
            loss_inner = (radius_inner - id_mask_inner)**2
            loss_inner = jnp.sum(loss_inner.ravel()) / jnp.count_nonzero(loss_inner.ravel())
            
            final_loss = jnp.where(step >  90, loss_outer + loss_inner, loss_outer)
            return final_loss , loss_outer, loss_inner



def prediction_loss2(params, input_graph, target_graph, input_inner):
        
        output_graph, output_inner = network.apply(params, input_graph, input_inner)        
        input_mask = input_graph.nodes[:,:,0]    
        radius = jnp.reshape(output_graph.nodes, (output_graph.nodes.shape[0], output_graph.nodes.shape[1]))        
        id_mask = target_graph.nodes
        loss = (radius - id_mask)**2
        loss2 = jnp.where(input_mask != 5, loss, 0)   
        
        return jnp.sum(loss2) / jnp.count_nonzero(loss2)


@jax.jit
def update(params, opt_state, gr):
        """Returns updated params and state."""
        updates, opt_state = opt_update(gr, opt_state)
        return optax.apply_updates(params, updates), opt_state


dataloader = DataLoader(arrays, batch_size,  key = jrandom.PRNGKey(68743))
it = itertools.tee(dataloader, steps) 

loss_list = []
outer_loss_list = []
inner_loss_list = []

n = (end_training - start_training) / batch_size
print('Training.. ')
for step in range(steps):
    
        epoch_loss = 0
        epoch_loss_outer = 0
        epoch_loss_inner = 0
        iter_data = it[step]
        for g in iter_data:
                input_graph = g[0]
                truth_graph = g[1]
                input_inner = dummy_graph
                loss, loss_outer, loss_inner = prediction_loss_value(params, input_graph, truth_graph, input_inner)                
                gr = jax.grad(prediction_loss, argnums=0)(params, input_graph, truth_graph, input_inner)            
                epoch_loss += loss 
                epoch_loss_outer += loss_outer
                epoch_loss_inner += loss_inner
               
                params, opt_state = update(params, opt_state, gr)

        print('--------------------->  STEP: ', step, ' LOSS: ', epoch_loss/n, 'OUTER LOSS: ', epoch_loss_outer/n, 'INNER LOSS: ', epoch_loss_inner/n)
        loss_list.append(epoch_loss / n )
        outer_loss_list.append(loss_outer / n)
        inner_loss_list.append(loss_inner / n)



import matplotlib.pyplot as plt 
plt.figure(figsize=(7, 5), dpi=100)
xrange = np.arange(0,steps,1)
plt.plot(xrange, np.asarray(loss_list), label = 'training loss')
plt.plot(xrange[90:], np.asarray(outer_loss_list[90:]), label = 'outer loss')
plt.plot(xrange[90:], np.asarray(inner_loss_list[90:]), label = 'inner loss')
plt.xlabel('epochs')
plt.ylabel('value')
plt.legend()
plt.savefig('loss_gatgat.png')
