# Installation 

## Pre-requistes 
To be able to install this repository you should first need [miniconda](https://docs.conda.io/projects/miniconda/en/latest/) to be installed on your machine (to do only once)

```bash
mkdir -p ~/miniconda3
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -O ~/miniconda3/miniconda.sh
bash ~/miniconda3/miniconda.sh -b -u -p ~/miniconda3
rm -rf ~/miniconda3/miniconda.sh
```
After installation 
```bash
~/miniconda3/bin/conda init bash
~/miniconda3/bin/conda init zsh
```

## Installation of the environment with conda 

In a fresh terminal clone the repository and create the environment needed 
```bash 
git clone ssh://git@gitlab.cern.ch:7999/lrambell/gnn-differentiable-patternreco.git

cd gnn-differentiable-patternreco
conda env create -f rambeluc_JAX.yml
```


In principle the installatiation has succeeded to setup the environment needed for studies you just need to do 
```
conda activate rambeluc_JAX
```

## Setup Requirements

<<<<<<< HEAD
- conda environment rambeluc_JAXnew
- 3 dataset required:
=======
- Setup the `rambeluc_JAX` conda environment with 
  ```
  conda activate rambeluc_JAX
  ```
- 3 dataset required: 
>>>>>>> 50d331ef15f51e26caded5b5924106b7472ff3a6
  1. ```/home/lrambelli/patternreco/arrays_layer_nhit.npy```
  2. ```/home/lrambelli/patternreco/arrays_layer_nhit_internal.npy```
  3. ```/home/lrambelli/patternreco/arrays_layer_nhit_tot_pt.npy```

1 contains info used for GAT graphs construction, i.e. only the outer layers hits (for preprocessed events with Nhit less than 100). 

2 contains internal layers hits for the selected outer ones.

3 contains all layers infos and also the pt related to each event (used for validation).

## Usage
The notebook `studies_final.ipynb` can be used for debugging, in it a pertained GAT model is used (parameters are in ```/home/lrambelli/patternreco/params_gnn.pkl```). 

When everything is fine the ```gnn_fit_onlydiff_10k.py``` script can be modified and used for high stat trainings. 

## Setup Environment info 
```pip install --upgrade pip```
```pip install --upgrade "jax[cuda12_local]" -f https://storage.googleapis.com/jax-releases/jax_cuda_releases.html```
```conda install -c conda-forge dm-haiku```
```pip install -U "jax[cuda12_pip]" -f https://storage.googleapis.com/jax-releases/jax_cuda_releases.html```
```pip install jraph```