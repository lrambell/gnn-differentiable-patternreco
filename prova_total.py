
import numpy as np

import jax.numpy as jnp
import jraph
import jax.random as jrandom
import jax.tree_util as tree
import jax
import flax 
import haiku as hk
from typing import Any, Callable, Dict, List, Optional, Tuple
import optax
import os
import itertools
import matplotlib.pyplot as plt 
from jraph._src import models
import jaxopt

from jax.experimental.host_callback import call



from jax.lib import xla_bridge
print('Using platform: ', xla_bridge.get_backend().platform)
jax.config.update("jax_enable_x64", True)

os.environ["XLA_PYTHON_CLIENT_PREALLOCATE"]="false"
os.environ["XLA_PYTHON_CLIENT_MEM_FRACTION"]=".10"
os.environ["XLA_PYTHON_CLIENT_ALLOCATOR"]="platform"

arrayss = np.load('/home/lrambelli/patternreco/arrays_layer_nhit.npy', allow_pickle = True)
X_nhit = arrayss[0]  #96493
Y_nhit = arrayss[1]*1e2
Z_nhit = arrayss[2]
ID_nhit = arrayss[3]

arrays_MLP = np.load('/home/lrambelli/gnn-differentiable-patternreco/arrays_layer_nhit_internal.npy', allow_pickle = True)
X_tot = arrays_MLP[0]
Y_tot = arrays_MLP[1]
Z_tot = arrays_MLP[2]
ID_tot = arrays_MLP[3]

arrays_PT = np.load('/home/lrambelli/gnn-differentiable-patternreco/arrays_layer_nhit_tot_pt.npy', allow_pickle=True)

print('Setting parameters..') 
start_training = 0
end_training= 6
batch_size = 3
lr = 1e-4
steps = 10

#from jax.config import config
#config.update("jax_debug_nans", True)



import pickle

def save(params, path):
  with open(path, 'wb') as fp:
    pickle.dump(params, fp)

def load(path):
    with open(path, 'rb') as fp:
        params = pickle.load(fp)
    return params


print('Events for training: ', end_training, ', batch size: ', batch_size, ', learning rate: ', lr)

arrays_training = np.asarray((X_nhit[start_training:end_training], Y_nhit[start_training:end_training], Z_nhit[start_training:end_training], ID_nhit[start_training:end_training]))
arrays_inner_training = np.asarray((X_tot[start_training:end_training], Y_tot[start_training:end_training], Z_tot[start_training:end_training], ID_tot[start_training:end_training]))
arrays_training_pt = np.asarray((arrays_PT[4][start_training:end_training]/1e3))
start_test = end_training + 1
end_test= start_test + 100

arrays_test = np.asarray((X_nhit[start_test:end_test], Y_nhit[start_test:end_test], Z_nhit[start_test:end_test], ID_nhit[start_test:end_test]))
arrays_inner_test = np.asarray((X_tot[start_test:end_test], Y_tot[start_test:end_test], Z_tot[start_test:end_test], ID_tot[start_test:end_test]))
arrays_test_pt = np.asarray((arrays_PT[4][start_test:end_test]/1e3))
## functions for graph creation ##

#function that compute the node number (hit number) for each graph (event) in the batch
def get_node_number(arrays, batch_size):
    node_numbers = []
    x = arrays[0]
    
    for i in range(batch_size):
        
        xx = x[i]
        node_numbers.append([xx.shape[0]])
    max_value = np.max(node_numbers)
    #max_value = 384
    
    return node_numbers, max_value

#function that returns the n_nodes and n_edges vectors required for defining the GraphTuple
#n_nodes vector is like ([a],[b],[c]) with a,b,c nodes numbers for each graph in the batch
#n_edges vector is like ([a**2], [b**2], [c**2]) where each element is the number of edges for the graph (fully connected)

def get_nodes_edges_per_event(arrays, batch_size):
    data_array = get_node_number(arrays, batch_size)[0]

    hits_per_event = data_array
    edges_per_event = [[nhits[0]**2] for nhits in data_array]
    return hits_per_event, edges_per_event


#function that creates for each graoh in the batch the vectors defining senders and receivers
#explicitly batched graph is used so all the vectors are padded to the dimension of the bigger one 

def create_senders_receivers(arrays, batch_size):
    #nhits = get_nodes_edges_per_event(arrays, batch_size)[0]
    nhits = get_nodes_edges_per_event(arrays, batch_size)[0]
    senders = []
    receivers = []
    for nhit in nhits:
        n = nhit[0]
        s = jnp.tile(np.arange(n), n).tolist()
        r = jnp.repeat(np.arange(n), n).tolist()
        
        senders.append(s)
        receivers.append(r)

    padded_senders = []
    padded_receivers = []
    #max_value = 384
    max_value = get_node_number(arrays, batch_size)[1]

    for sender in senders:
        pad_s = sender + [-1] * (max_value**2 - len(sender))
        padded_senders.append(pad_s)

    for receiver in receivers:
        pad_r = receiver + [-1] * (max_value**2 - len(receiver))
        padded_receivers.append(pad_r)

    return padded_senders, padded_receivers


#function that decorates the target nodes with only the particle id (muon == -13.)
def decorate_nodes_truth(arrays, batch_size):
    nodes = []
    #max_value = 384
    max_value = get_node_number(arrays, batch_size)[1]    
    
    for i in range (0, batch_size):        
        id = jnp.asarray(arrays[3][i])
        id = jnp.where(jnp.absolute(id) == 13, 1, 0) #for bce

        padded_id =  jnp.pad(id, (0, max_value - len(id)), mode='constant')
        node_features = jnp.stack((padded_id), axis=-1) 
        nodes.append(node_features)

    return nodes

#function that decorates the input nodes with (y,z) coordinates
def decorate_nodes(arrays,batch_size):
    nodes = []
    max_value = get_node_number(arrays, batch_size)[1]   
    #max_value = 384 
    for i in range (0, batch_size):
        y = jnp.asarray(arrays[1][i])
        z = jnp.asarray(arrays[2][i])
                   
        pad_y =  jnp.pad(y, (0, max_value - len(y)), mode='constant')
        pad_z =  jnp.pad(z, (0, max_value - len(z)), mode='constant')
      
        padded_y = jnp.where(pad_y!=0, pad_y, 5)
        padded_z = jnp.where(pad_y!=0, pad_z, 5)

        node_features = jnp.stack((padded_y,padded_z), axis=-1)
        nodes.append(node_features)

    return nodes

def GetGraphs(arrays: jnp.ndarray, batch_size : int) -> jraph.GraphsTuple:
    graph = jraph.GraphsTuple(
        n_node=jnp.array(get_nodes_edges_per_event(arrays, batch_size)[0]), 
        n_edge=jnp.array(get_nodes_edges_per_event(arrays, batch_size)[1]), 
        nodes=jnp.array(decorate_nodes(arrays, batch_size)), 
        edges=None, 
        globals=None,  
        senders=jnp.array(create_senders_receivers(arrays, batch_size)[0]), 
        receivers=jnp.array(create_senders_receivers(arrays, batch_size)[1]))
    
    graph_truth = jraph.GraphsTuple(
        n_node=jnp.array(get_nodes_edges_per_event(arrays, batch_size)[0]), 
        n_edge=jnp.array(get_nodes_edges_per_event(arrays, batch_size)[1]), 
        nodes=jnp.array(decorate_nodes_truth(arrays, batch_size)), 
        edges=None, 
        globals=None,  
        senders=jnp.array(create_senders_receivers(arrays, batch_size)[0]), 
        receivers=jnp.array(create_senders_receivers(arrays, batch_size)[1]))
    
    return (graph, graph_truth)

## network definition ##

def add_self_edges_fn(receivers: jnp.ndarray, senders: jnp.ndarray,
                      total_num_nodes: int) -> Tuple[jnp.ndarray, jnp.ndarray]:
  """Adds self edges. Assumes self edges are not in the graph yet."""
  receivers = jnp.concatenate((receivers, jnp.arange(total_num_nodes)), axis=0)
  senders = jnp.concatenate((senders, jnp.arange(total_num_nodes)), axis=0)
  return receivers, senders

#################
# GAT implementation adapted from https://github.com/deepmind/jraph/blob/master/jraph/_src/models.py#L442.
def GAT(attention_query_fn: Callable,
        attention_logit_fn: Callable,
        node_update_fn: Optional[Callable] = None,
        add_self_edges: bool = True) -> Callable:
  
  # pylint: disable=g-long-lambda
  if node_update_fn is None:
    # By default, apply the leaky relu and then concatenate the heads on the
    # feature axis.
    node_update_fn = lambda x: jnp.reshape(
        jax.nn.leaky_relu(x), (x.shape[0], -1))

  def _ApplyGAT(graph: jraph.GraphsTuple) -> jraph.GraphsTuple:
    """Applies a Graph Attention layer."""
    nodes, edges, receivers, senders, _, _, _ = graph
    # Equivalent to the sum of n_node, but statically known.
    try:
      sum_n_node = nodes.shape[0]
    except IndexError:
      raise IndexError('GAT requires node features')

    # Pass nodes through the attention query function to transform
    # node features, e.g. with an MLP.
    nodes = attention_query_fn(nodes)

    total_num_nodes = tree.tree_leaves(nodes)[0].shape[0]
    if add_self_edges:
      # We add self edges to the senders and receivers so that each node
      # includes itself in aggregation.
      receivers, senders = add_self_edges_fn(receivers, senders,
                                             total_num_nodes)

    # We compute the softmax logits using a function that takes the
    # embedded sender and receiver attributes.
    sent_attributes = nodes[senders]
    received_attributes = nodes[receivers]
    att_softmax_logits = attention_logit_fn(sent_attributes,
                                            received_attributes, edges)

    # Compute the attention softmax weights on the entire tree.
    att_weights = jraph.segment_softmax(
        att_softmax_logits, segment_ids=receivers, num_segments=sum_n_node)

    # Apply attention weights.
    messages = sent_attributes * att_weights
    # Aggregate messages to nodes.
    nodes = jax.ops.segment_sum(messages, receivers, num_segments=sum_n_node)

    # Apply an update function to the aggregated messages.
    nodes = node_update_fn(nodes)

    return graph._replace(nodes=nodes)

  # pylint: enable=g-long-lambda
  return _ApplyGAT

def gat_definition(graph: jraph.GraphsTuple, inner_graph: jraph.GraphsTuple) -> Tuple[jraph.GraphsTuple, jraph.GraphsTuple]:

  def _attention_query_fn1(node_features):
        return hk.nets.MLP([4, 8, 16, 32, 64, 128, 256, 512, 512, 1024])(node_features)
  
  def _attention_logit_fn1(senders, receivers, edges):
        del edges
        feat = jnp.concatenate((senders, receivers), axis=-1)
        return hk.nets.MLP([1024])(jax.nn.leaky_relu(hk.nets.MLP([2048, 1024, 1024])(feat)))

  gn = GAT(
      attention_query_fn=_attention_query_fn1,
      attention_logit_fn=_attention_logit_fn1,
      node_update_fn=None,
      add_self_edges=True)
  graph = gn(graph)

  def _attention_query_fn2(node_features):
        return node_features
  
  def _attention_logit_fn2(senders, receivers, edges):
        del edges
        feat = jnp.concatenate((senders, receivers), axis=-1)
        return hk.nets.MLP([1024])(jax.nn.leaky_relu(hk.nets.MLP([2048, 1024, 1024])(feat)))


  gn = GAT(
      attention_query_fn=_attention_query_fn2,
      attention_logit_fn=_attention_logit_fn2,
      node_update_fn=hk.nets.MLP([1024, 512, 256, 128, 64, 32, 32, 16, 8, 4, 2, 1]),
      add_self_edges=True)
  graph = gn(graph)

  def _attention_query_fn3(node_features):
        return hk.nets.MLP([4, 8, 16, 32, 64, 128, 256, 512, 512, 1024])(node_features)
  
  def _attention_logit_fn3(senders, receivers, edges):
        del edges
        feat = jnp.concatenate((senders, receivers), axis=-1)
        return hk.nets.MLP([1024])(jax.nn.leaky_relu(hk.nets.MLP([2048, 1024, 1024])(feat)))

  gn2 = GAT(
      attention_query_fn=_attention_query_fn3,
      attention_logit_fn=_attention_logit_fn3,
      node_update_fn=hk.nets.MLP([1024, 512, 256, 128, 64, 32, 32, 16, 8, 4, 2, 1]),
      add_self_edges=True)
  inner_graph = gn2(inner_graph)
 
  return graph, inner_graph

print('Graph definition')
graph = GetGraphs(arrays_training, batch_size)[0]
graph_truth = GetGraphs(arrays_training,batch_size)[1]

def DataLoader(arrays, arrays_inner, arrays_pt, batch_size, *, key):
    dataset_size = arrays[0].shape[0]
    #print('n_events: ', dataset_size, ', batch_size: ', batch_size)
        
    (key,) = jrandom.split(key, 1)
    start = 0
    end = batch_size
    while end <= dataset_size:
        
        yield GetGraphs(arrays[:,start:end], batch_size), arrays_inner[:,start:end], arrays_pt[start:end]
        start = end
        end = start + batch_size

def build_toy_graph(batch, n) -> jraph.GraphsTuple:
    """Define a four node graph, each node has a scalar as its feature."""

    # create new node features vector with 40 elements all with dimension 3 (x,y,z) = (1,1,1)
    node_features = jnp.ones((batch, n, 3))

    # define senders as a list of integer numbers between 0 and n-1 repeated batch times
    senders = jnp.tile(jnp.arange(n), (batch, 1))

    # new receivers vector with 40 elements with ordered numbers between 0 and 39
    receivers = jnp.tile(jnp.arange(n), (batch, 1))

    edges = None

    n_node = jnp.array([n]) * jnp.ones((batch, 1))
    n_edge = jnp.array([n]) * jnp.ones((batch, 1))

    # Optionally you can add `global` information, such as a graph label.
    global_context = None  # Same feature dims as nodes and edges.
    graph = jraph.GraphsTuple(
            nodes=node_features,
            edges=edges,
            senders=senders,
            receivers=receivers,
            n_node=n_node,
            n_edge=n_edge,
            globals=global_context
    )
    return graph

dummy_graph = build_toy_graph(batch_size, 40)

## functions for inner gat input building 

strip_error = 0.0005 / jnp.sqrt(12)


def inner_input_preprocessing(MLP_arrays, batch_size):
    coordinates = []
    labels = []

    max = get_node_number(MLP_arrays, batch_size)[1]

    for i in range (0, batch_size):
        
        y = jnp.asarray(MLP_arrays[1][i])
        z = jnp.asarray(MLP_arrays[2][i])
        id = jnp.asarray(MLP_arrays[3][i])
        id = jnp.where(jnp.absolute(id) == 13, 1, 0)

        pad_y =  jnp.pad(y, (0, max - len(y)), mode='constant')
        pad_z =  jnp.pad(z, (0, max - len(z)), mode='constant')
    
        padded_y = jnp.where(pad_y!=0, pad_y, 5)
        padded_z = jnp.where(pad_y!=0, pad_z, 5)
        padded_id =  jnp.pad(id, (0, max - len(id)), mode='constant')

        #funzioni per clustering, estrapolazione 
        input_coords = jnp.stack((padded_y,padded_z), axis=-1)
        truth_labels = jnp.stack((padded_id), axis=-1) 

        coordinates.append(input_coords)
        labels.append(truth_labels)

    return jnp.asarray(coordinates), jnp.asarray(labels)


def cluster_hits(input_graph, predictions):
    coordinates = input_graph.nodes   

    y_input = coordinates[:,0]
    z_input = coordinates[:,1]
    all_weights = jnp.absolute(predictions).ravel()
    #all_weights = ManipulateGraph.shifted_sigmoid(jnp.absolute(predictions)).ravel()
    real_weights = jnp.where(y_input != 5., all_weights, 0)

    layers = [2.495 , 2.505, 2.605, 2.705, 2.805, 4.205 , 4.305 ,4.405, 4.505]
    clustered_y = []
    weights_layer_sum = []
    rms_y_layers = []

    y_input = y_input/1e2
    for i in range(len(layers)):

        weights_layer = jnp.where(z_input == layers[i], real_weights, 0)
        y = jnp.where(z_input==layers[i], y_input, 0)
        #weights_layer = jnp.where(weights_layer == 1., 1, 0)

        weighted_y = weights_layer*y

        sum_w = jnp.sum(weights_layer)
        y_mean = jnp.sum(weighted_y) / sum_w

        clustered_y.append(y_mean)
        weights_layer_sum.append(sum_w)
        layer_rms = jnp.sqrt(jnp.sum((weights_layer*((y - y_mean)))**2) / jnp.sum(weights_layer))
        layer_rms = jnp.where(jnp.nan_to_num(layer_rms) != 0, layer_rms, strip_error)
        rms_y_layers.append(layer_rms)

    rms_y_layers = jnp.asarray(rms_y_layers)
    return clustered_y, layers, weights_layer_sum, rms_y_layers


@jax.jit
def cartesian_to_polar_GAT(y_cluster, fitted_xcenter, fitted_ycenter):
    GAT_layers = [2.495 , 2.505, 2.605, 2.705, 2.805, 4.205 , 4.305 ,4.405, 4.505]

    x = jnp.asarray(y_cluster)
    y = jnp.asarray(GAT_layers)
    dx = x - fitted_xcenter
    dy = y - fitted_ycenter

    theta = jnp.nan_to_num(jnp.arctan2(dy, dx))   
    theta_ref = jnp.arctan2(fitted_xcenter, fitted_ycenter)
    theta = jnp.where(theta < theta_ref, theta + 2*jnp.pi, theta)

    return jnp.sum(theta)/jnp.count_nonzero(theta), jnp.arctan2(dy, dx)

@jax.jit
def find_nearest_y_points(z_layer, y_layer, y_hits_layer, z_layers_hits, labels):
    num_points=5
    selected_y = []
    selected_z = []
    selected_labels = []
    selected_y_distances = []
    for i in range(len(z_layer)):
        z = z_layer[i]
        y = y_layer[i]   
        y_layer_hits = jnp.where((z_layers_hits == z), y_hits_layer, 5)
        y_diffs = y_layer_hits - y
        abs_y_diffs = jnp.absolute(y_diffs)
        sorted_indices = jnp.argsort(abs_y_diffs)

        sorted_array = y_layer_hits[sorted_indices]
        sorted_distances = y_diffs[sorted_indices]            
        
        sorted_label = labels[sorted_indices]

        selected_y_values = sorted_array[:num_points]
        selected_label = sorted_label[:num_points]      
        selected_z_values = z * jnp.ones_like(selected_y_values)
        selected_y_diffs = sorted_distances[:num_points]
    
        
        selected_y.append(selected_y_values)
        selected_z.append(selected_z_values)
        selected_labels.append(selected_label)
        selected_y_distances.append(selected_y_diffs)  


    return selected_y, selected_z, selected_labels, selected_y_distances



def from_fit_to_mlp_input(MLP_coordinates, MLP_labels, fitted_xcenter, fitted_ycenter, fitted_radius, y_cluster, batch_size):
    MLP_layers = [0.405, 0.505, 0.605, 0.705, 0.805, 0.905, 1.005, 1.105]
    outer_layers = [0.405, 4.505]
    theta_mean, theta = jax.vmap(cartesian_to_polar_GAT)(y_cluster, fitted_xcenter, fitted_ycenter)
    
    extrapolated_theta_MLP_layers = jnp.arcsin((jnp.asarray(MLP_layers) - fitted_ycenter[:,jnp.newaxis]) / fitted_radius[:,jnp.newaxis])
    mean_extrap_theta_MLP_layers = jnp.mean(extrapolated_theta_MLP_layers, axis=1)
    theta_outer_layers = jnp.arcsin((jnp.asarray(outer_layers) - fitted_ycenter[:,jnp.newaxis]) / fitted_radius[:,jnp.newaxis])
    theta_outer_layers = jnp.reshape(theta_outer_layers, (theta_outer_layers.shape[0], 2))
    delta_theta_outer_layers = jnp.abs(theta_outer_layers[:,0] - theta_outer_layers[:,1])
    delta_theta_outer_layers = jnp.reshape(delta_theta_outer_layers, (delta_theta_outer_layers.shape[0], 1))
    extrapolated_theta_MLP_layers = jnp.reshape(extrapolated_theta_MLP_layers, (extrapolated_theta_MLP_layers.shape[0], 8))
    theta_mean = jnp.reshape(theta_mean, (theta_mean.shape[0], 1))
    good_happy_theta_inner_layers = jnp.where((jnp.absolute(mean_extrap_theta_MLP_layers - theta_mean) < 2 * delta_theta_outer_layers)[:,jnp.newaxis], extrapolated_theta_MLP_layers, jnp.pi - extrapolated_theta_MLP_layers)
    extrapolated_y_MLP_fromtheta = fitted_xcenter[:,jnp.newaxis] + fitted_radius[:,jnp.newaxis] * jnp.cos(good_happy_theta_inner_layers)  
    
    y_mlp_input = MLP_coordinates[:,:,0]
    z_mlp_input = MLP_coordinates[:,:,1]
    MLP_layers_vec = jnp.tile(jnp.asarray(MLP_layers), (batch_size, 1))
    n_extrapolated =  jnp.nan_to_num(extrapolated_y_MLP_fromtheta)
    mean_y_cluster_outer = jnp.mean(y_cluster, axis=1, keepdims=True)
   
    mlp_labels = MLP_labels
    pt_value = 0.3*fitted_radius*1000
    pt_value = pt_value[:,jnp.newaxis]  
    extrapolated_hits = jnp.where(jnp.count_nonzero(n_extrapolated, axis=1, keepdims=True) == 8, 
                              extrapolated_y_MLP_fromtheta, 
                              mean_y_cluster_outer * jnp.ones(extrapolated_y_MLP_fromtheta.shape))    
   
    ymlp, zmlp, lmlp , ydiffmlp = jax.vmap(find_nearest_y_points)(MLP_layers_vec, extrapolated_hits[0,:,:], y_mlp_input, z_mlp_input, mlp_labels)

    return ymlp, zmlp, lmlp, extrapolated_hits, ydiffmlp



@jax.jit
def final_params(r):
  A3 = r[0]
  D3 = r[1]
  theta3 = r[2]

  B3 = jnp.sqrt(jnp.absolute(1+4*A3*D3))*jnp.cos(theta3)
  C3 = jnp.sqrt(jnp.absolute(1+4*A3*D3))*jnp.sin(theta3)

  fitted_xcenter = -B3/(2*A3)
  fitted_ycenter = -C3/(2*A3)
  fitted_radius = 1/(2*jnp.abs(A3))
  return fitted_xcenter, fitted_ycenter, fitted_radius


def predict(params, input_graph, inner_graph):
    return network.apply(params, input_graph, inner_graph)

@jax.jit
def chi_squared(fit_params, x, y, w, err):
    A, D, theta = fit_params
    P = A * (x**2 + y**2) + jnp.sqrt((1 + 4*A*D)) * (x * jnp.cos(theta) + y * jnp.sin(theta)) + D
    residuals = w * (2*P/(1+jnp.sqrt(1+4*A*P)))
    chi2 = jnp.sum((residuals / err)**2)
    return chi2

def loss_out(predictions, truth, mask):
    truth = jnp.reshape(truth, (truth.shape[0], truth.shape[1], 1))
    loss = (predictions - truth)**2
    mask = jnp.reshape(mask, (mask.shape[0], mask.shape[1], 1))
    loss_real = jnp.where(mask != 5, loss, 0)
    return jnp.sum(loss_real) / jnp.count_nonzero(loss_real)

def loss_fit(pt_reco, pt_truth):
    return jnp.mean(jnp.sqrt((pt_reco - pt_truth)**2))

def chi_squared_for_lbfgs(initial_fit_params, *args):
    x,y,w,err = args
    return chi_squared(initial_fit_params, x, y, w, err)

#@jaxopt.implicit_diff.custom_root(jaxopt.LBFGS)
def solve_lbfgs(params, x,y,w,err, initial_fit_params):
    chi2_optimizer = jaxopt.LBFGS(fun=chi_squared_for_lbfgs, value_and_grad=False)
    optimized_params = chi2_optimizer.run(initial_fit_params, x,y,w,err).params
    return optimized_params


def cluster_hits_inner(input_graph, output_graph):
    coordinates = input_graph.nodes
    predictions = output_graph.nodes
    y_input = coordinates[:,:,0]
    z_input = coordinates[:,:,1]
    all_weights = jnp.absolute(predictions).ravel()

    #all_weights = shifted_sigmoid(jnp.absolute(predictions)).ravel()
    real_weights = jnp.where(y_input.ravel() != 500., all_weights.ravel(), 0)
    layers = [0.405, 0.505, 0.605, 0.705, 0.805, 0.905, 1.005, 1.105]
    clustered_y = []
    weights_layer_sum = []
    rms_y_layers = []

    z_input = z_input.ravel()
    y_input = y_input.ravel()
    real_weights = real_weights.ravel()
    for i in range(len(layers)):
        weights_layer = jnp.where(z_input == layers[i], real_weights, 0)
        y = jnp.where(z_input==layers[i], y_input, 0)
        #weights_layer = jnp.where(weights_layer == 1., 1, 0)
        weighted_y = weights_layer*y

        sum_w = jnp.sum(weights_layer)
        y_mean = jnp.sum(weighted_y) / sum_w

        clustered_y.append(y_mean)
        weights_layer_sum.append(sum_w)
        layer_rms = (jnp.sqrt(jnp.sum((weights_layer*((y - y_mean)))**2) / jnp.sum(weights_layer))) / jnp.sqrt(12)
        layer_rms = jnp.where(jnp.nan_to_num(layer_rms) != 0, layer_rms, strip_error)
        rms_y_layers.append(layer_rms)

    rms_y_layers = jnp.asarray(rms_y_layers)
    return clustered_y, layers, weights_layer_sum, rms_y_layers

def loss_outer_fn(params, outer_graph, inner_graph, outer_truth_graph):

    output_graph = network.apply(params, outer_graph, inner_graph)[0]

    predictions = output_graph.nodes
    outer_input_mask = outer_graph.nodes[:,:,0]      
    outer_truth_labels = outer_truth_graph.nodes
    outer_truth_labels = jnp.reshape(outer_truth_labels, (outer_truth_labels.shape[0], outer_truth_labels.shape[1], 1))
    outer_input_mask = jnp.reshape(outer_input_mask, (outer_input_mask.shape[0], outer_input_mask.shape[1], 1))

    outer_loss = (predictions - outer_truth_labels)**2
    outer_loss_real = jnp.where(outer_input_mask != 5., outer_loss, 0)
    loss_outer = jnp.sum(outer_loss_real) / jnp.count_nonzero(outer_loss_real)

    return loss_outer

def loss_inner_fn(params, outer_graph, inner_graph, inner_truth):

    output_graph_inner = network.apply(params, outer_graph, inner_graph)[1]
    inner_labels = inner_truth
    #inner_labels = jnp.reshape(inner_labels, (inner_labels.shape[0], inner_labels.shape[1] * inner_labels.shape[2]))
    
    predictions_inner = jnp.reshape(output_graph_inner.nodes, (output_graph_inner.nodes.shape[0], output_graph_inner.nodes.shape[1] * output_graph_inner.nodes.shape[2]))
    mask_inner = inner_graph.nodes[:,:,:,0]
    mask_inner = jnp.reshape(mask_inner, (mask_inner.shape[0], mask_inner.shape[1]*mask_inner.shape[2]))   
    loss_inner = (predictions_inner - inner_labels)**2
    loss_inner = jnp.where(mask_inner/1e2 != 5., loss_inner, 0)
    inner_loss = jnp.sum(loss_inner)/jnp.count_nonzero(loss_inner)

    return inner_loss

def combined_loss_fn(params, outer_graph, inner_graph, inner_truth, outer_truth_graph, pt_truth):
    loss_outer = loss_outer_fn(params, outer_graph, inner_graph, outer_truth_graph)
    loss_inner = loss_inner_fn(params, outer_graph, inner_graph, inner_truth)
    return loss_outer + loss_inner

def loss_tot_fn(params, initial_fit_params, outer_graph, inner_graph, inner_coords, inner_labels, outer_truth_graph, pt_truth):

    output_graph = network.apply(params, outer_graph, inner_graph)[0]

    predictions = output_graph.nodes
    outer_input_mask = outer_graph.nodes[:,:,0]      
    outer_truth_labels = outer_truth_graph.nodes
    outer_truth_labels = jnp.reshape(outer_truth_labels, (outer_truth_labels.shape[0], outer_truth_labels.shape[1], 1))
    outer_input_mask = jnp.reshape(outer_input_mask, (outer_input_mask.shape[0], outer_input_mask.shape[1], 1))

    outer_loss = (predictions - outer_truth_labels)**2
    outer_loss_real = jnp.where(outer_input_mask != 5., outer_loss, 0)
    loss_outer = jnp.sum(outer_loss_real) / jnp.count_nonzero(outer_loss_real)


    y_cluster_outer , z_cluster_outer, w_cluster_outer, error_cluster_outer = jax.vmap(cluster_hits)(outer_graph, predictions)
    y_cluster_outer = jnp.array(y_cluster_outer).T
    z_cluster_outer = jnp.array(z_cluster_outer).T
    w_cluster_outer = jnp.array(w_cluster_outer).T

    w_cluster_outer = jnp.where(w_cluster_outer >= 1, 1, 0)
    y_cluster_outer = jnp.nan_to_num(y_cluster_outer)

    x = y_cluster_outer
    y = z_cluster_outer
    w = w_cluster_outer
    err = error_cluster_outer

    optimized_params = solve_lbfgs(params, x, y, w, err, initial_fit_params)
    xc_out, yc_out, radius_out = final_params(optimized_params)

    y_in, z_in, label_in, extrap_in, deltay_in = from_fit_to_mlp_input(MLP_coordinates=inner_coords, MLP_labels=inner_labels, fitted_xcenter=xc_out, fitted_ycenter=yc_out, fitted_radius=radius_out, y_cluster=y_cluster_outer, batch_size=batch_size)
    y_in = jnp.asarray(y_in).transpose(1,0,2)
    z_in = jnp.asarray(z_in).transpose(1,0,2)
    label_in = jnp.asarray(label_in).transpose(1,0,2)
    deltay_in = jnp.asarray(deltay_in).transpose(1,0,2)
    extrap_in = jnp.asarray(extrap_in)

    inner_graph_coords = jnp.stack((y_in*1e2, z_in, deltay_in), axis = -1)
    graph_inner = dummy_graph._replace(nodes = inner_graph_coords)
    truth_inner = jnp.reshape(label_in, (label_in.shape[0], label_in.shape[1]*label_in.shape[2]))
    
    output_graph_inner = network.apply(params, outer_graph, graph_inner)[1]
    y_cluster_inner, z_cluster_inner, w_cluster_inner, error_cluster_inner = jax.vmap(cluster_hits_inner)(graph_inner, output_graph_inner)
    y_cluster_inner = jnp.array(y_cluster_inner).T
    z_cluster_inner = jnp.array(z_cluster_inner).T
    w_cluster_inner = jnp.array(w_cluster_inner).T

    w_cluster_inner = jnp.where(w_cluster_inner >= 1, 1, 0)
    y_cluster_inner = jnp.nan_to_num(y_cluster_inner)

    y_cluster_total = jnp.concatenate((y_cluster_outer, y_cluster_inner), axis=1)
    z_cluster_total = jnp.concatenate((z_cluster_outer, z_cluster_inner), axis=1)
    w_cluster_total = jnp.concatenate((w_cluster_outer, w_cluster_inner), axis=1)
    error_cluster_total = jnp.concatenate((error_cluster_outer, error_cluster_inner), axis=1)

    optimized_final_params = solve_lbfgs(params, y_cluster_total, z_cluster_total, w_cluster_total, error_cluster_total, optimized_params)
    xc_tot, yc_tot, radius_tot = final_params(optimized_final_params)

    predictions_inner = jnp.reshape(output_graph_inner.nodes, (output_graph_inner.nodes.shape[0], output_graph_inner.nodes.shape[1] * output_graph_inner.nodes.shape[2]))
    mask_inner = graph_inner.nodes[:,:,:,0]
    mask_inner = jnp.reshape(mask_inner, (mask_inner.shape[0], mask_inner.shape[1]*mask_inner.shape[2]))   

    loss_inner = (predictions_inner - truth_inner)**2
    loss_inner = jnp.where(mask_inner/1e2 != 5., loss_inner, 0)
    inner_loss = jnp.sum(loss_inner)/jnp.count_nonzero(loss_inner)

    pt_reco_out = 0.3*radius_out
  
    #loss_fit_value = jnp.mean(jnp.sqrt(((pt_truth - pt_reco)/pt_truth)**2))
    #loss_fit_value = loss_fit_value.astype(jnp.float32)
    #print(' loss out ', jnp.isnan(loss_outer), ' loss pt ', jnp.isnan(loss_fit_value))
    #print(loss_outer, ' + ',   inner_loss)
    #total_loss = loss_outer + inner_loss #+ 0.001 * loss_fit_value
    total_loss =  jnp.nan_to_num(inner_loss)
    print('--> ', loss_outer, ' + ' , inner_loss, ' = ',  total_loss)
    return total_loss, optimized_final_params


## network inizialization ##
print('Network inizialization') 

network = hk.without_apply_rng(hk.transform(hk.vmap(gat_definition, split_rng=False)))
#params = load('/home/lrambelli/gnn-differentiable-patternreco/final_results/total_params/21_loss_0.27328648758555124_val_loss_0.33447893664110295.pkl')
params = load('/home/lrambelli/gnn-differentiable-patternreco/final_results/params_28_loss_0.09725006354438648_val_loss_0.12749541325188493.pkl')
opt_init, opt_update = optax.adam(lr)
opt_state = opt_init(params)

@jax.jit
def update(params, opt_state, gr):
        """Returns updated params and state."""
        updates, opt_state = opt_update(gr, opt_state)
        return optax.apply_updates(params, updates), opt_state

dataloader = DataLoader(arrays_training, arrays_inner_training, arrays_training_pt, batch_size,  key = jrandom.PRNGKey(683))
it = itertools.tee(dataloader, steps)


import time
start = time.time()
print('Training.. ')

loss_total_list = []
val_loss_total_list = []
loss_inner_list = []
loss_outer_list = []

for step in range(steps):

        epoch_total_loss = 0
        epoch_total_val_loss = 0
        epoch_inner_loss = 0
        epoch_outer_loss = 0
        iter_data = it[step]
        count = 0
        for g in iter_data:

            input_graph = g[0][0]
            truth_graph = g[0][1]
            arrays_inner_gat = g[1]
            pt_truth = jnp.array(g[2], dtype=jnp.float32)
            input_inner_graph = dummy_graph

            pt_truth = jnp.reshape(pt_truth, (pt_truth.shape[0], 1))

            output_graph = network.apply(params, input_graph, input_inner_graph)[0]
            
            y_cluster_outer , z_cluster_outer, w_cluster_outer, error_cluster_outer = jax.vmap(cluster_hits)(input_graph, output_graph.nodes)
            y_cluster_outer = jnp.array(y_cluster_outer).T
            z_cluster_outer = jnp.array(z_cluster_outer).T
            w_cluster_outer = jnp.array(w_cluster_outer).T

            w_cluster_outer = jnp.where(w_cluster_outer >= 1, 1, 0)
            y_cluster_outer = jnp.nan_to_num(y_cluster_outer)

            x = y_cluster_outer
            y = z_cluster_outer
            w = w_cluster_outer
            err = error_cluster_outer

            x0 = jnp.mean(x, axis=1)
            y0 = jnp.mean(y, axis=1)

            x0 = jnp.reshape(x0, (x0.shape[0], 1))
            y0 = jnp.reshape(y0, (y0.shape[0], 1))

            r = jnp.mean(jnp.sqrt((x-x0)**2 + (y-y0)**2), axis=1)
            r = jnp.reshape(r, (r.shape[0], 1))

            initial_fit_params = jnp.stack((1/2*r, jnp.zeros_like(r), jnp.zeros_like(r)), dtype = jnp.float32)            
            inner_coords, inner_labels = inner_input_preprocessing(arrays_inner_gat, batch_size)

            '''
            loss, params_val = loss_tot_fn(params, initial_fit_params, input_graph, input_inner_graph, inner_coords, inner_labels, truth_graph, pt_truth)
           
            (gr, optimized_params) = jax.grad(loss_tot_fn, argnums=0, has_aux=True)(params, initial_fit_params, input_graph, input_inner_graph, inner_coords, inner_labels, truth_graph, pt_truth)
            params = jax.tree_map(lambda p, g: p - lr*g , params, gr)

            initial_fit_params = optimized_params

            xc, yc, radius = final_params(optimized_params)
            pt_reco = 0.3*radius

            loss_value, optimized_params = loss_tot_fn(params, optimized_params, input_graph, input_inner_graph, inner_coords, inner_labels, truth_graph, pt_truth)
            print('loss post grad ', loss_value)

            #print('--->', pt_reco, pt_truth)
            '''
            y_cluster_outer , z_cluster_outer, w_cluster_outer, error_cluster_outer = jax.vmap(cluster_hits)(input_graph, output_graph.nodes)
            y_cluster_outer = jnp.array(y_cluster_outer).T
            z_cluster_outer = jnp.array(z_cluster_outer).T
            w_cluster_outer = jnp.array(w_cluster_outer).T

            w_cluster_outer = jnp.where(w_cluster_outer >= 1, 1, 0)
            y_cluster_outer = jnp.nan_to_num(y_cluster_outer)

            x = y_cluster_outer
            y = z_cluster_outer
            w = w_cluster_outer
            err = error_cluster_outer

            optimized_params = solve_lbfgs(params, x, y, w, err, initial_fit_params)
            xc_out, yc_out, radius_out = final_params(optimized_params)

            y_in, z_in, label_in, extrap_in, deltay_in = from_fit_to_mlp_input(MLP_coordinates=inner_coords, MLP_labels=inner_labels, fitted_xcenter=xc_out, fitted_ycenter=yc_out, fitted_radius=radius_out, y_cluster=y_cluster_outer, batch_size=batch_size)
            y_in = jnp.asarray(y_in).transpose(1,0,2)
            z_in = jnp.asarray(z_in).transpose(1,0,2)
            label_in = jnp.asarray(label_in).transpose(1,0,2)
            deltay_in = jnp.asarray(deltay_in).transpose(1,0,2)
            extrap_in = jnp.asarray(extrap_in)

            inner_graph_coords = jnp.stack((y_in*1e2, z_in, deltay_in), axis = -1)
            graph_inner = dummy_graph._replace(nodes = inner_graph_coords)
            truth_inner = jnp.reshape(label_in, (label_in.shape[0], label_in.shape[1]*label_in.shape[2]))


            gr = jax.grad(combined_loss_fn, argnums=0)(params, input_graph, graph_inner, truth_inner, truth_graph, pt_truth)
            params, opt_state = update(params, opt_state, gr)
            loss_inner_val = loss_inner_fn(params, input_graph, graph_inner, truth_inner)
            loss_outer_val = loss_outer_fn(params, input_graph, input_inner_graph, truth_graph)
            loss_value = combined_loss_fn(params, input_graph, graph_inner, truth_inner, truth_graph, pt_truth)

            epoch_total_loss += loss_value
            epoch_inner_loss += loss_inner_val
            epoch_outer_loss += loss_outer_val
            count += 1


        print('Step: ', step, ' Outer Loss: ', epoch_outer_loss/count, ' Inner Loss: ', epoch_inner_loss/count,  ' ----> Loss: ', epoch_total_loss/count, )
        loss_total_list.append(epoch_total_loss/count)
        loss_inner_list.append(epoch_inner_loss/count)
        loss_outer_list.append(epoch_outer_loss/count)

import matplotlib.pyplot as plt

plt.figure()
xrange = range(0, steps)
plt.plot(xrange, loss_total_list, label='Total Loss')
plt.plot(xrange, loss_inner_list, label='Inner Loss')
plt.plot(xrange, loss_outer_list, label='Outer Loss')
plt.legend()
plt.savefig('aridaje_losses.png')


