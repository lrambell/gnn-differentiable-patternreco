import numpy as np
import jax.numpy as jnp
import jraph
import jax.random as jrandom
import jax.tree_util as tree
import jax
import haiku as hk
from typing import Any, Callable, Dict, List, Optional, Tuple
import jaxopt

import optax 
import itertools

import pickle
import os

jax.config.update('jax_enable_x64', True)

def load(path):
    with open(path, 'rb') as fp:
        params = pickle.load(fp)
    return params

print("JAX backend:", jax.devices())

os.environ["XLA_PYTHON_CLIENT_PREALLOCATE"]="false"
os.environ["XLA_PYTHON_CLIENT_MEM_FRACTION"]=".50"
os.environ["XLA_PYTHON_CLIENT_ALLOCATOR"]="gpu"

gnn_params = load('params_gnn.pkl')
#mlp_params = load('FIT_params_gnn_mlp_epoch_49_loss_0.12440517900967624_val_loss_0.13381212054276626.pkl')

## loading dataset for classification GAT and MLP ###
arrays_GAT = np.load('arrays_layer_nhit.npy', allow_pickle = True)
X_nhit = arrays_GAT[0]  #96493
Y_nhit = arrays_GAT[1]*1e2
Z_nhit = arrays_GAT[2]
ID_nhit = arrays_GAT[3]

arrays_MLP = np.load('arrays_layer_nhit_internal.npy', allow_pickle = True)
X_tot = arrays_MLP[0]
Y_tot = arrays_MLP[1]
Z_tot = arrays_MLP[2]
ID_tot = arrays_MLP[3]


print('Setting parameters..') 
start_training = 0
end_training= 10
batch_size = 1

lr = 1e-4
lr_mlp = 1e-2
steps = 10

strip_error = 0.0005 / jnp.sqrt(12)

print('Events for training: ', end_training-start_training, ', batch size: ', batch_size)
print('GAT learning rate: ', lr, ' MLP learning rate: ', lr_mlp)

arrays_training_GAT = np.asarray((X_nhit[start_training:end_training], Y_nhit[start_training:end_training], Z_nhit[start_training:end_training], ID_nhit[start_training:end_training]))
arrays_training_MLP = np.asarray((X_tot[start_training:end_training], Y_tot[start_training:end_training], Z_tot[start_training:end_training], ID_tot[start_training:end_training]))

start_test = end_training + 1
end_test= start_test + 100

arrays_test_GAT = np.asarray((X_nhit[start_test:end_test], Y_nhit[start_test:end_test], Z_nhit[start_test:end_test], ID_nhit[start_test:end_test]))
arrays_test_MLP = np.asarray((X_tot[start_test:end_test], Y_tot[start_test:end_test], Z_tot[start_test:end_test], ID_tot[start_test:end_test]))

GAT_layers = [2.495 , 2.505, 2.605, 2.705, 2.805, 4.205 , 4.305 ,4.405, 4.505]
MLP_layers = [0.405, 0.505, 0.605, 0.705, 0.805, 0.905, 1.005, 1.105]
outer_layers = [0.405, 4.505]

def save(params, path):
  with open(path, 'wb') as fp:
    pickle.dump(params, fp)

## functions for graph creation ##

#function that compute the node number (hit number) for each graph (event) in the batch
def get_node_number(arrays, batch_size):
    node_numbers = []
    x = arrays[0]
    
    for i in range(batch_size):
        
        xx = x[i]
        node_numbers.append([xx.shape[0]])
    max_value = np.max(node_numbers)
    #max_value = 384
    
    return node_numbers, max_value

#function that returns the n_nodes and n_edges vectors required for defining the GraphTuple
#n_nodes vector is like ([a],[b],[c]) with a,b,c nodes numbers for each graph in the batch
#n_edges vector is like ([a**2], [b**2], [c**2]) where each element is the number of edges for the graph (fully connected)

def get_nodes_edges_per_event(arrays, batch_size):
    data_array = get_node_number(arrays, batch_size)[0]

    hits_per_event = data_array
    edges_per_event = [[nhits[0]**2] for nhits in data_array]
    return hits_per_event, edges_per_event


#function that creates for each graoh in the batch the vectors defining senders and receivers
#explicitly batched graph is used so all the vectors are padded to the dimension of the bigger one 

def create_senders_receivers(arrays, batch_size):
    #nhits = get_nodes_edges_per_event(arrays, batch_size)[0]
    nhits = get_nodes_edges_per_event(arrays, batch_size)[0]
    senders = []
    receivers = []
    for nhit in nhits:
        n = nhit[0]
        s = jnp.tile(np.arange(n), n).tolist()
        r = jnp.repeat(np.arange(n), n).tolist()
        
        senders.append(s)
        receivers.append(r)

    padded_senders = []
    padded_receivers = []
    #max_value = 384
    max_value = get_node_number(arrays, batch_size)[1]

    for sender in senders:
        pad_s = sender + [-1] * (max_value**2 - len(sender))
        padded_senders.append(pad_s)

    for receiver in receivers:
        pad_r = receiver + [-1] * (max_value**2 - len(receiver))
        padded_receivers.append(pad_r)

    return padded_senders, padded_receivers


#function that decorates the target nodes with only the particle id (muon == -13.)
def decorate_nodes_truth(arrays, batch_size):
    nodes = []
    #max_value = 384
    max_value = get_node_number(arrays, batch_size)[1]    
    
    for i in range (0, batch_size):        
        id = np.asarray(arrays[3][i])
        id = jnp.where(jnp.absolute(id) == 13, 1, 0) #for bce

        padded_id =  jnp.pad(id, (0, max_value - len(id)), mode='constant')
        node_features = jnp.stack((padded_id), axis=-1) 
        nodes.append(node_features)

    return nodes

#function that decorates the input nodes with (y,z) coordinates
def decorate_nodes(arrays,batch_size):
    nodes = []
    max_value = get_node_number(arrays, batch_size)[1]   
    #max_value = 384 
    for i in range (0, batch_size):
        y = np.asarray(arrays[1][i])
        z = np.asarray(arrays[2][i])
                   
        pad_y =  jnp.pad(y, (0, max_value - len(y)), mode='constant')
        pad_z =  jnp.pad(z, (0, max_value - len(z)), mode='constant')
      
        padded_y = jnp.where(pad_y!=0, pad_y, 5)
        padded_z = jnp.where(pad_y!=0, pad_z, 5)

        node_features = jnp.stack((padded_y,padded_z), axis=-1)
        nodes.append(node_features)

    return nodes

def GetGraphs(arrays: np.ndarray, batch_size : int) -> jraph.GraphsTuple:
    graph = jraph.GraphsTuple(
        n_node=jnp.array(get_nodes_edges_per_event(arrays, batch_size)[0]), 
        n_edge=jnp.array(get_nodes_edges_per_event(arrays, batch_size)[1]), 
        nodes=jnp.array(decorate_nodes(arrays, batch_size)), 
        edges=None, 
        globals=None,  
        senders=jnp.array(create_senders_receivers(arrays, batch_size)[0]), 
        receivers=jnp.array(create_senders_receivers(arrays, batch_size)[1]))
    
    graph_truth = jraph.GraphsTuple(
        n_node=jnp.array(get_nodes_edges_per_event(arrays, batch_size)[0]), 
        n_edge=jnp.array(get_nodes_edges_per_event(arrays, batch_size)[1]), 
        nodes=jnp.array(decorate_nodes_truth(arrays, batch_size)), 
        edges=None, 
        globals=None,  
        senders=jnp.array(create_senders_receivers(arrays, batch_size)[0]), 
        receivers=jnp.array(create_senders_receivers(arrays, batch_size)[1]))
    
    return (graph, graph_truth)

## network definition ##

def add_self_edges_fn(receivers: jnp.ndarray, senders: jnp.ndarray,
                      total_num_nodes: int) -> Tuple[jnp.ndarray, jnp.ndarray]:
  """Adds self edges. Assumes self edges are not in the graph yet."""
  receivers = jnp.concatenate((receivers, jnp.arange(total_num_nodes)), axis=0)
  senders = jnp.concatenate((senders, jnp.arange(total_num_nodes)), axis=0)
  return receivers, senders

#################
# GAT implementation adapted from https://github.com/deepmind/jraph/blob/master/jraph/_src/models.py#L442.
def GAT(attention_query_fn: Callable,
        attention_logit_fn: Callable,
        node_update_fn: Optional[Callable] = None,
        add_self_edges: bool = True) -> Callable:
  
  # pylint: disable=g-long-lambda
  if node_update_fn is None:
    # By default, apply the leaky relu and then concatenate the heads on the
    # feature axis.
    node_update_fn = lambda x: jnp.reshape(
        jax.nn.leaky_relu(x), (x.shape[0], -1))

  def _ApplyGAT(graph: jraph.GraphsTuple) -> jraph.GraphsTuple:
    """Applies a Graph Attention layer."""
    nodes, edges, receivers, senders, _, _, _ = graph
    # Equivalent to the sum of n_node, but statically known.
    try:
      sum_n_node = nodes.shape[0]
    except IndexError:
      raise IndexError('GAT requires node features')

    # Pass nodes through the attention query function to transform
    # node features, e.g. with an MLP.
    nodes = attention_query_fn(nodes)

    total_num_nodes = tree.tree_leaves(nodes)[0].shape[0]
    if add_self_edges:
      # We add self edges to the senders and receivers so that each node
      # includes itself in aggregation.
      receivers, senders = add_self_edges_fn(receivers, senders,
                                             total_num_nodes)

    # We compute the softmax logits using a function that takes the
    # embedded sender and receiver attributes.
    sent_attributes = nodes[senders]
    received_attributes = nodes[receivers]
    att_softmax_logits = attention_logit_fn(sent_attributes,
                                            received_attributes, edges)

    # Compute the attention softmax weights on the entire tree.
    att_weights = jraph.segment_softmax(
        att_softmax_logits, segment_ids=receivers, num_segments=sum_n_node)

    # Apply attention weights.
    messages = sent_attributes * att_weights
    # Aggregate messages to nodes.
    nodes = jax.ops.segment_sum(messages, receivers, num_segments=sum_n_node)

    # Apply an update function to the aggregated messages.
    nodes = node_update_fn(nodes)

    return graph._replace(nodes=nodes)

  # pylint: enable=g-long-lambda
  return _ApplyGAT

def gat_definition(graph: jraph.GraphsTuple) -> jraph.GraphsTuple:

  def _attention_query_fn1(node_features):
        return hk.nets.MLP([4, 8, 16, 32, 64, 128, 256, 512,512, 1024])(node_features)
  
  def _attention_logit_fn1(senders, receivers, edges):
        del edges
        feat = jnp.concatenate((senders, receivers), axis=-1)
        return hk.nets.MLP([1024])(jax.nn.leaky_relu(hk.nets.MLP([1024, 512, 256, 256, 128, 128])(feat)))

  gn = GAT(
      attention_query_fn=_attention_query_fn1,

      attention_logit_fn=_attention_logit_fn1,
      node_update_fn=hk.nets.MLP([512, 256, 256, 128, 128]),
      add_self_edges=True)
  graph = gn(graph)

  def _attention_query_fn2(node_features):
        return hk.nets.MLP([128, 256, 512, 1024])(node_features)
  
  def _attention_logit_fn2(senders, receivers, edges):
        del edges
        feat = jnp.concatenate((senders, receivers), axis=-1)
        return hk.nets.MLP([128, 64, 32, 16, 8, 4, 2, 1])(jax.nn.leaky_relu(hk.nets.MLP([1024, 512, 256, 256, 128, 128])(feat)))


  gn = GAT(
      attention_query_fn=_attention_query_fn2,

      attention_logit_fn=_attention_logit_fn2,
      node_update_fn=hk.nets.MLP([1024, 512, 256, 128, 64, 32, 32, 16, 8, 4, 2, 1]),
      add_self_edges=True)
  graph = gn(graph)
 
  return graph


@jax.jit
def shifted_sigmoid(x):
    return 1 / (1 + jnp.exp(-100 * (x - 0.5)))


def func(par, X, Y):
    A, D, theta = par
    P = A * (X**2 + Y**2) + jnp.sqrt(jnp.absolute(1 + 4*A*D)) * (X * jnp.cos(theta) + Y * jnp.sin(theta)) + D
    return 2*P/(1+jnp.sqrt(1+4*A*P))
@jax.jit
def final_params(r):
  A3 = r[0]
  D3 = r[1]
  theta3 = r[2]

  B3 = jnp.sqrt(jnp.absolute(1+4*A3*D3))*jnp.cos(theta3)
  C3 = jnp.sqrt(jnp.absolute(1+4*A3*D3))*jnp.sin(theta3)

  fitted_xcenter = -B3/(2*A3)
  fitted_ycenter = -C3/(2*A3)
  fitted_radius = 1/(2*jnp.abs(A3))
  return fitted_xcenter, fitted_ycenter, fitted_radius

def plot_circle(center_x, center_y, radius):
    theta = np.linspace(0, 2*np.pi, 1000)
    x = center_x + radius * np.cos(theta)
    y = center_y + radius * np.sin(theta)
    return x, y

def fit_errors(r, hessian):
    A3 = r[0]
    D3 = r[1]
    theta3 = r[2]

    A3_err = jnp.sqrt(jnp.absolute(hessian[0,0]))
    D3_err = jnp.sqrt(jnp.absolute(hessian[1,1]))
    theta3_err = jnp.sqrt(jnp.absolute(hessian[2,2]))

    fitted_xcenter_err = jnp.sqrt((A3_err ** 2 * (2 * A3 * D3 + 1) ** 2 * jnp.cos(theta3) ** 2)/(4 * A3 ** 4 * (4 * A3 * D3 + 1)) + 
                                    (theta3_err ** 2 * (4 * A3 * D3 + 1) * jnp.sin(theta3) ** 2) / (4 * A3 ** 2) + 
                                    (D3_err ** 2 * jnp.cos(theta3) ** 2) / (4 * A3 * D3 + 1))
    fitted_ycenter_err = jnp.sqrt((A3_err ** 2 * (2 * A3 * D3 + 1) ** 2 * jnp.sin(theta3) ** 2) / (4 * A3 ** 4 * (4 * A3 * D3 + 1)) + 
                                    (theta3_err ** 2 * (4 * A3 * D3 + 1) * jnp.cos(theta3) ** 2) / (4 * A3 ** 2) + 
                                    (D3_err ** 2 * jnp.sin(theta3) ** 2) / (4 * A3 * D3 + 1))
    fitted_radius_err = jnp.sqrt(A3 ** 2 * A3_err ** 2) / (2 * jnp.abs(A3) ** 3)

    return fitted_xcenter_err, fitted_ycenter_err, fitted_radius_err


def circle_residuals_jax(params, x, y, w, err):
    """Compute residuals for circle fitting using JAX."""
    A, D, theta = params
    err = strip_error* jnp.ones_like(x)
    P = A * (x**2 + y**2) + jnp.sqrt((1 + 4*A*D)) * (x * jnp.cos(theta) + y * jnp.sin(theta)) + D
       
    residuals = w * (2*P/(1+jnp.sqrt(1+4*A*P)))
    return jnp.sum(w*(residuals/err)**2)

def fit_circle_least_squares_jax(x, y, w, err, initial_params=None):
    """Fit a circle to points using JAX optimization."""
  

    if initial_params is None:
        # Use the centroid and average distance as initial parameters
        x0 = jnp.mean(x)
        y0 = jnp.mean(y)
        r = jnp.mean(jnp.sqrt((x - x0)**2 + (y - y0)**2))
        a0 = 1 / 2*r
        d0 = 0
        theta0 = 0 
        initial_params = jnp.array([a0, d0 ,theta0])

    # Define the cost function without calling it
    err = strip_error* jnp.ones_like(x)
    cost_function = lambda params: circle_residuals_jax(params, x, y, w, err)

    # Use JAX's minimize function with the L-BFGS-B optimizer
    solver = jaxopt.LBFGS(fun=cost_function, verbose=False)
    res = solver.run(initial_params)

    # Extract optimized parameters
    optimized_params = res.params

    # Print the value of the cost function with optimized parameters
    #print("Value of the cost function:", circle_residuals_jax(optimized_params, x, y, w, err))
    chisq = circle_residuals_jax(optimized_params, x, y, w, err)
    return optimized_params, chisq

def final_params(r):
  A3 = r[0]
  D3 = r[1]
  theta3 = r[2]

  B3 = jnp.sqrt(jnp.absolute(1+4*A3*D3))*jnp.cos(theta3)
  C3 = jnp.sqrt(jnp.absolute(1+4*A3*D3))*jnp.sin(theta3)

  fitted_xcenter = -B3/(2*A3)
  fitted_ycenter = -C3/(2*A3)
  fitted_radius = 1/(2*jnp.abs(A3))
  return fitted_xcenter, fitted_ycenter, fitted_radius


@jax.jit
def cluster_hits(input_graph, output_graph):
    coordinates = input_graph.nodes
    predictions = output_graph.nodes
    
    y_input = coordinates[:,0]
    z_input = coordinates[:,1]
    #all_weights = jnp.absolute(predictions).ravel() 
    all_weights = shifted_sigmoid(jnp.absolute(predictions)).ravel()
    real_weights = jnp.where(y_input != 5., all_weights, 0)
    
    layers = [2.495 , 2.505, 2.605, 2.705, 2.805, 4.205 , 4.305 ,4.405, 4.505]    
    clustered_y = []
    weights_layer_sum = []
    rms_y_layers = []

    y_input = y_input/1e2
    for i in range(len(layers)):

      weights_layer = jnp.where(z_input == layers[i], real_weights, 0)    
      y = jnp.where(z_input==layers[i], y_input, 0) 
      weights_layer = jnp.where(weights_layer == 1., 1, 0)
      weighted_y = weights_layer*y  

      sum_w = jnp.sum(weights_layer)        
      y_mean = jnp.sum(weighted_y) / sum_w

      clustered_y.append(y_mean)
      weights_layer_sum.append(sum_w)
      layer_rms = jnp.sqrt(jnp.sum((weights_layer*((y - y_mean)))**2) / jnp.sum(weights_layer))   
      layer_rms = jnp.where(jnp.nan_to_num(layer_rms) != 0, layer_rms, 1)
      rms_y_layers.append(layer_rms)
      
    rms_y_layers = jnp.asarray(rms_y_layers)
    return clustered_y, layers, weights_layer_sum, rms_y_layers


def MLP_input_preprocessing(MLP_arrays, batch_size):
    coordinates = []
    labels = []

    max = get_node_number(MLP_arrays, batch_size)[1]

    for i in range (0, batch_size):
        
        y = jnp.asarray(MLP_arrays[1][i])
        z = jnp.asarray(MLP_arrays[2][i])
        id = jnp.asarray(MLP_arrays[3][i])
        id = jnp.where(jnp.absolute(id) == 13, 1, 0)

        pad_y =  jnp.pad(y, (0, max - len(y)), mode='constant')
        pad_z =  jnp.pad(z, (0, max - len(z)), mode='constant')
      
        padded_y = jnp.where(pad_y!=0, pad_y, 5)
        padded_z = jnp.where(pad_y!=0, pad_z, 5)
        padded_id =  jnp.pad(id, (0, max - len(id)), mode='constant')

        #funzioni per clustering, estrapolazione 
        input_coords = jnp.stack((padded_y,padded_z), axis=-1)
        truth_labels = jnp.stack((padded_id), axis=-1) 

        coordinates.append(input_coords)
        labels.append(truth_labels)

    return jnp.asarray(coordinates), jnp.asarray(labels)


@jax.jit
def cartesian_to_polar_GAT(y_cluster, fitted_xcenter, fitted_ycenter):
    x = jnp.asarray(y_cluster)
    y = jnp.asarray(GAT_layers)
    dx = x - fitted_xcenter
    dy = y - fitted_ycenter

    theta = jnp.nan_to_num(jnp.arctan2(dy, dx))   
    theta_ref = jnp.arctan2(fitted_xcenter, fitted_ycenter)
    theta = jnp.where(theta < theta_ref, theta + 2*jnp.pi, theta)

    return jnp.sum(theta)/jnp.count_nonzero(theta), jnp.arctan2(dy, dx)

@jax.jit
def find_nearest_y_points(z_layer, y_layer, y_hits_layer, z_layers_hits, labels):
    num_points=5
    selected_y = []
    selected_z = []
    selected_labels = []
    selected_y_distances = []
    for i in range(len(z_layer)):
        z = z_layer[i]
        y = y_layer[i]   
        y_layer_hits = jnp.where((z_layers_hits == z), y_hits_layer, 5)
        y_diffs = y_layer_hits - y
        abs_y_diffs = jnp.absolute(y_diffs)
        sorted_indices = jnp.argsort(abs_y_diffs)

        sorted_array = y_layer_hits[sorted_indices]
        sorted_distances = y_diffs[sorted_indices]            
        
        sorted_label = labels[sorted_indices]

        selected_y_values = sorted_array[:num_points]
        selected_label = sorted_label[:num_points]      
        selected_z_values = z * jnp.ones_like(selected_y_values)
        selected_y_diffs = sorted_distances[:num_points]
       
        
        selected_y.append(selected_y_values)
        selected_z.append(selected_z_values)
        selected_labels.append(selected_label)
        selected_y_distances.append(selected_y_diffs)  
   

    return selected_y, selected_z, selected_labels, selected_y_distances

@jax.jit
def from_fit_to_mlp_input(MLP_coordinates, MLP_labels, fitted_xcenter, fitted_ycenter, fitted_radius, y_cluster):
    theta_mean, theta = jax.vmap(cartesian_to_polar_GAT)(y_cluster, fitted_xcenter, fitted_ycenter)
    extrapolated_theta_MLP_layers = jnp.arcsin((jnp.asarray(MLP_layers) - fitted_ycenter[:,jnp.newaxis]) / fitted_radius[:,jnp.newaxis])
    mean_extrap_theta_MLP_layers = jnp.mean(extrapolated_theta_MLP_layers, axis=1)
    theta_outer_layers = jnp.arcsin((jnp.asarray(outer_layers) - fitted_ycenter[:,jnp.newaxis]) / fitted_radius[:,jnp.newaxis])
    delta_theta_outer_layers = jnp.absolute(theta_outer_layers[:,0] - theta_outer_layers[:,1])
    good_happy_theta_inner_layers = jnp.where((jnp.absolute(mean_extrap_theta_MLP_layers - theta_mean) < 2 * delta_theta_outer_layers)[:,jnp.newaxis], extrapolated_theta_MLP_layers, jnp.pi - extrapolated_theta_MLP_layers)
    extrapolated_y_MLP_fromtheta = fitted_xcenter[:,jnp.newaxis] + fitted_radius[:,jnp.newaxis] * jnp.cos(good_happy_theta_inner_layers)  
    MLP_layers_vec = jnp.tile(jnp.asarray(MLP_layers), (batch_size, 1))
    y_mlp_input = MLP_coordinates[:,:,0]
    z_mlp_input = MLP_coordinates[:,:,1]
    mlp_labels = MLP_labels
   
    ymlp, zmlp, lmlp , ydiffmlp = jax.vmap(find_nearest_y_points)(MLP_layers_vec, extrapolated_y_MLP_fromtheta, y_mlp_input, z_mlp_input, mlp_labels)
    return ymlp, zmlp, lmlp, extrapolated_y_MLP_fromtheta, ydiffmlp

def final_MLP(x):
     return hk.nets.MLP([40, 70, 100,  200, 400, 600,400, 200, 100, 70, 40])(x)

@jax.jit
def update(params, opt_state, gr):
        """Returns updated params and state."""
        updates, opt_state = opt_update(gr, opt_state)
        return optax.apply_updates(params, updates), opt_state

@jax.jit
def UpdateWeights(weights,gradients):
    return weights - lr_mlp * gradients

graph = GetGraphs(arrays_training_GAT, batch_size)[0]
graph_truth = GetGraphs(arrays_training_GAT, batch_size)[1]
dummy_mlp_input = jnp.ones((batch_size, 40))

coords, labels = MLP_input_preprocessing(arrays_training_MLP, batch_size)


network = hk.without_apply_rng(hk.transform(hk.vmap(gat_definition, split_rng=False)))
params = gnn_params
opt_init, opt_update = optax.adam(lr)
opt_state = opt_init(params)

second_network = hk.transform(final_MLP)
rng2 = jax.random.PRNGKey(1234)
params_mlp = second_network.init(rng2, dummy_mlp_input)
#arams_mlp = mlp_params

def DataLoader(arrays_GAT, arrays_MLP, batch_size, *, key):
    dataset_size = arrays_GAT[0].shape[0]
    #print('n_events: ', dataset_size, ', batch_size: ', batch_size)
        
    (key,) = jrandom.split(key, 1)
    start = 0
    end = batch_size
    while end <= dataset_size:
        
        yield tuple(GetGraphs(arrays_GAT[:,start:end], batch_size)), tuple(MLP_input_preprocessing(arrays_MLP, batch_size))
        start = end
        end = start + batch_size



dataloader = DataLoader(arrays_training_GAT, arrays_training_MLP, batch_size, key = jrandom.PRNGKey(683))
it = itertools.tee(dataloader, steps)
gpus = jax.devices('gpu')

@jax.jit
def prediction_loss(params, input_graph, target_graph):
        

        output_graph = network.apply(params, input_graph)        
        input_mask = input_graph.nodes[:,:,0]    
        radius = jnp.reshape(output_graph.nodes, (output_graph.nodes.shape[0], output_graph.nodes.shape[1]))
        
        id_mask = target_graph.nodes

        loss = (radius - id_mask)**2
        loss2 = jnp.where(input_mask != 5, loss, 0)   
        
        return jnp.sum(loss2) / jnp.count_nonzero(loss2)

@jax.jit
def mlp_loss(second_params, mlp_coordinates, mlp_labels, c):    
    y_cords = c
    mlp_labels = jnp.reshape(mlp_labels, (batch_size, 5*8))    
    output_values = second_network.apply(second_params, rng2, mlp_coordinates)         
    diff = (output_values - mlp_labels)**2   
    good_fit_diff = jnp.where(y_cords != 5, diff, 0)
    

    return jnp.sum(jnp.nan_to_num(good_fit_diff)) /(jnp.count_nonzero(good_fit_diff) )

def mlp_loss_value(second_params, mlp_coordinates, mlp_labels, c):
    y_cords = c
    mlp_labels = jnp.reshape(mlp_labels, (batch_size, 5*8))
    output_values = second_network.apply(second_params, rng2, mlp_coordinates)  
   

    diff = (output_values - mlp_labels)**2
    
    good_fit_diff = jnp.where(y_cords != 5, diff, 0)
    return jnp.sum(jnp.nan_to_num(good_fit_diff)) /(jnp.count_nonzero(good_fit_diff) )


val_input_graph = GetGraphs(arrays_test_GAT, batch_size)[0]
val_graph_truth = GetGraphs(arrays_test_GAT, batch_size)[1]
val_coords, val_labels = MLP_input_preprocessing(arrays_test_MLP, batch_size)



print('Training')
import time 
start2 = time.time()

loss_gat_list = []
loss_mlp_list = []
loss_val_mlp_list = []
k = 0
for step in range(steps):
       
        epoch_loss = 0
        mlp_epoch_loss = 0
        epoch_loss_fit = 0
        val_mlp_epoch_loss = 0
        iter_data = it[step]
    
        for g in iter_data:
            start = time.time()
            input_graph = g[0][0]
            truth_graph = g[0][1]
            MLP_coordinates = g[1][0]
            MLP_labels = g[1][1]

            output_graph = network.apply(gnn_params, input_graph)      
            y_cluster, z_cluster , w_cluster , rms_y = jax.vmap(cluster_hits)(input_graph, output_graph)
            y_cluster = jnp.array(y_cluster).T
            z_cluster = jnp.array(z_cluster).T
            w_cluster = jnp.array(w_cluster).T

            error_y = rms_y
            w_cluster = jnp.where(w_cluster >= 1, 1, 0)
            y_cluster = jnp.nan_to_num(y_cluster)
            
            optimized_params, chisq = jax.vmap(fit_circle_least_squares_jax)(y_cluster, z_cluster, w_cluster, error_y)    
            xc , yc, R = final_params(optimized_params.T)
            MLP_coordinates = MLP_input_preprocessing(arrays_test_MLP, batch_size)[0]
            MLP_labels = MLP_input_preprocessing(arrays_test_MLP, batch_size)[1]
            ymlp, zmlp, lmlp, extr_y_mlp, y_diff_mlp = from_fit_to_mlp_input(MLP_coordinates, MLP_labels, xc, yc, R, y_cluster)
            ymlp = jnp.asarray(ymlp).transpose(1,0,2)  #(batch, 8, 5)
            zmlp = jnp.asarray(zmlp).transpose(1,0,2)
            lmlp = jnp.asarray(lmlp).transpose(1,0,2)
            y_diff_mlp = jnp.asarray(y_diff_mlp).transpose(1,0,2)
            extr_y_mlp = extr_y_mlp[:,:,jnp.newaxis]

            ymlp = jnp.reshape(ymlp, (ymlp.shape[0], 40))  
            y_diff_mlp = jnp.reshape(y_diff_mlp, (y_diff_mlp.shape[0], 40))     
            mlp_input_vector = y_diff_mlp*1e2
            
            loss_mlp = mlp_loss_value(params_mlp, mlp_input_vector, lmlp, ymlp)
            gr_mlp= jax.grad(mlp_loss, argnums=0)(params_mlp, mlp_input_vector, lmlp, ymlp)            
            params_mlp = jax.tree_map(UpdateWeights, params_mlp, gr_mlp)            
            mlp_epoch_loss = loss_mlp
            
               
            loss_val = 0                       
            
            val_output_graph = network.apply(gnn_params, val_input_graph)
            y_cluster_val , z_cluster_val, w_cluster_val, rms_y_val = jax.vmap(cluster_hits)(val_input_graph, val_output_graph)
            y_cluster_val = jnp.array(y_cluster_val).T
            z_cluster_val = jnp.array(z_cluster_val).T
            w_cluster_val = jnp.array(w_cluster_val).T

            error_y_val = rms_y_val
            w_cluster_val = jnp.where(w_cluster_val >=1, 1, 0)
            y_cluster_val = jnp.nan_to_num(y_cluster_val)

            optimized_params_val, chisq_val = fit_circle_least_squares_jax(y_cluster_val, z_cluster_val, w_cluster_val, error_y_val)
            xc_val, yc_val, R_val = final_params(optimized_params_val.T)

            ymlp_val, zmlp_val, lmlp_val, extr_y_mlp_val, y_diff_mlp_val = from_fit_to_mlp_input(val_coords, val_labels, xc_val, yc_val, R_val, y_cluster_val)
            ymlp_val = jnp.asarray(ymlp_val).transpose(1,0,2)
            zmlp_val = jnp.asarray(zmlp_val).transpose(1,0,2)
            lmlp_val = jnp.asarray(lmlp_val).transpose(1,0,2)
            y_diff_mlp_val = jnp.asarray(y_diff_mlp_val).transpose(1,0,2)
            extr_y_mlp_val = extr_y_mlp_val[:,:,jnp.newaxis]
            
            ymlp_val = jnp.reshape(ymlp_val, (ymlp_val.shape[0], 40))
            y_diff_mlp_val = jnp.reshape(y_diff_mlp_val, (y_diff_mlp_val.shape[0], 40))
            mlp_input_vector_val = y_diff_mlp_val*1e2

            loss_mlp_val = mlp_loss_value(params_mlp, mlp_input_vector_val, lmlp_val, ymlp_val)
            val_mlp_epoch_loss = loss_mlp_val
            

            end = time.time()               
            

        #mlp_params_path = '/home/lrambelli/gnn-differentiable-patternreco/training_mlp_params/FIT2_params_gnn_mlp_epoch_' + str(step) + '_loss_' + str(mlp_epoch_loss) + '_val_loss_' + str(val_mlp_epoch_loss) + '.pkl'
        #save(params_mlp, mlp_params_path)
        print('--------------------->  STEP: ', step, 'MLP LOSS: ', mlp_epoch_loss, ' VAL MLP LOSS: ', val_mlp_epoch_loss, ' TIME: ', end-start)
        loss_val_mlp_list.append(val_mlp_epoch_loss)
        loss_gat_list.append(epoch_loss)
        loss_mlp_list.append(mlp_epoch_loss)

end2 = time.time()
training_time = end2 - start2
print('Training time:  ', training_time)

import matplotlib.pyplot as plt 
plt.figure(figsize=(7, 5), dpi=100)
xrange = np.arange(0,steps,1)
plt.plot(xrange, np.asarray(loss_mlp_list), label = 'MLP training loss')
plt.plot(xrange, np.asarray(loss_val_mlp_list), label = 'MLP validation loss')
plt.xlabel('epochs')
plt.ylabel('value')
plt.legend()
#plt.savefig('loss_gnn_fit2.png')

