import numpy as np
import jax.numpy as jnp
import jraph
import jax.random as jrandom
import jax.tree_util as tree
import jax
import haiku as hk
from typing import Any, Callable, Dict, List, Optional, Tuple
import os
import matplotlib.pyplot as plt 
from jraph._src import models
import jaxopt
from fromGATtoGAT import ManipulateGraph, Methods4Fit
from jax.experimental.host_callback import call
from jax.lib import xla_bridge
import pickle
from mpl_toolkits.axes_grid1.inset_locator import inset_axes


from utility_functions import get_node_number, get_nodes_edges_per_event, create_senders_receivers, decorate_nodes, decorate_nodes_truth, GetGraphs, add_self_edges_fn, GAT, gat_definition, build_toy_graph, load, save
from utility_functions import circle_residuals_jax, get_point_at_y, find_nearest_y_points, findIntersection, findCircle, pre_fit_estimate, xyr_to_adtheta, fit_circle_least_squares_jax, final_params, cartesian_to_polar_GAT
from utility_functions import find_nearest_y_points, from_fit_to_mlp_input

print('Using platform: ', xla_bridge.get_backend().platform)
os.environ["XLA_PYTHON_CLIENT_PREALLOCATE"]="false"
os.environ["XLA_PYTHON_CLIENT_MEM_FRACTION"]=".10"
os.environ["XLA_PYTHON_CLIENT_ALLOCATOR"]="platform"

arrays_OUTER = np.load('/home/lrambelli/patternreco/arrays_layer_nhit.npy', allow_pickle = True)
X_nhit = arrays_OUTER[0]  #96493
Y_nhit = arrays_OUTER[1]*1e2
Z_nhit = arrays_OUTER[2]
ID_nhit = arrays_OUTER[3]

arrays_INNER = np.load('arrays_layer_nhit_internal.npy', allow_pickle = True)

X_tot = arrays_INNER[0]
Y_tot = arrays_INNER[1]
Z_tot = arrays_INNER[2]
ID_tot = arrays_INNER[3]

arrays_PT = np.load('/home/lrambelli/gnn-differentiable-patternreco/arrays_layer_nhit_tot_pt.npy', allow_pickle=True)


GAT_layers = [2.495 , 2.505, 2.605, 2.705, 2.805, 4.205 , 4.305 ,4.405, 4.505]
MLP_layers = [0.405, 0.505, 0.605, 0.705, 0.805, 0.905, 1.005, 1.105]
outer_layers = [0.405, 4.505]

N = 2

network = hk.without_apply_rng(hk.transform(hk.vmap(gat_definition, split_rng=False)))
#params = load('/home/lrambelli/gnn-differentiable-patternreco/params_gatinner_pretrain/gnnonly_params_101_loss_0.19401592781049432_val_loss_0.2021955484292239.pkl')
#params_20 = load('/home/lrambelli/gnn-differentiable-patternreco/params_gatinner_pretrain/B_gnnonly_params_21_loss_0.09892715224139666_val_loss_0.11726533009577544.pkl')
params = load('/home/lrambelli/gnn-differentiable-patternreco/params_gatinner_pretrain/B_gnnonly_params_40_loss_0.09731606524884155_val_loss_0.11444035825595064.pkl')

def shifted_sigmoid(x):
    return 1 / (1 + jnp.exp(-100 * (x - 0.45)))

def cluster_hits_inner(input_graph, output_graph):
    coordinates = input_graph.nodes
    predictions = output_graph.nodes
    y_input = coordinates[:,:,0]
    z_input = coordinates[:,:,1]
    all_weights = jnp.absolute(predictions).ravel() 
    
    all_weights = shifted_sigmoid(jnp.absolute(predictions)).ravel()
    real_weights = jnp.where(y_input.ravel() != 500., all_weights.ravel(), 0)
    layers = [0.405, 0.505, 0.605, 0.705, 0.805, 0.905, 1.005, 1.105]    
    clustered_y = []
    weights_layer_sum = []
    rms_y_layers = []

    y_input = y_input/1e2
    z_input = z_input.ravel()
    y_input = y_input.ravel()
    real_weights = real_weights.ravel()
    for i in range(len(layers)):
        weights_layer = jnp.where(z_input == layers[i], real_weights, 0)    
        y = jnp.where(z_input==layers[i], y_input, 0) 
        #weights_layer = jnp.where(weights_layer == 1., 1, 0)
        weighted_y = weights_layer*y  

        sum_w = jnp.sum(weights_layer)        
        y_mean = jnp.sum(weighted_y) / sum_w

        clustered_y.append(y_mean)
        weights_layer_sum.append(sum_w)
        layer_rms = jnp.sqrt(jnp.sum((weights_layer*((y - y_mean)))**2) / jnp.sum(weights_layer))   
        layer_rms = jnp.where(jnp.nan_to_num(layer_rms) != 0, layer_rms, 1)
        rms_y_layers.append(layer_rms)
        
    rms_y_layers = jnp.asarray(rms_y_layers)
    return clustered_y, layers, weights_layer_sum, rms_y_layers




#write a function that takes as input the inner_coords and labels 
#and count for each layer z how many signal and background hits are present

def count_hits(count, inner_coords, labels):
    layers = [0.405, 0.505, 0.605, 0.705, 0.805, 0.905, 1.005, 1.105]
    y = inner_coords[:,:,0]
    z = inner_coords[:,:,1]
    label = labels
    #print(y.shape, z.shape, label.shape)
    signal_hits = []
    bkg_hits = []
    total_hits = []

    for i in range(len(layers)):
        y_layer = y[z == layers[i]]
        label_layer = label[z == layers[i]]
        total_hits.append(len(y_layer))
        signal_hits.append(len(y_layer[label_layer == 1]))
        bkg_hits.append(len(y_layer[label_layer == 0]))
    
    # Check for the anomaly condition
    for i in range(len(layers)):
        if signal_hits[i] + bkg_hits[i] < 5:
            print(f"Anomaly in event {count}: Layer {i} has {signal_hits[i]} signal hits and {bkg_hits[i]} background hits (total: {signal_hits[i] + bkg_hits[i]})")


    return signal_hits, bkg_hits, total_hits

def predictions_vs_distance(count, inner_y, inner_z, labels, predictions):
    layers = [0.405, 0.505, 0.605, 0.705, 0.805, 0.905, 1.005, 1.105]
    y = inner_y
    z = inner_z
    label = labels
    prediction = predictions
    #print(y.shape, z.shape, label.shape)
    bkg_d = []
    bkg_pred = []
    truth = []

    for i in range(len(layers)):
        y_layer = y[z == layers[i]]
        label_layer = label[z == layers[i]]
        prediction_layer = prediction[z == layers[i]]

        y_layer_signal = y_layer[label_layer == 1]
        y_layer_signal_mean = jnp.mean(y_layer_signal)

        distances = jnp.abs(y_layer - y_layer_signal_mean)
        prediction_layer = prediction_layer.ravel()   
        distances = distances.ravel()

        bkg_d.append(distances)
        bkg_pred.append(prediction_layer)
        truth.append(label_layer)
                
    return bkg_d, bkg_pred, truth

mu_outer = []
bkg_outer = []

mu_inner = []
bkg_inner = []

n_signal_hits_layer_z = []
n_bkg_hits_layer_z = []
n_total_hits_layer_z = []

pt_truth_value = []
pt_1st_fit = []
pt_2nd_fit = []

bkg_predictions = []
bkg_distances = []
truth_info = []

for i in range(0,N):
    start = 10000+i 
    end = start+1
    arrays_test_outer = np.asarray([X_nhit[start:end], Y_nhit[start:end], Z_nhit[start:end], ID_nhit[start:end]])
    arrays_test_inner = np.asarray([X_tot[start:end], Y_tot[start:end], Z_tot[start:end], ID_tot[start:end]])
    pt_truth = np.asarray(arrays_PT[4][start:end])/1e3
    graph_outer = GetGraphs(arrays_test_outer, 1)[0]
    graph_truth_outer = GetGraphs(arrays_test_outer, 1)[1]
    dummy_graph = build_toy_graph(1,40)

    output_graph_outer = network.apply(params, graph_outer, dummy_graph)[0]
    input_mask_outer = graph_outer.nodes[:,:,0]
    prediction_outer = jnp.reshape(output_graph_outer.nodes, (output_graph_outer.nodes.shape[0], output_graph_outer.nodes.shape[1]))
    
    prediction_outer_ravel = prediction_outer.ravel()
    input_mask_outer_ravel = input_mask_outer.ravel()
    truth_label_outer_ravel = graph_truth_outer.nodes.ravel()

    for i in range(len(prediction_outer_ravel)):
        if input_mask_outer_ravel[i] != 5:
            if truth_label_outer_ravel[i] == 1:
                mu_outer.append(prediction_outer_ravel[i])
            if truth_label_outer_ravel[i] == 0:
                bkg_outer.append(prediction_outer_ravel[i])

    y_cluster_outer , z_cluster_outer, w_cluster_outer, error_cluster_outer = jax.vmap(ManipulateGraph.cluster_hits)(graph_outer, output_graph_outer)
    y_cluster_outer = jnp.array(y_cluster_outer).T
    z_cluster_outer = jnp.array(z_cluster_outer).T
    w_cluster_outer = jnp.array(w_cluster_outer).T


    w_cluster_outer = jnp.where(w_cluster_outer >= 1, 1, 0)
    y_cluster_outer = jnp.nan_to_num(y_cluster_outer)

    outer_fit_params, chisq = jax.vmap(Methods4Fit.fit_circle_least_squares_jax)(y_cluster_outer, z_cluster_outer, w_cluster_outer, error_cluster_outer)
    xc_outer, yc_outer, r_outer = jax.vmap(Methods4Fit.final_params)(outer_fit_params)

    inner_coords, inner_labels = ManipulateGraph.inner_input_preprocessing(arrays_test_inner, 1)
    print(inner_coords.shape, inner_labels.shape)
    y_inner, z_inner, label_inner, extrap_inner, deltay_inner = from_fit_to_mlp_input(inner_coords, inner_labels, xc_outer, yc_outer, r_outer, y_cluster_outer, 1)
    '''
    s, b, tot = count_hits(i, inner_coords, inner_labels)
    n_signal_hits_layer_z.append(s)
    n_bkg_hits_layer_z.append(b)
    n_total_hits_layer_z.append(tot)
    '''

    
    
    y_inner = jnp.asarray(y_inner).transpose(1,0,2)
    z_inner = jnp.asarray(z_inner).transpose(1,0,2)
    label_inner = jnp.asarray(label_inner).transpose(1,0,2)
    deltay_inner = jnp.asarray(deltay_inner).transpose(1,0,2)
    z_layers = jnp.asarray([0.405, 0.505, 0.605, 0.705, 0.805, 0.905, 1.005, 1.105])

    inner_graph_coords = jnp.stack((y_inner*1e2, z_inner, deltay_inner), axis=-1)
    graph_inner = dummy_graph._replace(nodes = inner_graph_coords)
    output_graph_inner = network.apply(params, graph_outer, graph_inner)[1]
    '''
    d, p, t = predictions_vs_distance(i, y_inner, z_inner, label_inner, output_graph_inner.nodes)
    bkg_distances.extend(d)
    bkg_predictions.extend(p)
    truth_info.extend(t)
    '''


    prediction_inner_ravel = output_graph_inner.nodes.ravel()
    truth_label_graph_inner_ravel = label_inner.ravel()


    for i in range(len(prediction_inner_ravel)):
        if truth_label_graph_inner_ravel[i] == 1:
            mu_inner.append(prediction_inner_ravel[i])
        if truth_label_graph_inner_ravel[i] == 0:
            bkg_inner.append(prediction_inner_ravel[i])

    
    #plotting event displays
    y_outer_hits = graph_outer.nodes[:,:,0]
    z_outer_hits = graph_outer.nodes[:,:,1]
    label_outer_hits = graph_truth_outer.nodes[:]

    y_inner_hits = graph_inner.nodes[:,:,:,0]
    z_inner_hits = graph_inner.nodes[:,:,:,1]
    label_inner_hits = label_inner

    theta = jnp.linspace(0, 2*jnp.pi, 3000)
    x_1st = r_outer*jnp.cos(theta) + xc_outer
    y_1st = r_outer*jnp.sin(theta) + yc_outer
    
    y_cluster_inner, z_cluster_inner, w_cluster_inner, error_cluster_inner = jax.vmap(cluster_hits_inner)(graph_inner, output_graph_inner)
    y_cluster_inner = jnp.array(y_cluster_inner).T
    z_cluster_inner = jnp.array(z_cluster_inner).T
    w_cluster_inner = jnp.array(w_cluster_inner).T



    w_cluster_inner = jnp.where(w_cluster_inner >= 1, 1, 0)
    y_cluster_inner = jnp.nan_to_num(y_cluster_inner)

    y_cluster_total = jnp.concatenate((y_cluster_outer, y_cluster_inner), axis=1)
    z_cluster_total = jnp.concatenate((z_cluster_outer, z_cluster_inner), axis=1)
    w_cluster_total = jnp.concatenate((w_cluster_outer, w_cluster_inner), axis=1)
    error_cluster_total = jnp.concatenate((error_cluster_outer, error_cluster_inner), axis=1)

    total_fit_params, chisq_total = jax.vmap(Methods4Fit.fit_circle_least_squares_jax)(y_cluster_total, z_cluster_total, w_cluster_total, error_cluster_total, initial_params = outer_fit_params)
    xc_total, yc_total, r_total = jax.vmap(Methods4Fit.final_params)(total_fit_params)

    x_2nd = r_total*jnp.cos(theta) + xc_total
    y_2nd = r_total*jnp.sin(theta) + yc_total

    pt_truth_value.append(pt_truth)
    pt_1st_fit.append(0.3*r_outer)
    pt_2nd_fit.append(0.3*r_total)
    print('Event: ', start, 'pt_truth: ', pt_truth, 'pt_1st_fit: ', 0.3*r_outer, 'pt_2nd_fit: ', 0.3*r_total)
    
    
    clustered_outer_for_plot_y = y_cluster_outer[y_cluster_outer != 0]
    clustered_outer_for_plot_z = z_cluster_outer[y_cluster_outer != 0]

    clustered_inner_for_plot_y = y_cluster_inner[y_cluster_inner != 0]
    clustered_inner_for_plot_z = z_cluster_inner[y_cluster_inner != 0]
   
  
    outer_signal_y = y_outer_hits[label_outer_hits == 1]
    outer_signal_z = z_outer_hits[label_outer_hits == 1]
    outer_bkg_y = y_outer_hits[label_outer_hits == 0]
    outer_bkg_z = z_outer_hits[label_outer_hits == 0]

    y_inner_all = (inner_coords[:,:,0]).ravel()
    y_inner_all = np.asarray(y_inner_all)*1e2
    z_inner_all = inner_coords[:,:,1].ravel()
    label_inner_all = inner_labels.ravel()

    inner_signal_y = y_inner_all[label_inner_all == 1]
    inner_signal_z = z_inner_all[label_inner_all == 1]
    inner_bkg_y = y_inner_all[label_inner_all == 0]
    inner_bkg_z = z_inner_all[label_inner_all == 0]
    
    total_signal_y = jnp.concatenate((outer_signal_y, inner_signal_y), axis=0)
    total_signal_z = jnp.concatenate((outer_signal_z, inner_signal_z), axis=0)
    total_bkg_y = jnp.concatenate((outer_bkg_y, inner_bkg_y), axis=0)
    total_bkg_z = jnp.concatenate((outer_bkg_z, inner_bkg_z), axis=0)
    
    '''
    #create a figure composed of two plots 
    fig, axs = plt.subplots(1, 2, figsize=(12, 5), gridspec_kw={'width_ratios': [1, 2]})


    axs[0].scatter(total_signal_y.ravel()/1e2, total_signal_z.ravel(), c='yellow', label='Signal Hits')
    axs[0].scatter(total_bkg_y.ravel()/1e2, total_bkg_z.ravel(), c='purple', label='Background Hits')

    #axs[0].scatter(y_outer_hits.ravel()/1e2, z_outer_hits.ravel(), c=label_outer_hits.ravel(), cmap='viridis', label='Truth outer info')
    #axs[0].scatter(y_inner_hits.ravel()/1e2, z_inner_hits.ravel(), c=label_inner_hits.ravel(), cmap='viridis', label='Truth inner info')
    axs[0].legend(fontsize=10)
    axs[0].set_xlabel('Y Coord', fontsize=10)
    axs[0].set_ylabel('Z Layer Coord', fontsize=10)
    axs[0].set_xlim(-0.4, 0.4)
    axs[0].set_ylim(0, 5)
    axs[0].set_title('Event Display with Truth Labels')

    
    # Plotting on the second axis
    #axs[1].scatter(y_outer_hits.ravel()/1e2, z_outer_hits.ravel(), c=prediction_outer.ravel(), cmap='viridis', label='Prediction outer info', alpha=0.5)
    #axs[1].scatter(y_inner_hits.ravel()/1e2, z_inner_hits.ravel(), c=prediction_inner_ravel, cmap='viridis', label='Prediction inner info', alpha=0.5)
    #axs[1].scatter(y_cluster_outer.ravel(), z_cluster_outer.ravel(), c='black', label='Clustered Outer Hits')
    sc3 = axs[1].scatter(y_outer_hits.ravel()/1e2, z_outer_hits.ravel(), c=prediction_outer.ravel(), cmap='viridis')
    sc4 = axs[1].scatter(y_inner_hits.ravel()/1e2, z_inner_hits.ravel(), c=prediction_inner_ravel, cmap='viridis', label='Predicted Hits', marker='o')
    #axs[1].scatter(y_cluster_outer.ravel(), z_cluster_outer.ravel(), c='black', s=20, label='Clustered Outer Hits')
    axs[1].scatter(clustered_outer_for_plot_y, clustered_outer_for_plot_z, marker = 'D', c='red', s=18, label='Clustered Hits')
    axs[1].plot(x_1st, y_1st, color='black', label='1st Circle Fit', linestyle='--')
    axs[1].scatter(extrap_inner.ravel(), z_layers, c='black', marker='x', label='Extrapolated Hits')
    #axs[1].scatter(y_cluster_inner.ravel(), z_cluster_inner.ravel(), c='red', label='Clustered Inner Hits')
    axs[1].scatter(clustered_inner_for_plot_y, clustered_inner_for_plot_z, c='red', marker = 'D', s=18)
    axs[1].plot(x_2nd, y_2nd, color='black', label='2nd Circle Fit')
   
    axs[1].legend(fontsize=10)
    axs[1].set_xlabel('Y Coord', fontsize=10)
    axs[1].set_ylabel('Z Layer Coord', fontsize=10)
    axs[1].set_xlim(-0.4, 0.4)
    axs[1].set_ylim(0, 5)
    axs[1].set_title('Event Display with Predicted Labels')
    cbar2 = plt.colorbar(sc3, ax=axs[1])
    cbar2.set_label('Prediction outer info', fontsize=10)  

    # Adding zoomed inset
    axins = inset_axes(axs[1], width="40%", height="40%", loc='upper right', borderpad=0.5)
    axins.scatter(y_outer_hits.ravel()/1e2, z_outer_hits.ravel(), c=prediction_outer.ravel(), cmap='viridis')
    axins.scatter(y_inner_hits.ravel()/1e2, z_inner_hits.ravel(), c=prediction_inner_ravel, cmap='viridis', marker='o')
    axins.scatter(clustered_outer_for_plot_y, clustered_outer_for_plot_z, marker='D', c='red', s=18)
    axins.plot(x_1st, y_1st, color='black', linestyle='--')
    axins.scatter(extrap_inner.ravel(), z_layers, c='black', marker='x')
    axins.scatter(clustered_inner_for_plot_y, clustered_inner_for_plot_z, c='red', marker='D', s=18)
    axins.plot(x_2nd, y_2nd, color='black')
    axins.set_ylim(0.4, 1.15)
    axins.set_xlim(0, 0.05)

    #plt.tight_layout()
    plt.savefig('./test_plots_tot/event_display_final/event_display_'+str(start)+'.png', dpi=800)
    plt.close()
    '''
    
    # First plot: Signal and Background Hits
    fig, ax = plt.subplots(figsize=(12, 10))
    ax.scatter(total_signal_y.ravel() / 1e2, total_signal_z.ravel(), c='yellow', label='Signal Hits', s = 150)
    ax.scatter(total_bkg_y.ravel() / 1e2, total_bkg_z.ravel(), c='purple', label='Background Hits', s = 150)
    ax.legend(fontsize=25)
    ax.set_xlabel('Y Coord', fontsize=25)
    ax.set_ylabel('Z Layer Coord', fontsize=25)
    ax.set_xlim(-0.4, 0.4)
    ax.set_ylim(0, 5)
    #enlarge numbers on axes
    ax.tick_params(axis='both', which='major', labelsize=25)
    ax.set_title('Event Display with Truth Labels', fontsize=25)
    plt.savefig('./test_plots_tot/final_event_displays/event_' + str(start) + '_truth_40.png', dpi=800)
    plt.close()

    # Second plot: Predicted Labels
    fig, ax = plt.subplots(figsize=(15, 10))
    sc3 = ax.scatter(y_outer_hits.ravel() / 1e2, z_outer_hits.ravel(), c=prediction_outer.ravel(), cmap='viridis', s = 150)
    sc4 = ax.scatter(y_inner_hits.ravel() / 1e2, z_inner_hits.ravel(), c=prediction_inner_ravel, cmap='viridis', label='Predicted Hits', marker='o', s=150)
    ax.scatter(clustered_outer_for_plot_y, clustered_outer_for_plot_z, marker='D', c='red', s=100, label='Clustered Hits')
    ax.plot(x_1st, y_1st, color='black', label='1st Circle Fit', linestyle='--', linewidth=4)
    ax.scatter(extrap_inner.ravel(), z_layers, c='black', marker='x', label='Extrapolated Hits', s = 200)
    ax.scatter(clustered_inner_for_plot_y, clustered_inner_for_plot_z, c='red', marker='D', s=100)
    ax.plot(x_2nd, y_2nd, color='black', label='2nd Circle Fit', linewidth=4)
    ax.legend(fontsize=25)
    ax.set_xlabel('Y Coord', fontsize=25)
    ax.set_ylabel('Z Layer Coord', fontsize=25)
    ax.set_xlim(-0.4, 0.4)
    ax.set_ylim(0, 5)
    ax.set_title('Event Display with Predicted Labels', fontsize=25)
    ax.tick_params(axis='both', which='major', labelsize=25)
    cbar2 = plt.colorbar(sc3, ax=ax)
    cbar2.ax.tick_params(labelsize=25)
    cbar2.set_label('Prediction outer info', fontsize=25)
    plt.savefig('./test_plots_tot/final_event_displays/event_' + str(start) + '_predicted_40.png', dpi=800)
    plt.close()

    # Third plot: Zoomed Inset
    fig, ax = plt.subplots(figsize=(12, 10))
    ax.scatter(y_outer_hits.ravel() / 1e2, z_outer_hits.ravel(), c=prediction_outer.ravel(), cmap='viridis', s = 150)
    ax.scatter(y_inner_hits.ravel() / 1e2, z_inner_hits.ravel(), c=prediction_inner_ravel, cmap='viridis', marker='o', s = 150)
    ax.scatter(clustered_outer_for_plot_y, clustered_outer_for_plot_z, marker='D', c='red', s=100)
    ax.plot(x_1st, y_1st, color='black', linestyle='--', linewidth=4)
    ax.scatter(extrap_inner.ravel(), z_layers, c='black', marker='x', s=200)
    ax.scatter(clustered_inner_for_plot_y, clustered_inner_for_plot_z, c='red', marker='D', s=100)
    ax.plot(x_2nd, y_2nd, color='black', linewidth=4)
    ax.set_ylim(0.4, 1.15)
    ax.set_xlim(0, 0.05)
    ax.set_xlabel('Y Coord', fontsize=25)
    ax.set_ylabel('Z Layer Coord', fontsize=25)
    ax.tick_params(axis='both', which='major', labelsize=25)
    plt.savefig('./test_plots_tot/final_event_displays/event_' + str(start) + '_zoom_40.png', dpi=800)
    plt.close()
    

'''
bkg_distances = np.asarray(bkg_distances).ravel()
bkg_predictions = np.asarray(bkg_predictions).ravel()
truth_info = np.asarray(truth_info).ravel()

print(bkg_distances.shape, bkg_predictions.shape)

plt.figure()
plt.scatter(bkg_distances.ravel(), bkg_predictions.ravel(), c=truth_info.ravel(), cmap='viridis', alpha=0.5)
plt.axhline(y=1, color='black', linestyle='--')
plt.colorbar()
plt.xlabel('Distance to Signal Hit')
plt.ylabel('Prediction')
plt.title('Hits Predictions vs Distance to Signal Hit')
plt.savefig('./test_plots_tot/bkg_predictions_vs_distance.png', dpi=600)
'''

'''
n_bkg_hits_layer_z = np.array(n_bkg_hits_layer_z)  # Convert list to NumPy array
n_signal_hits_layer_z = np.array(n_signal_hits_layer_z)  # Convert list to NumPy array
n_total_hits_layer_z = np.array(n_total_hits_layer_z)

print(n_bkg_hits_layer_z.shape, n_signal_hits_layer_z.shape, n_total_hits_layer_z.shape)
print(n_bkg_hits_layer_z[:2], n_signal_hits_layer_z[:2], n_total_hits_layer_z[:2])

plt.figure(figsize=(10,5))
#plt.hist(n_total_hits_layer_z.ravel(), bins=50, range=[0, 50], alpha=0.3, color='grey', label='total')
plt.hist(n_bkg_hits_layer_z.ravel(), bins=50, range=[0, 50], alpha=0.5, label='bkg')
plt.hist(n_signal_hits_layer_z.ravel(), bins=50, range=[0, 50], alpha=0.5, label='mu')
#add vertival line to 5
plt.axvline(x=5, color='black', linestyle='--')

plt.xlabel('Number of hits per layer')
plt.ylabel('Counts')
plt.legend()
plt.savefig('./test_plots_tot/hits_per_layer.png')

plt.figure()
plt.hist(n_total_hits_layer_z.ravel(), bins=50, range=[0, 50], alpha=0.3, color='grey', label='total')
plt.hist(n_bkg_hits_layer_z.ravel(), bins=50, range=[0, 50], alpha=0.5, label='bkg')
plt.hist(n_signal_hits_layer_z.ravel(), bins=50, range=[0, 50], alpha=0.5, label='mu')

plt.xlabel('Number of hits per layer')
plt.ylabel('Counts')
plt.legend()
plt.savefig('./test_plots_tot/hits_per_layer_tot.png')



plt.figure()
plt.hist(mu_outer, bins=50, range = [0,1], alpha=0.5, label='mu')
plt.hist(bkg_outer, bins=50, range = [0,1], alpha=0.5, label='bkg')
plt.legend()
plt.savefig('./test_plots_tot/outer_predictions.png')
'''

'''
plt.figure()
#plot residuals of truth and 1st pt
pt_truth_value = np.array(pt_truth_value)
pt_1st_fit = np.array(pt_1st_fit)
pt_2nd_fit = np.array(pt_2nd_fit)

residuals_1st = pt_truth_value - pt_1st_fit
residuals_2nd = pt_truth_value - pt_2nd_fit

plt.hist(residuals_1st, bins=50, alpha=0.5, label='1st fit')
plt.hist(residuals_2nd, bins=50, alpha=0.5, label='2nd fit')
plt.xlabel('pT Residuals')
plt.ylabel('Counts')
plt.legend()
plt.yscale('log')
plt.savefig('./test_plots_tot/pt_residuals.png')
'''

'''
plt.figure()
plt.hist(mu_inner, bins=50, range = [0,1], alpha=0.5, label='mu')
plt.hist(bkg_inner, bins=50, range = [0,1], alpha=0.5, label='bkg')
plt.legend()
plt.savefig('./test_plots_tot/inner_predictions_epoc40.png')
'''

#plot outer and inner predictions 
plt.figure()
plt.hist(mu_outer, bins=50, range=[0, 1], alpha=0.5, label='mu_outer')
plt.hist(bkg_outer, bins=50, range=[0, 1], alpha=0.5, label='bkg_outer')
plt.hist(mu_inner, bins=50, range=[0, 1], histtype='step', label='mu_inner', edgecolor='black', linewidth=1.2)
plt.hist(bkg_inner, bins=50, range=[0, 1], histtype='step', label='bkg_inner', edgecolor='black', linewidth=1.2)
plt.legend()
plt.yscale('log')
plt.xlabel('Predicted values')
plt.ylabel('Entries')
plt.savefig('./test_plots_tot/outer_inner_predictions.png')

'''
def compute_roc_curve(signal, background):
    # Concatenate signal and background arrays
    y_true = np.concatenate([np.ones_like(signal), np.zeros_like(background)])
    scores = np.concatenate([signal, background])

    # Sort scores and corresponding true labels
    desc_score_indices = np.argsort(scores)[::-1]
    y_true = y_true[desc_score_indices]
    scores = scores[desc_score_indices]

    # Calculate True Positive Rate and False Positive Rate
    tpr = np.cumsum(y_true) / np.sum(y_true)
    fpr = np.cumsum(1 - y_true) / np.sum(1 - y_true)

    # Insert initial point (0, 0)
    tpr = np.insert(tpr, 0, 0)
    fpr = np.insert(fpr, 0, 0)
    thresholds = np.insert(scores, 0, scores[0] + 1)

    return fpr, tpr, thresholds

def compute_auc(fpr, tpr):
    # Compute Area Under the Curve using the trapezoidal rule
    return np.trapz(tpr, fpr)


fpr_outer, tpr_outer, thresholds_outer = compute_roc_curve(mu_outer, bkg_outer)
roc_auc_outer = compute_auc(fpr_outer, tpr_outer)

fpr_inner, tpr_inner, thresholds_inner = compute_roc_curve(mu_inner, bkg_inner)
roc_auc_inner = compute_auc(fpr_inner, tpr_inner)

fpr_inner_20, tpr_inner_20, thresholds_inner_20 = compute_roc_curve(mu_inner_20, bkg_inner_20)
roc_auc_inner_20 = compute_auc(fpr_inner_20, tpr_inner_20)

plt.figure()
#plt.plot(fpr_outer, tpr_outer, color='darkorange', lw=2, label=f'Outer ROC curve (area = {roc_auc_outer:0.2f})')
plt.plot(fpr_inner, tpr_inner, color='blue', lw=2, label=f'Inner ROC curve (area = {roc_auc_inner:0.2f})')
plt.plot(fpr_inner_20, tpr_inner_20, color='green', lw=2, label=f'Inner ROC curve (area = {roc_auc_inner_20:0.2f})')
plt.plot([0, 1], [0, 1], color='navy', lw=2, linestyle='--')
plt.xlim([0.0, 1.1])
plt.ylim([0.0, 1.1])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('Receiver Operating Characteristic')
plt.legend(loc="lower right")
plt.savefig('./test_plots_tot/roc_curve_inner.png')
'''

pt_truth = np.asarray(pt_truth_value)
pt_1st = np.asarray(pt_1st_fit)
pt_2nd = np.asarray(pt_2nd_fit)

pt_1st = np.nan_to_num(pt_1st)
pt_2nd = np.nan_to_num(pt_2nd)

pt_1st = pt_1st[pt_1st != 0]
pt_2nd = pt_2nd[pt_2nd != 0]

pt_truth_1st = pt_truth[pt_1st != 0]
pt_truth_2nd = pt_truth[pt_2nd != 0]

#count number of zeros in pt_1st and pt_2nd
n_zeros_1st = len(pt_1st) - np.count_nonzero(pt_1st)
n_zeros_2nd = len(pt_2nd) - np.count_nonzero(pt_2nd)

print('Number of failed 1st fit: ', n_zeros_1st)
print('Number of failed 2nd fit: ', n_zeros_2nd)


#plot on the right plot the pt_truth distribution and on the left the residuals
plt.figure()
plt.hist(pt_truth, bins=50, alpha=0.5, label='pt_truth')
plt.hist(pt_1st, bins=50, alpha=0.5, label='pt_1st')
plt.hist(pt_2nd, bins=50, alpha=0.5, label='pt_2nd')
plt.legend()
plt.yscale('log')
plt.xlabel('pT values')
plt.ylabel('Entries')
plt.savefig('./test_plots_tot/pt_values.png')

residuals_1 = (1/pt_truth - 1/pt_1st)
residuals_2 = (1/pt_truth - 1/pt_2nd)

plt.figure()
plt.hist(residuals_1, bins=50, alpha=0.5, label='1st fit')
plt.hist(residuals_2, bins=50, alpha=0.5, label='2nd fit')
plt.xlabel('1/pT Residuals')
plt.ylabel('Counts')
plt.legend()
plt.yscale('log')
plt.savefig('./test_plots_tot/pt_residuals_inverse.png')

#fit the distribution of residuals with a gaussian
from scipy.stats import norm
from scipy.optimize import curve_fit

def gaussian(x, mu, sigma, A):
    return A*np.exp(-1/2*((x-mu)/sigma)**2)

def fit_gaussian(data):
    hist, bins = np.histogram(data, bins=50)
    bin_centers = (bins[:-1] + bins[1:]) / 2
    popt, _ = curve_fit(gaussian, bin_centers, hist, p0=[0, 1, 1])
    return popt

popt_1 = fit_gaussian(residuals_1)
popt_2 = fit_gaussian(residuals_2)

x = np.linspace(-0.1, 0.1, 1000)
y_1 = gaussian(x, *popt_1)
y_2 = gaussian(x, *popt_2)

plt.figure()
plt.hist(residuals_1, bins=50, alpha=0.5, label='1st fit')
plt.hist(residuals_2, bins=50, alpha=0.5, label='2nd fit')
plt.plot(x, y_1, label='1st fit')
plt.plot(x, y_2, label='2nd fit')
plt.xlabel('1/pT Residuals')
#print mu and sigma on the plot
plt.title(f'1st fit: mu={popt_1[0]:.3f}, sigma={popt_1[1]:.3f}')
plt.ylabel('Counts')
plt.legend()
plt.savefig('./test_plots_tot/pt_residuals_inverse_fit.png')
