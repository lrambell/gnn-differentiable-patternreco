import numpy as np
import jax.numpy as jnp
import jraph
import jax.random as jrandom
import jax.tree_util as tree
import jax
import haiku as hk
from typing import Any, Callable, Dict, List, Optional, Tuple
import jaxopt

import optax 
import itertools

import pickle
import os

jax.config.update('jax_enable_x64', True)

def load(path):
    with open(path, 'rb') as fp:
        params = pickle.load(fp)
    return params

def save(params, path):
  params = jax.device_get(params)
  with open(path, 'wb') as fp:
    pickle.dump(params, fp)



print("JAX backend:", jax.devices())

os.environ["XLA_PYTHON_CLIENT_PREALLOCATE"]="false"
os.environ["XLA_PYTHON_CLIENT_MEM_FRACTION"]=".20"
os.environ["XLA_PYTHON_CLIENT_ALLOCATOR"]="gpu"


## loading dataset for classification GAT and MLP ###
arrays_GAT = np.load('arrays_layer_nhit.npy', allow_pickle = True)
X_nhit = arrays_GAT[0]  #96493
Y_nhit = arrays_GAT[1]*1e2
Z_nhit = arrays_GAT[2]
ID_nhit = arrays_GAT[3]



print('Setting parameters..') 
start_training = 0
end_training= 1
batch_size = 1

lr = 1e-4

arrays_training_GAT = np.asarray((X_nhit[start_training:end_training], Y_nhit[start_training:end_training], Z_nhit[start_training:end_training], ID_nhit[start_training:end_training]))

#start_test = end_training + 1
#end_test= start_test + 100

start_test = 0
end_test = 1
arrays_test_GAT = np.asarray((X_nhit[start_test:end_test], Y_nhit[start_test:end_test], Z_nhit[start_test:end_test], ID_nhit[start_test:end_test]))

## functions for graph creation ##

#function that compute the node number (hit number) for each graph (event) in the batch
def get_node_number(arrays, batch_size):
    node_numbers = []
    x = arrays[0]
    
    for i in range(batch_size):
        
        xx = x[i]
        node_numbers.append([xx.shape[0]])
    max_value = np.max(node_numbers)
    #max_value = 384
    
    return node_numbers, max_value

#function that returns the n_nodes and n_edges vectors required for defining the GraphTuple
#n_nodes vector is like ([a],[b],[c]) with a,b,c nodes numbers for each graph in the batch
#n_edges vector is like ([a**2], [b**2], [c**2]) where each element is the number of edges for the graph (fully connected)

def get_nodes_edges_per_event(arrays, batch_size):
    data_array = get_node_number(arrays, batch_size)[0]

    hits_per_event = data_array
    edges_per_event = [[nhits[0]**2] for nhits in data_array]
    return hits_per_event, edges_per_event


#function that creates for each graoh in the batch the vectors defining senders and receivers
#explicitly batched graph is used so all the vectors are padded to the dimension of the bigger one 

def create_senders_receivers(arrays, batch_size):
    #nhits = get_nodes_edges_per_event(arrays, batch_size)[0]
    nhits = get_nodes_edges_per_event(arrays, batch_size)[0]
    senders = []
    receivers = []
    for nhit in nhits:
        n = nhit[0]
        s = jnp.tile(np.arange(n), n).tolist()
        r = jnp.repeat(np.arange(n), n).tolist()
        
        senders.append(s)
        receivers.append(r)

    padded_senders = []
    padded_receivers = []
    #max_value = 384
    max_value = get_node_number(arrays, batch_size)[1]

    for sender in senders:
        pad_s = sender + [-1] * (max_value**2 - len(sender))
        padded_senders.append(pad_s)

    for receiver in receivers:
        pad_r = receiver + [-1] * (max_value**2 - len(receiver))
        padded_receivers.append(pad_r)

    return padded_senders, padded_receivers


#function that decorates the target nodes with only the particle id (muon == -13.)
def decorate_nodes_truth(arrays, batch_size):
    nodes = []
    #max_value = 384
    max_value = get_node_number(arrays, batch_size)[1]    
    
    for i in range (0, batch_size):        
        id = jnp.asarray(arrays[3][i])
        id = jnp.where(jnp.absolute(id) == 13, 1, 0) #for bce

        padded_id =  jnp.pad(id, (0, max_value - len(id)), mode='constant')
        node_features = jnp.stack((padded_id), axis=-1) 
        nodes.append(node_features)

    return nodes

#function that decorates the input nodes with (y,z) coordinates
def decorate_nodes(arrays,batch_size):
    nodes = []
    max_value = get_node_number(arrays, batch_size)[1]   
    #max_value = 384 
    for i in range (0, batch_size):
        y = jnp.asarray(arrays[1][i])
        z = jnp.asarray(arrays[2][i])
                   
        pad_y =  jnp.pad(y, (0, max_value - len(y)), mode='constant')
        pad_z =  jnp.pad(z, (0, max_value - len(z)), mode='constant')
      
        padded_y = jnp.where(pad_y!=0, pad_y, 5)
        padded_z = jnp.where(pad_y!=0, pad_z, 5)

        node_features = jnp.stack((padded_y,padded_z), axis=-1)
        nodes.append(node_features)

    return nodes

def GetGraphs(arrays: jnp.ndarray, batch_size : int) -> jraph.GraphsTuple:
    graph = jraph.GraphsTuple(
        n_node=jnp.array(get_nodes_edges_per_event(arrays, batch_size)[0]), 
        n_edge=jnp.array(get_nodes_edges_per_event(arrays, batch_size)[1]), 
        nodes=jnp.array(decorate_nodes(arrays, batch_size)), 
        edges=None, 
        globals=None,  
        senders=jnp.array(create_senders_receivers(arrays, batch_size)[0]), 
        receivers=jnp.array(create_senders_receivers(arrays, batch_size)[1]))
    
    graph_truth = jraph.GraphsTuple(
        n_node=jnp.array(get_nodes_edges_per_event(arrays, batch_size)[0]), 
        n_edge=jnp.array(get_nodes_edges_per_event(arrays, batch_size)[1]), 
        nodes=jnp.array(decorate_nodes_truth(arrays, batch_size)), 
        edges=None, 
        globals=None,  
        senders=jnp.array(create_senders_receivers(arrays, batch_size)[0]), 
        receivers=jnp.array(create_senders_receivers(arrays, batch_size)[1]))
    
    return (graph, graph_truth)

## network definition ##

def add_self_edges_fn(receivers: jnp.ndarray, senders: jnp.ndarray,
                      total_num_nodes: int) -> Tuple[jnp.ndarray, jnp.ndarray]:
  """Adds self edges. Assumes self edges are not in the graph yet."""
  receivers = jnp.concatenate((receivers, jnp.arange(total_num_nodes)), axis=0)
  senders = jnp.concatenate((senders, jnp.arange(total_num_nodes)), axis=0)
  return receivers, senders


#################
# GAT implementation adapted from https://github.com/deepmind/jraph/blob/master/jraph/_src/models.py#L442.
def GAT(attention_query_fn: Callable,
        attention_logit_fn: Callable,
        node_update_fn: Optional[Callable] = None,
        add_self_edges: bool = True) -> Callable:
  
  # pylint: disable=g-long-lambda
  if node_update_fn is None:
    # By default, apply the leaky relu and then concatenate the heads on the
    # feature axis.
    node_update_fn = lambda x: jnp.reshape(
        jax.nn.leaky_relu(x), (x.shape[0], -1))

  def _ApplyGAT(graph: jraph.GraphsTuple) -> jraph.GraphsTuple:
    """Applies a Graph Attention layer."""
    nodes, edges, receivers, senders, _, _, _ = graph
    # Equivalent to the sum of n_node, but statically known.
    try:
      sum_n_node = nodes.shape[0]
    except IndexError:
      raise IndexError('GAT requires node features')

    # Pass nodes through the attention query function to transform
    # node features, e.g. with an MLP.
    nodes = attention_query_fn(nodes)

    total_num_nodes = tree.tree_leaves(nodes)[0].shape[0]
    if add_self_edges:
      # We add self edges to the senders and receivers so that each node
      # includes itself in aggregation.
      receivers, senders = add_self_edges_fn(receivers, senders,
                                             total_num_nodes)

    # We compute the softmax logits using a function that takes the
    # embedded sender and receiver attributes.
    sent_attributes = nodes[senders]
    received_attributes = nodes[receivers]
    att_softmax_logits = attention_logit_fn(sent_attributes,
                                            received_attributes, edges)

    # Compute the attention softmax weights on the entire tree.
    att_weights = jraph.segment_softmax(
        att_softmax_logits, segment_ids=receivers, num_segments=sum_n_node)

    # Apply attention weights.
    messages = sent_attributes * att_weights
    # Aggregate messages to nodes.
    nodes = jax.ops.segment_sum(messages, receivers, num_segments=sum_n_node)

    # Apply an update function to the aggregated messages.
    nodes = node_update_fn(nodes)

    return graph._replace(nodes=nodes)

  # pylint: enable=g-long-lambda
  return _ApplyGAT

def gat_definition(graph: jraph.GraphsTuple, inner_graph: jraph.GraphsTuple) -> Tuple[jraph.GraphsTuple, jraph.GraphsTuple]:

  def _attention_query_fn1(node_features):
        return hk.nets.MLP([4, 8, 16, 32, 64, 128, 256, 512,512, 1024])(node_features)
  
  def _attention_logit_fn1(senders, receivers, edges):
        del edges
        feat = jnp.concatenate((senders, receivers), axis=-1)
        #return hk.nets.MLP([100,50,25,10,1])(jax.nn.leaky_relu(hk.nets.MLP([1000,800,600,400,200,100])(feat)))
        return hk.nets.MLP([1024])(jax.nn.leaky_relu(hk.nets.MLP([1024, 512, 256, 256, 128, 128])(feat)))


  gn = GAT(
      attention_query_fn=_attention_query_fn1,

      attention_logit_fn=_attention_logit_fn1,
      node_update_fn=hk.nets.MLP([512, 256, 256, 128, 128]),
      add_self_edges=True)
  graph = gn(graph)

  def _attention_query_fn2(node_features):
        return hk.nets.MLP([128, 256, 512, 1024])(node_features)
  
  def _attention_logit_fn2(senders, receivers, edges):
        del edges
        feat = jnp.concatenate((senders, receivers), axis=-1)
        #return hk.nets.MLP([100,50,25,10,1])(jax.nn.leaky_relu(hk.nets.MLP([1000,800,600,400,200,100])(feat)))
        return hk.nets.MLP([128, 64, 32, 16, 8, 4, 2, 1])(jax.nn.leaky_relu(hk.nets.MLP([1024, 512, 256, 256, 128, 128])(feat)))


  gn = GAT(
      attention_query_fn=_attention_query_fn2,

      attention_logit_fn=_attention_logit_fn2,
      node_update_fn=hk.nets.MLP([1024, 512, 256, 128, 64, 32, 32, 16, 8, 4, 2, 1]),
      add_self_edges=True)
  graph = gn(graph)
  
  def _attention_query_fn3(node_features):
        return hk.nets.MLP([3, 6, 12, 24, 48, 128, 256])(node_features)
  
  def _attention_logit_fn3(senders, receivers, edges):
        del edges
        feat = jnp.concatenate((senders, receivers), axis=-1)
        return hk.nets.MLP([64, 32, 16, 8, 4, 2, 1])(jax.nn.leaky_relu(hk.nets.MLP([256, 128, 64])(feat)))

  gn2 = GAT(
      attention_query_fn=_attention_query_fn3,
      attention_logit_fn=_attention_logit_fn3,
      node_update_fn=hk.nets.MLP([256, 128, 64, 32, 32, 16, 8, 4, 2, 1]),
      add_self_edges=True)
  inner_graph = gn2(inner_graph)
  
  return graph, inner_graph


network = hk.without_apply_rng(hk.transform(hk.vmap(gat_definition, split_rng=False)))
gnn_params = load('/home/lrambelli/gnn-differentiable-patternreco/params_gnn6.pkl')

def build_toy_graph(batch, n) -> jraph.GraphsTuple:
    """Define a four node graph, each node has a scalar as its feature."""

    # create new node features vector with 40 elements all with dimension 3 (x,y,z) = (1,1,1)
    node_features = jnp.ones((batch, n, 3))

    # define senders as a list of integer numbers between 0 and n-1 repeated batch times
    senders = jnp.tile(jnp.arange(n), (batch, 1))

    # new receivers vector with 40 elements with ordered numbers between 0 and 39
    receivers = jnp.tile(jnp.arange(n), (batch, 1))

    edges = None

    n_node = jnp.array([n]) * jnp.ones((batch, 1))
    n_edge = jnp.array([n]) * jnp.ones((batch, 1))

    # Optionally you can add `global` information, such as a graph label.
    global_context = None  # Same feature dims as nodes and edges.
    graph = jraph.GraphsTuple(
            nodes=node_features,
            edges=edges,
            senders=senders,
            receivers=receivers,
            n_node=n_node,
            n_edge=n_edge,
            globals=global_context
    )
    return graph

dummy_graph = build_toy_graph(batch_size, 40)


# Initialize empty lists for predictions, truth values, and mask
predictions = []
truth_values = []
pad_mask = []

graph_batch, graph_truth_batch = GetGraphs(arrays_test_GAT, batch_size)
predictions_batch = network.apply(gnn_params, graph_batch, dummy_graph)[0]
predictions.extend(predictions_batch.nodes.ravel())
truth_values.extend(graph_truth_batch.nodes.ravel())
pad_mask.extend(graph_batch.nodes[:,:,0].ravel())



'''
num_batches = 20

# Loop over each batch of graphs
for i in range(num_batches):
    start_batch = start_test + i*batch_size
    end_batch =start_test + (i+1)*batch_size

    # Get the current batch of graphs
    arrays_batch = np.asarray((X_nhit[start_batch:end_batch], Y_nhit[start_batch:end_batch], Z_nhit[start_batch:end_batch], ID_nhit[start_batch:end_batch]))
    
    # Get the graphs and truth values for the current batch
    graph_batch, graph_truth_batch = GetGraphs(arrays_batch, batch_size)
    
    # Apply the network to get predictions for the current batch
    predictions_batch = network.apply(gnn_params, graph_batch, dummy_graph)[0]
    
    # Append the predictions, truth values, and mask for the current batch to the respective lists
    predictions.extend(predictions_batch.nodes.ravel())
    truth_values.extend(graph_truth_batch.nodes.ravel())
    pad_mask.extend(graph_batch.nodes[:,:,0].ravel())

'''

print(len(predictions), len(truth_values), len(pad_mask))

mu = []
bkg = []
loss = []
count = 0
for i in range(len(predictions)):
    
    if truth_values[i] == 1:
        mu.append(predictions[i])
    if truth_values[i] == 0:
        bkg.append(predictions[i])
    else: 
        count += 1

print('padded hits: ', count)

predictions = jnp.asarray(predictions)
truth_values = jnp.asarray(truth_values)
loss = (predictions - truth_values)**2
loss = jnp.where(jnp.asarray(pad_mask) == 5, loss, 0)
print('loss: ', jnp.sum(loss)/ jnp.count_nonzero(loss))

import matplotlib.pyplot as plt
plt.hist(mu, bins=100, range = [0,1.5], alpha=0.5, label='muon')
plt.hist(bkg, bins=100, range = [0,1.5], alpha=0.5, label='bkg')
plt.legend(loc='upper right')
plt.yscale('log')
plt.savefig('test_gnn_final_testset66.png')

from fromGATtoGAT import GraphCreation, ManipulateGraph, Methods4Fit

arrays_MLP = np.load('arrays_layer_nhit_internal.npy', allow_pickle = True)
X_tot = arrays_MLP[0]
Y_tot = arrays_MLP[1]
Z_tot = arrays_MLP[2]
ID_tot = arrays_MLP[3]

arrays_inner = np.asarray((X_tot[start_test:end_test], Y_tot[start_test:end_test], Z_tot[start_test:end_test], ID_tot[start_test:end_test]))



strip_error = 0.0005 / jnp.sqrt(12)


def circle_residuals_jax(params, x, y, w, err):
    """Compute residuals for circle fitting using JAX."""
    A, D, theta = params
    err = strip_error* jnp.ones_like(x)
    P = A * (x**2 + y**2) + jnp.sqrt((1 + 4*A*D)) * (x * jnp.cos(theta) + y * jnp.sin(theta)) + D
       
    residuals = w * (2*P/(1+jnp.sqrt(1+4*A*P)))
    return jnp.sum(w*(residuals/err)**2)


def get_point_at_y(x, y, w, err,y_ref):
    distance = jnp.exp(-jnp.square((y-y_ref)/0.01))
    weight = jnp.multiply(distance,w)
    x_out = jnp.average(x,weights=weight)
    y_out = jnp.average(y,weights=weight)
    return x_out,y_out

def findIntersection(x1,y1,x2,y2,x3,y3,x4,y4): # 1 and 2 = first line / 3 and 4 = second line
    px= ( (x1*y2-y1*x2)*(x3-x4)-(x1-x2)*(x3*y4-y3*x4) ) / ( (x1-x2)*(y3-y4)-(y1-y2)*(x3-x4) )
    py= ( (x1*y2-y1*x2)*(y3-y4)-(y1-y2)*(x3*y4-y3*x4) ) / ( (x1-x2)*(y3-y4)-(y1-y2)*(x3-x4) )
    return px,py

def findCircle(ax,ay,bx,by,cx,cy): # a = first point / b = mid sagitta point / c = end point
    x1 = (ax+bx)/2
    y1 = (ay+by)/2
    x2 = x1 - (by-ay)
    y2 = y1 + (bx-ax)
    x3 = (bx+cx)/2
    y3 = (by+cy)/2
    x4 = x3 - (cy-by)
    y4 = y3 + (cx-bx)
    cx,cy = findIntersection(x1,y1,x2,y2,x3,y3,x4,y4)
    r  = jnp.sqrt((ax-cx)**2+(ay-cy)**2)
    return cx,cy,r

def pre_fit_estimate(x, y, w, err):
    points = jnp.column_stack([x,y,w,err])
    # get two extreme points
    low_x, low_y = get_point_at_y(x, y, w, err,2.5)
    hi_x, hi_y = get_point_at_y(x, y, w, err,4.5)
    # get two mid points
    mid_x1, mid_y1 = get_point_at_y(x, y, w, err,2.8)
    mid_x2, mid_y2 = get_point_at_y(x, y, w, err,4.2)
    # get individual circle estimates
    x1,y1,r1 = findCircle(low_x,low_y,mid_x1,mid_y1,hi_x,hi_y)
    x2,y2,r2 = findCircle(low_x,low_y,mid_x2,mid_y2,hi_x,hi_y)
    # get the average value
    x0 = (x1+x2)/2
    y0 = (y1+y2)/2
    r  = (r1+r2)/2
    return x0,y0,r

def xyr_to_adtheta(xc, yc, r):
   a = 1 / 2*r
   theta = jnp.arctan((yc/xc)**2)
   d = r / 2 * (xc**2 / (4 * jnp.cos(theta)*r**2))

   return jnp.array([a,d,theta])

def fit_circle_least_squares_jax(x, y, w, err, initial_params=None):

    print(x,y,w)
    jax.config.update('jax_enable_x64', True)
    """Fit a circle to points using JAX optimization."""
    real_pre_fit = True
    if initial_params is None:
      if real_pre_fit :
        # Real pre-fit
        x0,y0,r = pre_fit_estimate(x, y, w, err)
        initial_params = xyr_to_adtheta(x0,y0,r)
      else :
        # Use the centroid and average distance as initial parameters
        x0 = jnp.mean(x)
        y0 = jnp.mean(y)
        r = jnp.mean(jnp.sqrt((x - x0)**2 + (y - y0)**2))
        initial_params = jnp.array([1/2*r, 0, 0])

    # Define the cost function without calling it
    cost_function = lambda params: circle_residuals_jax(params, x, y, w, err)
    print('pre', circle_residuals_jax(initial_params, x, y, w, err))

    # Use JAX's minimize function with the L-BFGS-B optimizer
    
  
    lower_bounds = jnp.array([-jnp.inf, -jnp.inf,0])
    upper_bounds = jnp.array([jnp.inf, jnp.inf,jnp.inf])

    #solver = jaxopt.LBFGSB(fun=cost_function,implicit_diff=True)
    #scipyMin = jaxopt.ScipyMinimize(fun=cost_function, method="bfgs")
    #res = scipyMin.run(initial_params,x = x, y = y, w = w, err= err)
    solver = jaxopt.LBFGS(fun=cost_function)
    res = solver.run(initial_params)
    #res = solver.run(initial_params,bounds=(lower_bounds,upper_bounds))

    # Extract optimized parameters
    optimized_params = res.params
    print('post', circle_residuals_jax(optimized_params, x, y, w, err))

 

    return optimized_params, circle_residuals_jax(optimized_params, x, y, w, err)


def final_params(r):
  A3 = r[0]
  D3 = r[1]
  theta3 = r[2]

  B3 = jnp.sqrt(jnp.absolute(1+4*A3*D3))*jnp.cos(theta3)
  C3 = jnp.sqrt(jnp.absolute(1+4*A3*D3))*jnp.sin(theta3)

  fitted_xcenter = -B3/(2*A3)
  fitted_ycenter = -C3/(2*A3)
  fitted_radius = 1/(2*jnp.abs(A3))
  return fitted_xcenter, fitted_ycenter, fitted_radius


def shifted_sigmoid(x):
    return 1 / (1 + jnp.exp(-100 * (x - 0.5)))



def cluster_hits(input_graph, output_graph):
    coordinates = input_graph.nodes
    predictions = output_graph.nodes
    y_input = coordinates[:,0]
    z_input = coordinates[:,1]
    #all_weights = jnp.absolute(predictions).ravel() 
    
    all_weights = shifted_sigmoid(jnp.absolute(predictions)).ravel()
    real_weights = jnp.where(y_input != 5., all_weights, 0)
    
    layers = [2.495 , 2.505, 2.605, 2.705, 2.805, 4.205 , 4.305 ,4.405, 4.505]    
    clustered_y = []
    weights_layer_sum = []
    rms_y_layers = []

    y_input = y_input/1e2
    for i in range(len(layers)):
        weights_layer = jnp.where(z_input == layers[i], real_weights, 0)    
        y = jnp.where(z_input==layers[i], y_input, 0) 
        weights_layer = jnp.where(weights_layer == 1., 1, 0)
        weighted_y = weights_layer*y  

        sum_w = jnp.sum(weights_layer)        
        y_mean = jnp.sum(weighted_y) / sum_w

        clustered_y.append(y_mean)
        weights_layer_sum.append(sum_w)
        layer_rms = jnp.sqrt(jnp.sum((weights_layer*((y - y_mean)))**2) / jnp.sum(weights_layer))   
        layer_rms = jnp.where(jnp.nan_to_num(layer_rms) != 0, layer_rms, 1)
        rms_y_layers.append(layer_rms)
        
    rms_y_layers = jnp.asarray(rms_y_layers)
    return clustered_y, layers, weights_layer_sum, rms_y_layers





for i in range(0,1):
    batch_size = 1

    start_test = i
    end_test= i+1

    fitted_xcenter=0
    fitted_ycenter=0
    fitted_radius=0
    fitted_xcenter_err=0
    fitted_ycenter_err=0
    fitted_radius_err=0
    fitted_xcenter3 = 0
    fitted_ycenter3 = 0
    fitted_radius3 = 0

    arrays_test_GAT = np.asarray((X_nhit[start_test:end_test], Y_nhit[start_test:end_test], Z_nhit[start_test:end_test], ID_nhit[start_test:end_test]))
    arrays_test_MLP = np.asarray((X_tot[start_test:end_test], Y_tot[start_test:end_test], Z_tot[start_test:end_test], ID_tot[start_test:end_test]))
    graph, graph_truth = GetGraphs(arrays_test_GAT, batch_size)
    
    dummy_graph = build_toy_graph(batch_size, 40)
    output_graph = network.apply(gnn_params, graph, dummy_graph)[0]
    
    y_cluster, z_cluster , w_cluster , rms_y = jax.vmap(cluster_hits)(graph, output_graph)
    y_cluster = jnp.array(y_cluster).T
    z_cluster = jnp.array(z_cluster).T
    w_cluster = jnp.array(w_cluster).T

    
    
    
    error_y = rms_y
    w_cluster = jnp.where(w_cluster >= 1, 1, 0)
    y_cluster = jnp.nan_to_num(y_cluster)
    z_cluster = z_cluster

    print('A', y_cluster)
    print('B', z_cluster)
    print('C', w_cluster)

    optimized_params, chisq = jax.vmap(fit_circle_least_squares_jax)(y_cluster, z_cluster, w_cluster, error_y)   
    print('params1', optimized_params) 
    theta = jnp.linspace(0, 2*jnp.pi, 1000)

    xc , yc, R = final_params(optimized_params.T)
    print('params11', xc, yc, R)
    x_fit = xc + R * jnp.cos(theta)
    y_fit = yc + R * jnp.sin(theta)

    plt.figure(figsize=(6, 3), dpi=100)
    plt.scatter(y_cluster, z_cluster, c=shifted_sigmoid(w_cluster).ravel(), label = 'Clustered GAT Hits', alpha = 0.9)
    plt.plot(x_fit, y_fit, color = 'black',label = '1st Fitted Function', linestyle='dashed', alpha = 0.5)
    

    plt.legend()
    plt.title('Event #' + str(i))
    plt.xlabel('Y Coordinate')
    plt.ylabel('Z Coordinate')
    plt.ylim(0.5,5)
    plt.xlim(-0.4, 0.4)
    plt.colorbar()
    plt.grid(True)
    plt.savefig('test_circle_fit6.png')
      

input_graph, graph_truth = GetGraphs(arrays_test_GAT, batch_size)
output_graph = network.apply(gnn_params, input_graph, dummy_graph)[0]

y_cluster_outer , z_cluster_outer, w_cluster_outer, error_cluster_outer = jax.vmap(ManipulateGraph.cluster_hits)(input_graph, output_graph)

y_cluster_outer = jnp.array(y_cluster_outer).T
z_cluster_outer = jnp.array(z_cluster_outer).T
w_cluster_outer = jnp.array(w_cluster_outer).T
w_cluster_outer = jnp.where(w_cluster_outer >= 1, 1, 0)
y_cluster_outer = jnp.nan_to_num(y_cluster_outer)

outer_fit_params, chisq = jax.vmap(fit_circle_least_squares_jax)(y_cluster_outer, z_cluster_outer, w_cluster_outer, error_cluster_outer)
print('params2', outer_fit_params)
xc_outer, yc_outer, r_outer = final_params(outer_fit_params.T)
print('params22', xc_outer, yc_outer, r_outer)

xc_outer = xc_outer[:,jnp.newaxis]
yc_outer = yc_outer[:,jnp.newaxis]
r_outer = r_outer[:,jnp.newaxis]

y_all = input_graph.nodes[:,:,0]
z_all = input_graph.nodes[:,:,1]

idx = 0

y_plot = y_all[idx]
z_plot = z_all[idx]
w_plot = w_cluster_outer[idx]
y_cluster_outer_plot = y_cluster_outer[idx]
z_cluster_outer_plot = z_cluster_outer[idx]


xc_outer_plot = xc_outer[idx]
yc_outer_plot = yc_outer[idx]
r_outer_plot = r_outer[idx]

# deifne circle with x y r 
theta = jnp.linspace(0, 2*jnp.pi, 100)

x_circle = xc_outer_plot + r_outer_plot*jnp.cos(theta)
y_circle = yc_outer_plot + r_outer_plot*jnp.sin(theta)

plt.figure()
plt.scatter(y_plot/1e2, z_plot, label = 'all')
plt.scatter(y_cluster_outer_plot, z_cluster_outer_plot, c= w_plot, cmap='viridis', label = 'clustered')
plt.plot(x_circle, y_circle, label = 'circle fit')
plt.legend()
plt.ylim(2,5)
plt.xlim(-0.4, 0.4)
plt.colorbar()
plt.savefig('test_circle_fit62.png')
'''
inner_coordinates = ManipulateGraph.inner_input_preprocessing(arrays_inner, batch_size)[0]
inner_labels = ManipulateGraph.inner_input_preprocessing(arrays_inner, batch_size)[1]


def from_fit_to_mlp_input(MLP_coordinates, MLP_labels, fitted_xcenter, fitted_ycenter, fitted_radius, y_cluster):
        MLP_layers = [0.405, 0.505, 0.605, 0.705, 0.805, 0.905, 1.005, 1.105]
        outer_layers = [0.405, 4.505]
        theta_mean, theta = jax.vmap(ManipulateGraph.cartesian_to_polar_GAT)(y_cluster, fitted_xcenter, fitted_ycenter)
        extrapolated_theta_MLP_layers = jnp.arcsin((jnp.asarray(MLP_layers) - fitted_ycenter[:,jnp.newaxis]) / fitted_radius[:,jnp.newaxis])
        mean_extrap_theta_MLP_layers = jnp.mean(extrapolated_theta_MLP_layers, axis=2)
        theta_outer_layers = jnp.arcsin((jnp.asarray(outer_layers) - fitted_ycenter[:,jnp.newaxis]) / fitted_radius[:,jnp.newaxis])
        # compute the difference between the two outer layers
        delta_theta_outer_layers = jnp.absolute(theta_outer_layers[:,:,0] - theta_outer_layers[:,:,1])
        theta_mean = theta_mean[:,jnp.newaxis]
        good_happy_theta_inner_layers = jnp.where((jnp.absolute(mean_extrap_theta_MLP_layers[...,jnp.newaxis] - theta_mean[...,jnp.newaxis]) < 2 * delta_theta_outer_layers[...,jnp.newaxis]), extrapolated_theta_MLP_layers, jnp.pi - extrapolated_theta_MLP_layers)
        extrapolated_y_MLP_fromtheta = fitted_xcenter[..., jnp.newaxis] + fitted_radius[..., jnp.newaxis] * jnp.sin(good_happy_theta_inner_layers)
        # fitted_xcenter[:,jnp.newaxis] + fitted_radius[:,jnp.newaxis] * jnp.cos(good_happy_theta_inner_layers)  
        
        print(extrapolated_y_MLP_fromtheta)
        MLP_layers_vec = jnp.asarray(MLP_layers) #jnp.tile(jnp.asarray(MLP_layers), (batch_size, 1))
        y_mlp_input = MLP_coordinates[:,0]
        z_mlp_input = MLP_coordinates[:,1]
        mlp_labels = MLP_labels
    
        print(MLP_layers_vec.shape, y_mlp_input.shape, z_mlp_input.shape, mlp_labels.shape, extrapolated_y_MLP_fromtheta.shape)
        ymlp, zmlp, lmlp , ydiffmlp = ManipulateGraph.find_nearest_y_points(MLP_layers_vec, extrapolated_y_MLP_fromtheta.ravel(), y_mlp_input, z_mlp_input, mlp_labels)
        return ymlp, zmlp, lmlp, extrapolated_y_MLP_fromtheta, ydiffmlp

y_inner, z_inner, label_inner, extrap_inner, deltay_inner = from_fit_to_mlp_input(inner_coordinates, inner_labels, xc_outer, yc_outer, r_outer, y_cluster_outer)
y_inner = jnp.asarray(y_inner)
z_inner = jnp.asarray(z_inner)
label_inner = jnp.asarray(label_inner)
extrap_inner = jnp.asarray(extrap_inner)
deltay_inner = jnp.asarray(deltay_inner)

extrap_plot = extrap_inner[idx].ravel()
layers = jnp.asarray([0.405, 0.505, 0.605, 0.705, 0.805, 0.905, 1.005, 1.105])
plt.figure()
plt.scatter(y_plot/1e2, z_plot, label = 'all')
plt.scatter(y_cluster_outer_plot, z_cluster_outer_plot, c= w_plot, cmap='viridis', label = 'clustered')
plt.plot(x_circle, y_circle, label = 'circle fit')
plt.scatter(extrap_plot, layers, marker='x', label = 'extrapolated')
plt.legend()
plt.ylim(0,5)

plt.colorbar()
plt.savefig('test_extrapolation6.png')




print(y_inner.shape, z_inner.shape, label_inner.shape, extrap_inner.shape, deltay_inner.shape)
y_inner = jnp.asarray(y_inner).transpose(1,0,2)
z_inner = jnp.asarray(z_inner).transpose(1,0,2)
label_inner = jnp.asarray(label_inner).transpose(1,0,2)
extrap_inner = extrap_inner[:,:,jnp.newaxis]
deltay_inner = jnp.asarray(deltay_inner).transpose(1,0,2)

# function that takes as input xc yc R and y coordinate and return the extrapolated x on the cirle
#y_inner, z_inner, label_inner, extrap_inner, deltay_inner = jax.vmap(ManipulateGraph.from_fit_to_mlp_input)(inner_coordinates, inner_labels, xc_outer, yc_outer, r_outer, y_cluster_outer)


z_layers_inner = np.asarray([0.405, 0.505, 0.605, 0.705, 0.805, 0.905, 1.005, 1.105])

print(y_inner.shape, z_inner.shape, label_inner.shape, extrap_inner.shape, deltay_inner.shape)
print(inner_coordinates.shape)

print(z_inner)
print(y_inner)
y_inner_plot = y_inner[idx].ravel()
z_inner_plot = z_inner[idx].ravel()
label_inner_plot = label_inner[idx].ravel()
extrap_inner_plot = extrap_inner[idx].ravel()

y_inner_tot_plot = inner_coordinates[idx][:,0]
z_inner_tot_plot = inner_coordinates[idx][:,1]

#print(y_inner_plot)
#print(z_inner_plot)
#print(label_inner_plot)



plt.figure()
plt.scatter(y_plot/1e2, z_plot, label = 'all')
plt.scatter(y_cluster_outer_plot, z_cluster_outer_plot, c= w_plot, cmap='viridis', label = 'clustered')
plt.plot(x_circle, y_circle, label = 'circle fit')
plt.scatter(y_inner_tot_plot, z_inner_tot_plot, label = 'inner')
plt.scatter(y_inner_plot, z_inner_plot, c=label_inner_plot, cmap='viridis', label = 'inner')
plt.scatter(extrap_inner_plot, z_layers_inner, marker='x', label = 'extrapolated')
plt.legend()
plt.ylim(0,5)
plt.xlim(-0.4, 0.4)
plt.colorbar()
plt.savefig('test_extrapolation.png')
'''