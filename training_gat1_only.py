
import numpy as np

import jax.numpy as jnp
import jraph
import jax.random as jrandom
import jax.tree_util as tree
import jax

import haiku as hk
from typing import Any, Callable, Dict, List, Optional, Tuple
import optax
import os
import itertools
import matplotlib.pyplot as plt 
from jraph._src import models

from jax.experimental.host_callback import call



from jax.lib import xla_bridge
print('Using platform: ', xla_bridge.get_backend().platform)

os.environ["XLA_PYTHON_CLIENT_PREALLOCATE"]="false"
os.environ["XLA_PYTHON_CLIENT_MEM_FRACTION"]=".10"
os.environ["XLA_PYTHON_CLIENT_ALLOCATOR"]="platform"

arrayss = np.load('/home/lrambelli/patternreco/arrays_layer_nhit.npy', allow_pickle = True)
X_nhit = arrayss[0]  #96493
Y_nhit = arrayss[1]*1e2
Z_nhit = arrayss[2]
ID_nhit = arrayss[3]

print('Setting parameters..') 
start_training = 0
end_training= 10000
batch_size = 40
lr = 1e-4
steps =30

print('Events for training: ', end_training, ', batch size: ', batch_size, ', learning rate: ', lr)

arrays_training = np.asarray((X_nhit[start_training:end_training], Y_nhit[start_training:end_training], Z_nhit[start_training:end_training], ID_nhit[start_training:end_training]))

start_test = end_training + 1
end_test= start_test + 100

arrays_test = np.asarray((X_nhit[start_test:end_test], Y_nhit[start_test:end_test], Z_nhit[start_test:end_test], ID_nhit[start_test:end_test]))

## functions for graph creation ##

#function that compute the node number (hit number) for each graph (event) in the batch
def get_node_number(arrays, batch_size):
    node_numbers = []
    x = arrays[0]
    
    for i in range(batch_size):
        
        xx = x[i]
        node_numbers.append([xx.shape[0]])
    max_value = np.max(node_numbers)
    #max_value = 384
    
    return node_numbers, max_value

#function that returns the n_nodes and n_edges vectors required for defining the GraphTuple
#n_nodes vector is like ([a],[b],[c]) with a,b,c nodes numbers for each graph in the batch
#n_edges vector is like ([a**2], [b**2], [c**2]) where each element is the number of edges for the graph (fully connected)

def get_nodes_edges_per_event(arrays, batch_size):
    data_array = get_node_number(arrays, batch_size)[0]

    hits_per_event = data_array
    edges_per_event = [[nhits[0]**2] for nhits in data_array]
    return hits_per_event, edges_per_event


#function that creates for each graoh in the batch the vectors defining senders and receivers
#explicitly batched graph is used so all the vectors are padded to the dimension of the bigger one 

def create_senders_receivers(arrays, batch_size):
    #nhits = get_nodes_edges_per_event(arrays, batch_size)[0]
    nhits = get_nodes_edges_per_event(arrays, batch_size)[0]
    senders = []
    receivers = []
    for nhit in nhits:
        n = nhit[0]
        s = jnp.tile(np.arange(n), n).tolist()
        r = jnp.repeat(np.arange(n), n).tolist()
        
        senders.append(s)
        receivers.append(r)

    padded_senders = []
    padded_receivers = []
    #max_value = 384
    max_value = get_node_number(arrays, batch_size)[1]

    for sender in senders:
        pad_s = sender + [-1] * (max_value**2 - len(sender))
        padded_senders.append(pad_s)

    for receiver in receivers:
        pad_r = receiver + [-1] * (max_value**2 - len(receiver))
        padded_receivers.append(pad_r)

    return padded_senders, padded_receivers


#function that decorates the target nodes with only the particle id (muon == -13.)
def decorate_nodes_truth(arrays, batch_size):
    nodes = []
    #max_value = 384
    max_value = get_node_number(arrays, batch_size)[1]    
    
    for i in range (0, batch_size):        
        id = np.asarray(arrays[3][i])
        id = jnp.where(jnp.absolute(id) == 13, 1, 0) #for bce

        padded_id =  jnp.pad(id, (0, max_value - len(id)), mode='constant')
        node_features = jnp.stack((padded_id), axis=-1) 
        nodes.append(node_features)

    return nodes

#function that decorates the input nodes with (y,z) coordinates
def decorate_nodes(arrays,batch_size):
    nodes = []
    max_value = get_node_number(arrays, batch_size)[1]   
    #max_value = 384 
    for i in range (0, batch_size):
        y = np.asarray(arrays[1][i])
        z = np.asarray(arrays[2][i])
                   
        pad_y =  jnp.pad(y, (0, max_value - len(y)), mode='constant')
        pad_z =  jnp.pad(z, (0, max_value - len(z)), mode='constant')
      
        padded_y = jnp.where(pad_y!=0, pad_y, 5)
        padded_z = jnp.where(pad_y!=0, pad_z, 5)

        node_features = jnp.stack((padded_y,padded_z), axis=-1)
        nodes.append(node_features)

    return nodes

def GetGraphs(arrays: np.ndarray, batch_size : int) -> jraph.GraphsTuple:
    graph = jraph.GraphsTuple(
        n_node=jnp.array(get_nodes_edges_per_event(arrays, batch_size)[0]), 
        n_edge=jnp.array(get_nodes_edges_per_event(arrays, batch_size)[1]), 
        nodes=jnp.array(decorate_nodes(arrays, batch_size)), 
        edges=None, 
        globals=None,  
        senders=jnp.array(create_senders_receivers(arrays, batch_size)[0]), 
        receivers=jnp.array(create_senders_receivers(arrays, batch_size)[1]))
    
    graph_truth = jraph.GraphsTuple(
        n_node=jnp.array(get_nodes_edges_per_event(arrays, batch_size)[0]), 
        n_edge=jnp.array(get_nodes_edges_per_event(arrays, batch_size)[1]), 
        nodes=jnp.array(decorate_nodes_truth(arrays, batch_size)), 
        edges=None, 
        globals=None,  
        senders=jnp.array(create_senders_receivers(arrays, batch_size)[0]), 
        receivers=jnp.array(create_senders_receivers(arrays, batch_size)[1]))
    
    return (graph, graph_truth)

## network definition ##

def add_self_edges_fn(receivers: jnp.ndarray, senders: jnp.ndarray,
                      total_num_nodes: int) -> Tuple[jnp.ndarray, jnp.ndarray]:
  """Adds self edges. Assumes self edges are not in the graph yet."""
  receivers = jnp.concatenate((receivers, jnp.arange(total_num_nodes)), axis=0)
  senders = jnp.concatenate((senders, jnp.arange(total_num_nodes)), axis=0)
  return receivers, senders

#################
# GAT implementation adapted from https://github.com/deepmind/jraph/blob/master/jraph/_src/models.py#L442.
def GAT(attention_query_fn: Callable,
        attention_logit_fn: Callable,
        node_update_fn: Optional[Callable] = None,
        add_self_edges: bool = True) -> Callable:
  
  # pylint: disable=g-long-lambda
  if node_update_fn is None:
    # By default, apply the leaky relu and then concatenate the heads on the
    # feature axis.
    node_update_fn = lambda x: jnp.reshape(
        jax.nn.leaky_relu(x), (x.shape[0], -1))

  def _ApplyGAT(graph: jraph.GraphsTuple) -> jraph.GraphsTuple:
    """Applies a Graph Attention layer."""
    nodes, edges, receivers, senders, _, _, _ = graph
    # Equivalent to the sum of n_node, but statically known.
    try:
      sum_n_node = nodes.shape[0]
    except IndexError:
      raise IndexError('GAT requires node features')

    # Pass nodes through the attention query function to transform
    # node features, e.g. with an MLP.
    nodes = attention_query_fn(nodes)

    total_num_nodes = tree.tree_leaves(nodes)[0].shape[0]
    if add_self_edges:
      # We add self edges to the senders and receivers so that each node
      # includes itself in aggregation.
      receivers, senders = add_self_edges_fn(receivers, senders,
                                             total_num_nodes)

    # We compute the softmax logits using a function that takes the
    # embedded sender and receiver attributes.
    sent_attributes = nodes[senders]
    received_attributes = nodes[receivers]
    att_softmax_logits = attention_logit_fn(sent_attributes,
                                            received_attributes, edges)

    # Compute the attention softmax weights on the entire tree.
    att_weights = jraph.segment_softmax(
        att_softmax_logits, segment_ids=receivers, num_segments=sum_n_node)

    # Apply attention weights.
    messages = sent_attributes * att_weights
    # Aggregate messages to nodes.
    message_nodes = jax.ops.segment_sum(messages, receivers, num_segments=sum_n_node)

    # Concatenate the aggregated messages with the node features.
    nodes = jnp.concatenate([nodes, message_nodes], axis=-1)
    # Apply an update function to the aggregated messages.
    nodes = node_update_fn(nodes)


    return graph._replace(nodes=nodes)

  # pylint: enable=g-long-lambda
  return _ApplyGAT

def gat_definition(graph: jraph.GraphsTuple, inner_graph: jraph.GraphsTuple) -> tuple[jraph.GraphsTuple, jraph.GraphsTuple]:

  def _attention_query_fn1(node_features):
        return hk.nets.MLP([4, 8, 16, 32, 64, 128, 256, 512,512, 1024])(node_features)
  
  def _attention_logit_fn1(senders, receivers, edges):
        del edges
        feat = jnp.concatenate((senders, receivers), axis=-1)
        #return hk.nets.MLP([100,50,25,10,1])(jax.nn.leaky_relu(hk.nets.MLP([1000,800,600,400,200,100])(feat)))
        return hk.nets.MLP([1024])(jax.nn.leaky_relu(hk.nets.MLP([1024, 512, 256, 256, 128, 128])(feat)))


  gn = GAT(
      attention_query_fn=_attention_query_fn1,

      attention_logit_fn=_attention_logit_fn1,
      node_update_fn=hk.nets.MLP([512, 256, 256, 128, 128]),
      add_self_edges=True)
  graph = gn(graph)

  def _attention_query_fn2(node_features):
        return hk.nets.MLP([128, 256, 512, 1024])(node_features)
  
  def _attention_logit_fn2(senders, receivers, edges):
        del edges
        feat = jnp.concatenate((senders, receivers), axis=-1)
        #return hk.nets.MLP([100,50,25,10,1])(jax.nn.leaky_relu(hk.nets.MLP([1000,800,600,400,200,100])(feat)))
        return hk.nets.MLP([128, 64, 32, 16, 8, 4, 2, 1])(jax.nn.leaky_relu(hk.nets.MLP([1024, 512, 256, 256, 128, 128])(feat)))


  gn = GAT(
      attention_query_fn=_attention_query_fn2,

      attention_logit_fn=_attention_logit_fn2,
      node_update_fn=hk.nets.MLP([1024, 512, 256, 128, 64, 32, 32, 16, 8, 4, 2, 1]),
      add_self_edges=True)
  graph = gn(graph)
 
  def _attention_query_fn3(node_features):
        return hk.nets.MLP([3, 6, 12, 24, 48, 128, 256])(node_features)
  
  def _attention_logit_fn3(senders, receivers, edges):
        del edges
        feat = jnp.concatenate((senders, receivers), axis=-1)
        return hk.nets.MLP([64, 32, 16, 8, 4, 2, 1])(jax.nn.leaky_relu(hk.nets.MLP([256, 128, 64])(feat)))

  gn = GAT(
      attention_query_fn=_attention_query_fn3,
      attention_logit_fn=_attention_logit_fn3,
      node_update_fn=hk.nets.MLP([256, 128, 64, 32, 32, 16, 8, 4, 2, 1]),
      add_self_edges=True)
  inner_graph = gn(inner_graph)

  return graph, inner_graph


print('Graph definition')
graph = GetGraphs(arrays_training, batch_size)[0]
graph_truth = GetGraphs(arrays_training,batch_size)[1]

graph_test = GetGraphs(arrays_test, batch_size)[0]
graph_truth_test = GetGraphs(arrays_test, batch_size)[1]

def build_toy_graph(batch, n) -> jraph.GraphsTuple:
    """Define a four node graph, each node has a scalar as its feature."""

    # create new node features vector with 40 elements all with dimension 3 (x,y,z) = (1,1,1)
    node_features = jnp.ones((batch, n, 3))

    # define senders as a list of integer numbers between 0 and n-1 repeated batch times
    senders = jnp.tile(jnp.arange(n), (batch, 1))

    # new receivers vector with 40 elements with ordered numbers between 0 and 39
    receivers = jnp.tile(jnp.arange(n), (batch, 1))

    edges = None

    n_node = jnp.array([n]) * jnp.ones((batch, 1))
    n_edge = jnp.array([n]) * jnp.ones((batch, 1))

    # Optionally you can add `global` information, such as a graph label.
    global_context = None  # Same feature dims as nodes and edges.
    graph = jraph.GraphsTuple(
            nodes=node_features,
            edges=edges,
            senders=senders,
            receivers=receivers,
            n_node=n_node,
            n_edge=n_edge,
            globals=global_context
    )
    return graph

dummy_graph = build_toy_graph(batch_size, 40)

def DataLoader(arrays, batch_size, *, key):
    dataset_size = arrays[0].shape[0]
    #print('n_events: ', dataset_size, ', batch_size: ', batch_size)
        
    (key,) = jrandom.split(key, 1)
    start = 0
    end = batch_size
    while end <= dataset_size:
        
        yield tuple(GetGraphs(arrays[:,start:end], batch_size))
        start = end
        end = start + batch_size

## network inizialization ##
print('Network inizialization') 

network = hk.without_apply_rng(hk.transform(hk.vmap(gat_definition, split_rng=False)))
params = network.init(jax.random.PRNGKey(1234), graph, dummy_graph)
opt_init, opt_update = optax.adam(lr)
opt_state = opt_init(params)

@jax.jit
def prediction_loss(params, input_graph, target_graph, inner_graph):

        output_graph = network.apply(params, input_graph, inner_graph)[0]        
        input_mask = input_graph.nodes[:,:,0]    
        radius = jnp.reshape(output_graph.nodes, (output_graph.nodes.shape[0], output_graph.nodes.shape[1]))
        
        id_mask = target_graph.nodes

        loss = (radius - id_mask)**2
        loss2 = jnp.where(input_mask != 5, loss, 0)   
        
        return jnp.sum(loss2) / jnp.count_nonzero(loss2)

@jax.jit
def update(params, opt_state, gr):
        """Returns updated params and state."""
        updates, opt_state = opt_update(gr, opt_state)
        return optax.apply_updates(params, updates), opt_state

dataloader = DataLoader(arrays_training, batch_size,  key = jrandom.PRNGKey(683))
it = itertools.tee(dataloader, steps)
gpus = jax.devices('gpu')

import pickle
def save(params, path):
  with open(path, 'wb') as fp:
    pickle.dump(params, fp)


import time
start = time.time()
print('Training.. ')
loss_list = []
val_loss_list = []

for step in range(steps):
       
        epoch_loss = 0
        epoch_val_loss = 0
        iter_data = it[step]
        for g in iter_data:
                input_graph = g[0]
                truth_graph = g[1]
                inner_graph = dummy_graph
               
                loss = prediction_loss(params, input_graph, truth_graph, inner_graph)
        
                gr = jax.grad(prediction_loss, argnums=0)(params, input_graph, truth_graph, inner_graph)
                epoch_loss = loss

                val_loss = prediction_loss(params, graph_test, graph_truth_test, dummy_graph)
                epoch_val_loss = val_loss
                params, opt_state = update(params, opt_state, gr)

        print('--------------------->  STEP: ', step, ' LOSS: ', epoch_loss, ' VAL_LOSS: ', val_loss)
        loss_list.append(epoch_loss)
        val_loss_list.append(val_loss)
        save(params, 'params_gat1_10k_epoch_' + str(step) + '_loss_' + str(epoch_loss) + '_val_loss_' + str(epoch_val_loss) + '.pkl')

end = time.time()
training_time = end - start
print('Training time:  ', training_time)


plt.figure(figsize=(7, 5), dpi=500)
xrange = np.arange(0,steps,1)
plt.plot(xrange, np.asarray(loss_list), label = 'loss')
plt.plot(xrange, np.asarray(val_loss_list), label = 'val_loss')
plt.xlabel('epochs')
plt.ylabel('value')
plt.savefig('loss_gnn.png')

## post processing

print('Post processing ..')

graph_test, graph_truth_test = GetGraphs(arrays_test, batch_size)
predicted_graph = network.apply(params, graph_test, dummy_graph)[0]
predicted_radius = predicted_graph.nodes

predicted_radius = np.reshape(predicted_radius, (predicted_radius.shape[0], predicted_radius.shape[1]))
radius = predicted_radius.ravel().tolist()
truth = graph_truth_test.nodes.flatten().tolist()

nhit = graph_truth_test.n_node
pad_index = nhit[0][0]
pad_value = radius[pad_index + 1]


mu = []
bkg = []
for i in range(len(truth)):
    
    t = truth[i]
    r = radius[i]
    
    if t ==1:
        mu.append(r)
    if t==0:
        bkg.append(r)

plot_bkg = [val for val in bkg if val != pad_value]

plt.figure(figsize=(7, 5), dpi=500)

plt.hist(plot_bkg, bins=100, range = [0,1.5], label = 'bkg', alpha = 0.5)
plt.hist(mu, bins = 100, range = [0,1.5], label = 'signal', alpha = 0.5)
plt.xlabel('Predicted Value')
plt.ylabel('Entries')
plt.legend()
plt.savefig('pred_gnn.png')


plt.figure(figsize=(7, 5), dpi=500)

plt.hist(plot_bkg, bins=100, range = [0,1.5], label = 'bkg', alpha = 0.5)
plt.hist(mu, bins = 100, range = [0,1.5], label = 'signal', alpha = 0.5)
plt.xlabel('Predicted Value')
plt.ylabel('Entries')
plt.yscale('log')
plt.legend()
plt.savefig('pred_gnn_log.png')



