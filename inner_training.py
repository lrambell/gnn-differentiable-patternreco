
import numpy as np

import jax.numpy as jnp
import jraph
import jax.random as jrandom
import jax.tree_util as tree
import jax
import flax 
import haiku as hk
from typing import Any, Callable, Dict, List, Optional, Tuple
import optax
import os
import itertools
import matplotlib.pyplot as plt 
from jraph._src import models
import jaxopt

from jax.experimental.host_callback import call



from jax.lib import xla_bridge
print('Using platform: ', xla_bridge.get_backend().platform)
jax.config.update("jax_enable_x64", True)

os.environ["XLA_PYTHON_CLIENT_PREALLOCATE"]="false"
os.environ["XLA_PYTHON_CLIENT_MEM_FRACTION"]=".10"
os.environ["XLA_PYTHON_CLIENT_ALLOCATOR"]="platform"

arrayss = np.load('/home/lrambelli/patternreco/arrays_layer_nhit.npy', allow_pickle = True)
X_nhit = arrayss[0]  #96493
Y_nhit = arrayss[1]*1e2
Z_nhit = arrayss[2]
ID_nhit = arrayss[3]

arrays_MLP = np.load('arrays_layer_nhit_internal.npy', allow_pickle = True)
X_tot = arrays_MLP[0]
Y_tot = arrays_MLP[1]
Z_tot = arrays_MLP[2]
ID_tot = arrays_MLP[3]

print('Setting parameters..') 
start_training = 0
end_training= 10000
batch_size = 40
lr = 1e-3
steps = 100


import pickle

def save(params, path):
  with open(path, 'wb') as fp:
    pickle.dump(params, fp)

def load(path):
    with open(path, 'rb') as fp:
        params = pickle.load(fp)
    return params


print('Events for training: ', end_training, ', batch size: ', batch_size, ', learning rate: ', lr)

arrays_training = np.asarray((X_nhit[start_training:end_training], Y_nhit[start_training:end_training], Z_nhit[start_training:end_training], ID_nhit[start_training:end_training]))
arrays_inner_training = np.asarray((X_tot[start_training:end_training], Y_tot[start_training:end_training], Z_tot[start_training:end_training], ID_tot[start_training:end_training]))

start_test = end_training + 1
end_test= start_test + 100

arrays_test = np.asarray((X_nhit[start_test:end_test], Y_nhit[start_test:end_test], Z_nhit[start_test:end_test], ID_nhit[start_test:end_test]))
arrays_inner_test = np.asarray((X_tot[start_test:end_test], Y_tot[start_test:end_test], Z_tot[start_test:end_test], ID_tot[start_test:end_test]))
## functions for graph creation ##

#function that compute the node number (hit number) for each graph (event) in the batch
def get_node_number(arrays, batch_size):
    node_numbers = []
    x = arrays[0]
    
    for i in range(batch_size):
        
        xx = x[i]
        node_numbers.append([xx.shape[0]])
    max_value = np.max(node_numbers)
    #max_value = 384
    
    return node_numbers, max_value

#function that returns the n_nodes and n_edges vectors required for defining the GraphTuple
#n_nodes vector is like ([a],[b],[c]) with a,b,c nodes numbers for each graph in the batch
#n_edges vector is like ([a**2], [b**2], [c**2]) where each element is the number of edges for the graph (fully connected)

def get_nodes_edges_per_event(arrays, batch_size):
    data_array = get_node_number(arrays, batch_size)[0]

    hits_per_event = data_array
    edges_per_event = [[nhits[0]**2] for nhits in data_array]
    return hits_per_event, edges_per_event


#function that creates for each graoh in the batch the vectors defining senders and receivers
#explicitly batched graph is used so all the vectors are padded to the dimension of the bigger one 

def create_senders_receivers(arrays, batch_size):
    #nhits = get_nodes_edges_per_event(arrays, batch_size)[0]
    nhits = get_nodes_edges_per_event(arrays, batch_size)[0]
    senders = []
    receivers = []
    for nhit in nhits:
        n = nhit[0]
        s = jnp.tile(np.arange(n), n).tolist()
        r = jnp.repeat(np.arange(n), n).tolist()
        
        senders.append(s)
        receivers.append(r)

    padded_senders = []
    padded_receivers = []
    #max_value = 384
    max_value = get_node_number(arrays, batch_size)[1]

    for sender in senders:
        pad_s = sender + [-1] * (max_value**2 - len(sender))
        padded_senders.append(pad_s)

    for receiver in receivers:
        pad_r = receiver + [-1] * (max_value**2 - len(receiver))
        padded_receivers.append(pad_r)

    return padded_senders, padded_receivers


#function that decorates the target nodes with only the particle id (muon == -13.)
def decorate_nodes_truth(arrays, batch_size):
    nodes = []
    #max_value = 384
    max_value = get_node_number(arrays, batch_size)[1]    
    
    for i in range (0, batch_size):        
        id = jnp.asarray(arrays[3][i])
        id = jnp.where(jnp.absolute(id) == 13, 1, 0) #for bce

        padded_id =  jnp.pad(id, (0, max_value - len(id)), mode='constant')
        node_features = jnp.stack((padded_id), axis=-1) 
        nodes.append(node_features)

    return nodes

#function that decorates the input nodes with (y,z) coordinates
def decorate_nodes(arrays,batch_size):
    nodes = []
    max_value = get_node_number(arrays, batch_size)[1]   
    #max_value = 384 
    for i in range (0, batch_size):
        y = jnp.asarray(arrays[1][i])
        z = jnp.asarray(arrays[2][i])
                   
        pad_y =  jnp.pad(y, (0, max_value - len(y)), mode='constant')
        pad_z =  jnp.pad(z, (0, max_value - len(z)), mode='constant')
      
        padded_y = jnp.where(pad_y!=0, pad_y, 5)
        padded_z = jnp.where(pad_y!=0, pad_z, 5)

        node_features = jnp.stack((padded_y,padded_z), axis=-1)
        nodes.append(node_features)

    return nodes

def GetGraphs(arrays: jnp.ndarray, batch_size : int) -> jraph.GraphsTuple:
    graph = jraph.GraphsTuple(
        n_node=jnp.array(get_nodes_edges_per_event(arrays, batch_size)[0]), 
        n_edge=jnp.array(get_nodes_edges_per_event(arrays, batch_size)[1]), 
        nodes=jnp.array(decorate_nodes(arrays, batch_size)), 
        edges=None, 
        globals=None,  
        senders=jnp.array(create_senders_receivers(arrays, batch_size)[0]), 
        receivers=jnp.array(create_senders_receivers(arrays, batch_size)[1]))
    
    graph_truth = jraph.GraphsTuple(
        n_node=jnp.array(get_nodes_edges_per_event(arrays, batch_size)[0]), 
        n_edge=jnp.array(get_nodes_edges_per_event(arrays, batch_size)[1]), 
        nodes=jnp.array(decorate_nodes_truth(arrays, batch_size)), 
        edges=None, 
        globals=None,  
        senders=jnp.array(create_senders_receivers(arrays, batch_size)[0]), 
        receivers=jnp.array(create_senders_receivers(arrays, batch_size)[1]))
    
    return (graph, graph_truth)

## network definition ##

def add_self_edges_fn(receivers: jnp.ndarray, senders: jnp.ndarray,
                      total_num_nodes: int) -> Tuple[jnp.ndarray, jnp.ndarray]:
  """Adds self edges. Assumes self edges are not in the graph yet."""
  receivers = jnp.concatenate((receivers, jnp.arange(total_num_nodes)), axis=0)
  senders = jnp.concatenate((senders, jnp.arange(total_num_nodes)), axis=0)
  return receivers, senders

#################
# GAT implementation adapted from https://github.com/deepmind/jraph/blob/master/jraph/_src/models.py#L442.
def GAT(attention_query_fn: Callable,
        attention_logit_fn: Callable,
        node_update_fn: Optional[Callable] = None,
        add_self_edges: bool = True) -> Callable:
  
  # pylint: disable=g-long-lambda
  if node_update_fn is None:
    # By default, apply the leaky relu and then concatenate the heads on the
    # feature axis.
    node_update_fn = lambda x: jnp.reshape(
        jax.nn.leaky_relu(x), (x.shape[0], -1))

  def _ApplyGAT(graph: jraph.GraphsTuple) -> jraph.GraphsTuple:
    """Applies a Graph Attention layer."""
    nodes, edges, receivers, senders, _, _, _ = graph
    # Equivalent to the sum of n_node, but statically known.
    try:
      sum_n_node = nodes.shape[0]
    except IndexError:
      raise IndexError('GAT requires node features')

    # Pass nodes through the attention query function to transform
    # node features, e.g. with an MLP.
    nodes = attention_query_fn(nodes)

    total_num_nodes = tree.tree_leaves(nodes)[0].shape[0]
    if add_self_edges:
      # We add self edges to the senders and receivers so that each node
      # includes itself in aggregation.
      receivers, senders = add_self_edges_fn(receivers, senders,
                                             total_num_nodes)

    # We compute the softmax logits using a function that takes the
    # embedded sender and receiver attributes.
    sent_attributes = nodes[senders]
    received_attributes = nodes[receivers]
    att_softmax_logits = attention_logit_fn(sent_attributes,
                                            received_attributes, edges)

    # Compute the attention softmax weights on the entire tree.
    att_weights = jraph.segment_softmax(
        att_softmax_logits, segment_ids=receivers, num_segments=sum_n_node)

    # Apply attention weights.
    messages = sent_attributes * att_weights
    # Aggregate messages to nodes.
    nodes = jax.ops.segment_sum(messages, receivers, num_segments=sum_n_node)

    # Apply an update function to the aggregated messages.
    nodes = node_update_fn(nodes)

    return graph._replace(nodes=nodes)

  # pylint: enable=g-long-lambda
  return _ApplyGAT

def gat_definition(graph: jraph.GraphsTuple, inner_graph: jraph.GraphsTuple) -> Tuple[jraph.GraphsTuple, jraph.GraphsTuple]:

  def _attention_query_fn1(node_features):
        return hk.nets.MLP([4, 8, 16, 32, 64, 128, 256, 512,512, 1024])(node_features)
  
  def _attention_logit_fn1(senders, receivers, edges):
        del edges
        feat = jnp.concatenate((senders, receivers), axis=-1)
        return hk.nets.MLP([1024])(jax.nn.leaky_relu(hk.nets.MLP([1024, 512, 256, 256, 128, 128])(feat)))

  gn = GAT(
      attention_query_fn=_attention_query_fn1,

      attention_logit_fn=_attention_logit_fn1,
      node_update_fn=hk.nets.MLP([512, 256, 256, 128, 128]),
      add_self_edges=True)
  graph = gn(graph)

  def _attention_query_fn2(node_features):
        return hk.nets.MLP([128, 256, 512, 1024])(node_features)
  
  def _attention_logit_fn2(senders, receivers, edges):
        del edges
        feat = jnp.concatenate((senders, receivers), axis=-1)
        return hk.nets.MLP([128, 64, 32, 16, 8, 4, 2, 1])(jax.nn.leaky_relu(hk.nets.MLP([1024, 512, 256, 256, 128, 128])(feat)))


  gn = GAT(
      attention_query_fn=_attention_query_fn2,

      attention_logit_fn=_attention_logit_fn2,
      node_update_fn=hk.nets.MLP([1024, 512, 256, 128, 64, 32, 32, 16, 8, 4, 2, 1]),
      add_self_edges=True)
  graph = gn(graph)

  def _attention_query_fn3(node_features):
        return hk.nets.MLP([4, 8, 16, 32, 64, 128, 256, 512,512, 1024])(node_features)
  
  def _attention_logit_fn3(senders, receivers, edges):
        del edges
        feat = jnp.concatenate((senders, receivers), axis=-1)
        #return hk.nets.MLP([100,50,25,10,1])(jax.nn.leaky_relu(hk.nets.MLP([1000,800,600,400,200,100])(feat)))
        return hk.nets.MLP([1024])(jax.nn.leaky_relu(hk.nets.MLP([1024, 512, 256, 256, 128, 128])(feat)))


  gn2 = GAT(
      attention_query_fn=_attention_query_fn3,
      attention_logit_fn=_attention_logit_fn3,
      node_update_fn=hk.nets.MLP([512, 256, 256, 128, 128]),
      add_self_edges=True)
  inner_graph = gn2(inner_graph)

  def _attention_query_fn4(node_features):
        return hk.nets.MLP([128, 256, 512, 1024])(node_features)
  
  def _attention_logit_fn4(senders, receivers, edges):
        del edges
        feat = jnp.concatenate((senders, receivers), axis=-1)
        #return hk.nets.MLP([100,50,25,10,1])(jax.nn.leaky_relu(hk.nets.MLP([1000,800,600,400,200,100])(feat)))
        return hk.nets.MLP([128, 64, 32, 16, 8, 4, 2, 1])(jax.nn.leaky_relu(hk.nets.MLP([1024, 512, 256, 256, 128, 128])(feat)))


  gn2 = GAT(
      attention_query_fn=_attention_query_fn4,

      attention_logit_fn=_attention_logit_fn4,
      node_update_fn=hk.nets.MLP([1024, 512, 256, 128, 64, 32, 32, 16, 8, 4, 2, 1]),
      add_self_edges=True)
  inner_graph = gn2(inner_graph)
 
  return graph, inner_graph

print('Graph definition')
graph = GetGraphs(arrays_training, batch_size)[0]
graph_truth = GetGraphs(arrays_training,batch_size)[1]

def DataLoader(arrays, arrays_inner, batch_size, *, key):
    dataset_size = arrays[0].shape[0]
    #print('n_events: ', dataset_size, ', batch_size: ', batch_size)
        
    (key,) = jrandom.split(key, 1)
    start = 0
    end = batch_size
    while end <= dataset_size:
        
        yield GetGraphs(arrays[:,start:end], batch_size), arrays_inner[:,start:end]
        start = end
        end = start + batch_size

def build_toy_graph(batch, n) -> jraph.GraphsTuple:
    """Define a four node graph, each node has a scalar as its feature."""

    # create new node features vector with 40 elements all with dimension 3 (x,y,z) = (1,1,1)
    node_features = jnp.ones((batch, n, 3))

    # define senders as a list of integer numbers between 0 and n-1 repeated batch times
    senders = jnp.tile(jnp.arange(n), (batch, 1))

    # new receivers vector with 40 elements with ordered numbers between 0 and 39
    receivers = jnp.tile(jnp.arange(n), (batch, 1))

    edges = None

    n_node = jnp.array([n]) * jnp.ones((batch, 1))
    n_edge = jnp.array([n]) * jnp.ones((batch, 1))

    # Optionally you can add `global` information, such as a graph label.
    global_context = None  # Same feature dims as nodes and edges.
    graph = jraph.GraphsTuple(
            nodes=node_features,
            edges=edges,
            senders=senders,
            receivers=receivers,
            n_node=n_node,
            n_edge=n_edge,
            globals=global_context
    )
    return graph

dummy_graph = build_toy_graph(batch_size, 40)

## functions for inner gat input building 

strip_error = 0.0005 / jnp.sqrt(12)

@jax.jit
def circle_residuals_jax(params, x, y, w, err):
    """Compute residuals for circle fitting using JAX."""
    A, D, theta = params
    err = strip_error* jnp.ones_like(x)
    P = A * (x**2 + y**2) + jnp.sqrt((1 + 4*A*D)) * (x * jnp.cos(theta) + y * jnp.sin(theta)) + D
       
    residuals = w * (2*P/(1+jnp.sqrt(1+4*A*P)))
    return jnp.sum(w*(residuals/err)**2)

@jax.jit
def get_point_at_y(x, y, w, err,y_ref):
    distance = jnp.exp(-jnp.square((y-y_ref)/0.01))
    weight = jnp.multiply(distance,w)
    x_out = jnp.average(x,weights=weight)
    y_out = jnp.average(y,weights=weight)
    return x_out,y_out

@jax.jit
def findIntersection(x1,y1,x2,y2,x3,y3,x4,y4): # 1 and 2 = first line / 3 and 4 = second line
    px= ( (x1*y2-y1*x2)*(x3-x4)-(x1-x2)*(x3*y4-y3*x4) ) / ( (x1-x2)*(y3-y4)-(y1-y2)*(x3-x4) )
    py= ( (x1*y2-y1*x2)*(y3-y4)-(y1-y2)*(x3*y4-y3*x4) ) / ( (x1-x2)*(y3-y4)-(y1-y2)*(x3-x4) )
    return px,py

@jax.jit
def findCircle(ax,ay,bx,by,cx,cy): # a = first point / b = mid sagitta point / c = end point
    x1 = (ax+bx)/2
    y1 = (ay+by)/2
    x2 = x1 - (by-ay)
    y2 = y1 + (bx-ax)
    x3 = (bx+cx)/2
    y3 = (by+cy)/2
    x4 = x3 - (cy-by)
    y4 = y3 + (cx-bx)
    cx,cy = findIntersection(x1,y1,x2,y2,x3,y3,x4,y4)
    r  = jnp.sqrt((ax-cx)**2+(ay-cy)**2)
    return cx,cy,r

@jax.jit
def pre_fit_estimate(x, y, w, err):
    points = jnp.column_stack([x,y,w,err])
    # get two extreme points
    low_x, low_y = get_point_at_y(x, y, w, err,2.5)
    hi_x, hi_y = get_point_at_y(x, y, w, err,4.5)
    # get two mid points
    mid_x1, mid_y1 = get_point_at_y(x, y, w, err,2.8)
    mid_x2, mid_y2 = get_point_at_y(x, y, w, err,4.2)
    # get individual circle estimates
    x1,y1,r1 = findCircle(low_x,low_y,mid_x1,mid_y1,hi_x,hi_y)
    x2,y2,r2 = findCircle(low_x,low_y,mid_x2,mid_y2,hi_x,hi_y)
    # get the average value
    x0 = (x1+x2)/2
    y0 = (y1+y2)/2
    r  = (r1+r2)/2
    return x0,y0,r

@jax.jit
def xyr_to_adtheta(xc, yc, r):
   a = 1 / 2*r
   theta = jnp.arctan((yc/xc)**2)
   d = r / 2 * (xc**2 / (4 * jnp.cos(theta)*r**2))

   return jnp.array([a,d,theta])

@jax.jit
def fit_circle_least_squares_jax(x, y, w, err, initial_params=None):

    
    jax.config.update('jax_enable_x64', True)
    """Fit a circle to points using JAX optimization."""
    real_pre_fit = False
    if initial_params is None:
      if real_pre_fit :
        # Real pre-fit
        x0,y0,r = pre_fit_estimate(x, y, w, err)
        initial_params = xyr_to_adtheta(x0,y0,r)
      else :
        # Use the centroid and average distance as initial parameters
        x0 = jnp.mean(x)
        y0 = jnp.mean(y)
        r = jnp.mean(jnp.sqrt((x - x0)**2 + (y - y0)**2))
        initial_params = jnp.array([1/2*r, 0, 0])

    # Define the cost function without calling it
    cost_function = lambda params: circle_residuals_jax(params, x, y, w, err)

    # Use JAX's minimize function with the L-BFGS-B optimizer
    
  
    lower_bounds = jnp.array([-jnp.inf, -jnp.inf,0])
    upper_bounds = jnp.array([jnp.inf, jnp.inf,jnp.inf])

    #solver = jaxopt.LBFGSB(fun=cost_function,implicit_diff=True)
    #scipyMin = jaxopt.ScipyMinimize(fun=cost_function, method="bfgs")
    #res = scipyMin.run(initial_params,x = x, y = y, w = w, err= err)
    solver = jaxopt.LBFGS(fun=cost_function)
    res = solver.run(initial_params)
    #res = solver.run(initial_params,bounds=(lower_bounds,upper_bounds))

    # Extract optimized parameters
    optimized_params = res.params
    return optimized_params, circle_residuals_jax(optimized_params, x, y, w, err)

@jax.jit
def final_params(r):
  A3 = r[0]
  D3 = r[1]
  theta3 = r[2]

  B3 = jnp.sqrt(jnp.absolute(1+4*A3*D3))*jnp.cos(theta3)
  C3 = jnp.sqrt(jnp.absolute(1+4*A3*D3))*jnp.sin(theta3)

  fitted_xcenter = -B3/(2*A3)
  fitted_ycenter = -C3/(2*A3)
  fitted_radius = 1/(2*jnp.abs(A3))
  return fitted_xcenter, fitted_ycenter, fitted_radius



GAT_layers = [2.495 , 2.505, 2.605, 2.705, 2.805, 4.205 , 4.305 ,4.405, 4.505]
MLP_layers = [0.405, 0.505, 0.605, 0.705, 0.805, 0.905, 1.005, 1.105]
outer_layers = [0.405, 4.505]

@jax.jit
def cartesian_to_polar_GAT(y_cluster, fitted_xcenter, fitted_ycenter):
    x = jnp.asarray(y_cluster)
    y = jnp.asarray(GAT_layers)
    dx = x - fitted_xcenter
    dy = y - fitted_ycenter

    theta = jnp.nan_to_num(jnp.arctan2(dy, dx))   
    theta_ref = jnp.arctan2(fitted_xcenter, fitted_ycenter)
    theta = jnp.where(theta < theta_ref, theta + 2*jnp.pi, theta)

    return jnp.sum(theta)/jnp.count_nonzero(theta), jnp.arctan2(dy, dx)

@jax.jit
def find_nearest_y_points(z_layer, y_layer, y_hits_layer, z_layers_hits, labels):
    num_points=5
    selected_y = []
    selected_z = []
    selected_labels = []
    selected_y_distances = []
    #print('find nan', jnp.isnan(y_layer).any(), jnp.isnan(y_hits_layer).any())
    for i in range(len(z_layer)):
 
        z = z_layer[i]
        y = y_layer[i]   

        y_layer_hits = jnp.where((z_layers_hits == z), y_hits_layer, 5)
        y_diffs = y_layer_hits - y
        abs_y_diffs = jnp.absolute(y_diffs)
        sorted_indices = jnp.argsort(abs_y_diffs)

        sorted_array = y_layer_hits[sorted_indices]
        sorted_distances = y_diffs[sorted_indices] 
        sorted_label = labels[sorted_indices]

        selected_y_values = sorted_array[:num_points]
        selected_label = sorted_label[:num_points]      
        selected_z_values = z * jnp.ones_like(selected_y_values)
        selected_y_diffs = sorted_distances[:num_points]
               
        selected_y.append(selected_y_values)
        selected_z.append(selected_z_values)
        selected_labels.append(selected_label)
        selected_y_distances.append(selected_y_diffs)  
   

    return selected_y, selected_z, selected_labels, selected_y_distances

@jax.jit
def from_fit_to_mlp_input(MLP_coordinates, MLP_labels, fitted_xcenter, fitted_ycenter, fitted_radius, y_cluster):
    theta_mean, theta = jax.vmap(cartesian_to_polar_GAT)(y_cluster, fitted_xcenter, fitted_ycenter)
    
    extrapolated_theta_MLP_layers = jnp.arcsin((jnp.asarray(MLP_layers) - fitted_ycenter[:,jnp.newaxis]) / fitted_radius[:,jnp.newaxis])
    mean_extrap_theta_MLP_layers = jnp.mean(extrapolated_theta_MLP_layers, axis=1)
    theta_outer_layers = jnp.arcsin((jnp.asarray(outer_layers) - fitted_ycenter[:,jnp.newaxis]) / fitted_radius[:,jnp.newaxis])
    delta_theta_outer_layers = jnp.absolute(theta_outer_layers[:,0] - theta_outer_layers[:,1])
    good_happy_theta_inner_layers = jnp.where((jnp.absolute(mean_extrap_theta_MLP_layers - theta_mean) < 2 * delta_theta_outer_layers)[:,jnp.newaxis], extrapolated_theta_MLP_layers, jnp.pi - extrapolated_theta_MLP_layers)
    extrapolated_y_MLP_fromtheta = fitted_xcenter[:,jnp.newaxis] + fitted_radius[:,jnp.newaxis] * jnp.cos(good_happy_theta_inner_layers)  
    
    y_mlp_input = MLP_coordinates[:,:,0]
    z_mlp_input = MLP_coordinates[:,:,1]
    MLP_layers_vec = jnp.tile(jnp.asarray(MLP_layers), (batch_size, 1))
    n_extrapolated =  jnp.nan_to_num(extrapolated_y_MLP_fromtheta)
    #print('nextr ', n_extrapolated, jnp.count_nonzero(n_extrapolated))
    '''
    if n_extrapolated == 0:
        print(count) 
    '''
    #print('pt value', 0.3*fitted_radius*1000)
    mean_y_cluster_outer = jnp.mean(y_cluster, axis=1, keepdims=True)
   
    mlp_labels = MLP_labels
    pt_value = 0.3*fitted_radius*1000
    pt_value = pt_value[:,jnp.newaxis]  
    extrapolated_hits = jnp.where(jnp.count_nonzero(n_extrapolated, axis=1, keepdims=True) == 8, 
                              extrapolated_y_MLP_fromtheta, 
                              mean_y_cluster_outer * jnp.ones(extrapolated_y_MLP_fromtheta.shape))    
   
    ymlp, zmlp, lmlp , ydiffmlp = jax.vmap(find_nearest_y_points)(MLP_layers_vec, extrapolated_hits, y_mlp_input, z_mlp_input, mlp_labels)

    return ymlp, zmlp, lmlp, extrapolated_hits, ydiffmlp


## network inizialization ##
print('Network inizialization') 

network = hk.without_apply_rng(hk.transform(hk.vmap(gat_definition, split_rng=False)))
#params = network.init(jax.random.PRNGKey(1234), graph, dummy_graph)

#params = load('/home/lrambelli/gnn-differentiable-patternreco/params_gatouter_pretrain/gnnonly_params_161_loss_0.023310047_val_loss_0.018170455.pkl')
params = load('/home/lrambelli/gnn-differentiable-patternreco/params_gatinner_pretrain/B_gnnonly_params_40_loss_0.09731606524884155_val_loss_0.11444035825595064.pkl')
opt_init, opt_update = optax.adam(lr)
opt_state = opt_init(params)

@jax.jit
def prediction_loss(params, input_graph, input_inner_graph, id_mask_inner):

        #print('nan loss 0 ', jnp.isnan(input_graph.nodes).any(), jnp.isnan(input_inner_graph.nodes).any())
        output_graph_inner = network.apply(params, input_graph, input_inner_graph)[1]
        #print('nan loss 1 ', jnp.isnan(output_graph_inner.nodes).any())

        radius_inner = jnp.reshape(output_graph_inner.nodes, (output_graph_inner.nodes.shape[0], output_graph_inner.nodes.shape[1] * output_graph_inner.nodes.shape[2]))
        loss_inner = (radius_inner - id_mask_inner)**2  
        mask_inner = input_inner_graph.nodes[:,:,:,0]
        
        mask_inner = jnp.reshape(mask_inner, (mask_inner.shape[0], mask_inner.shape[1]*mask_inner.shape[2]))   
        loss_inner = jnp.where(mask_inner/1e2 != 5, loss_inner, 0) 
        loss_inner = jnp.sum(loss_inner) / jnp.count_nonzero(loss_inner)
        return loss_inner

@jax.jit
def update(params, opt_state, gr):
        """Returns updated params and state."""
        updates, opt_state = opt_update(gr, opt_state)
        return optax.apply_updates(params, updates), opt_state

dataloader = DataLoader(arrays_training, arrays_inner_training, batch_size,  key = jrandom.PRNGKey(683))
it = itertools.tee(dataloader, steps)
gpus = jax.devices('gpu')

graph_test, graph_truth_test = GetGraphs(arrays_test, batch_size)


import time
start = time.time()
print('Training.. ')
loss_list = []
val_loss_list = []

from fromGATtoGAT import GraphCreation, ManipulateGraph, Methods4Fit

for step in range(steps):
       
        epoch_loss = 0
        epoch_val_loss = 0
        iter_data = it[step]
        count = 0
        for g in iter_data:
                
                input_graph = g[0][0]
                truth_graph = g[0][1]
                arrays_inner_gat = g[1]
                input_inner_graph = dummy_graph
               
                output_graph = network.apply(params, input_graph, input_inner_graph)[0]
               
                input_mask = input_graph.nodes[:,:,0]
                radius = jnp.reshape(output_graph.nodes, (output_graph.nodes.shape[0], output_graph.nodes.shape[1]))
                
                y_cluster_outer , z_cluster_outer, w_cluster_outer, error_cluster_outer = jax.vmap(ManipulateGraph.cluster_hits)(input_graph, output_graph)
                y_cluster_outer = jnp.array(y_cluster_outer).T
                z_cluster_outer = jnp.array(z_cluster_outer).T
                w_cluster_outer = jnp.array(w_cluster_outer).T        
   

                w_cluster_outer = jnp.where(w_cluster_outer >= 1, 1, 0)
                y_cluster_outer = jnp.nan_to_num(y_cluster_outer)

                outer_fit_params, chisq = jax.vmap(fit_circle_least_squares_jax)(y_cluster_outer, z_cluster_outer, w_cluster_outer, error_cluster_outer)
                xc_outer, yc_outer, r_outer = jax.vmap(final_params)(outer_fit_params)

           
                inner_coords, inner_labels = ManipulateGraph.inner_input_preprocessing(arrays_inner_gat, batch_size)
                y_inner, z_inner, label_inner, extrap_inner, deltay_inner = from_fit_to_mlp_input(inner_coords, inner_labels, xc_outer, yc_outer, r_outer, y_cluster_outer)


                y_inner = jnp.asarray(y_inner).transpose(1,0,2)
                y_inner = y_inner *1e2
                z_inner = jnp.asarray(z_inner).transpose(1,0,2)
                label_inner = jnp.asarray(label_inner).transpose(1,0,2)
                deltay_inner = jnp.asarray(deltay_inner).transpose(1,0,2) 


                '''
                plt.figure(figsize=(20, 8))
                plt.subplot(1, 2, 1)
                y_plot = jnp.asarray(input_graph.nodes[:,:,0]).ravel()
                z_plot = jnp.asarray(input_graph.nodes[:,:,1]).ravel()
                label_plot = jnp.asarray(truth_graph.nodes).ravel()
                y_inner_plot = jnp.asarray(y_inner).ravel()
                z_inner_plot = jnp.asarray(z_inner).ravel()
                label_inner_plot = jnp.asarray(label_inner).ravel()
                theta = np.linspace(0, 2*np.pi, 1000)
                xfit = xc_outer + r_outer * np.cos(theta)
                yfit = yc_outer + r_outer * np.sin(theta)
                z_layers = jnp.asarray(MLP_layers)
                print('extrapolated', jnp.asarray(extrap_inner).ravel())

                plt.scatter(y_plot/1e2, z_plot, c = label_plot, label = 'All outer hits', alpha = 0.5)
                plt.scatter(inner_coords[:,:,0].ravel(), inner_coords[:,:,1].ravel(), c = inner_labels.ravel(), label = 'All inner hits', alpha = 0.5)
                plt.scatter(y_cluster_outer.ravel(), z_cluster_outer.ravel(), marker = '.', color = 'r', label = 'Clustered hits')
                plt.scatter(y_inner_plot/1e2, z_inner_plot, c = 'red', marker = '*', label = 'Inner hits')
                plt.plot(xfit, yfit, label='Outer circle fit', color = 'k')
                
                plt.scatter(jnp.asarray(extrap_inner).ravel(), z_layers.ravel(), marker = 'x', color = 'k', label = 'Extrapolated inner hits')
                plt.colorbar()
                plt.ylim(0,5)
                plt.xlim(-0.5, 0.5)
                plt.legend()
                plt.subplot(1, 2, 2)
                mu = []
                bkg = []
                for i in range(len(label_plot)):
                    if label_plot[i] == 0:
                        bkg.append(np.abs(label_plot[i] - radius.ravel()[i]))
                    else:
                        mu.append(np.abs(label_plot[i] - radius.ravel()[i]))
                
                plt.hist(mu, bins = 100,range = [0,1], alpha = 0.5, label = 'muon')
                plt.hist(bkg, bins = 100, range = [0,1], alpha = 0.5, label = 'bkg')
                plt.xlabel('Outer |Predicted - Truth|')
                plt.ylabel('Entries')
                plt.tight_layout()
                plt.legend()
                plt.savefig(r'fit2_' + str(start_training + count) + '.png', dpi=600)
                '''
                #print(count , '-->', jnp.isnan(y_inner).any(), jnp.isnan(z_inner).any(), jnp.isnan(deltay_inner).any())
                #print(deltay_inner)
                inner_graph_coords = jnp.stack((y_inner, z_inner, deltay_inner), axis=-1)
                input_inner_graph = dummy_graph._replace(nodes = inner_graph_coords)
                #print(count, '--->', input_inner_graph.nodes, '==>', input_inner_graph.nodes[:,:,:,0]/1e2)
                id_mask_inner = jnp.reshape(label_inner, (label_inner.shape[0], label_inner.shape[1]*label_inner.shape[2]))

                loss = prediction_loss(params, input_graph, input_inner_graph, id_mask_inner)
                  
                gr = jax.grad(prediction_loss, argnums=0)(params, input_graph, input_inner_graph, id_mask_inner)
                epoch_loss += loss
             
                params, opt_state = update(params, opt_state, gr)

                output_graph_test = network.apply(params, graph_test, dummy_graph)[0]
                input_mask_test = graph_test.nodes[:,:,0]
                radius_test = jnp.reshape(output_graph_test.nodes, (output_graph_test.nodes.shape[0], output_graph_test.nodes.shape[1]))

                y_cluster_outer_test , z_cluster_outer_test, w_cluster_outer_test, error_cluster_outer_test = jax.vmap(ManipulateGraph.cluster_hits)(graph_test, output_graph_test)
                y_cluster_outer_test = jnp.array(y_cluster_outer_test).T
                z_cluster_outer_test = jnp.array(z_cluster_outer_test).T
                w_cluster_outer_test = jnp.array(w_cluster_outer_test).T

                w_cluster_outer_test = jnp.where(w_cluster_outer_test >= 1, 1, 0)
                y_cluster_outer_test = jnp.nan_to_num(y_cluster_outer_test)
                
                outer_fit_params_test, chisq_test = jax.vmap(fit_circle_least_squares_jax)(y_cluster_outer_test, z_cluster_outer_test, w_cluster_outer_test, error_cluster_outer_test)
                xc_outer_test, yc_outer_test, r_outer_test = jax.vmap(final_params)(outer_fit_params_test)

                inner_coords_test, inner_labels_test = ManipulateGraph.inner_input_preprocessing(arrays_inner_test, batch_size)
                y_inner_test, z_inner_test, label_inner_test, extrap_inner_test, deltay_inner_test = from_fit_to_mlp_input(inner_coords_test, inner_labels_test, xc_outer_test, yc_outer_test, r_outer_test, y_cluster_outer_test)
                y_inner_test = jnp.asarray(y_inner_test).transpose(1,0,2)
                y_inner_test = y_inner_test*1e2
                z_inner_test = jnp.asarray(z_inner_test).transpose(1,0,2)
                label_inner_test = jnp.asarray(label_inner_test).transpose(1,0,2)
                deltay_inner_test = jnp.asarray(deltay_inner_test).transpose(1,0,2)

                inner_graph_coords_test = jnp.stack((y_inner_test, z_inner_test, deltay_inner_test), axis=-1)
                input_inner_graph_test = dummy_graph._replace(nodes = inner_graph_coords_test)

                id_mask_inner_test = jnp.reshape(label_inner_test, (label_inner_test.shape[0], label_inner_test.shape[1]*label_inner_test.shape[2]))
                                                                 
                val_loss = prediction_loss(params, graph_test, input_inner_graph_test, id_mask_inner_test)
                
                
                #if loss is nan print count 
                '''
                if jnp.isnan(loss):
                    print('NAN LOSS event: ', count)
                    break
                
                print('count: ', count, ' loss: ', loss)
                '''
                epoch_val_loss += val_loss
                count += 1
        print('--------------------->  STEP: ', step, ' LOSS: ', epoch_loss/count, ' VAL_LOSS: ', epoch_val_loss/count)
        loss_list.append(epoch_loss/count)
        val_loss_list.append(epoch_val_loss/count)
        save(params, '/home/lrambelli/gnn-differentiable-patternreco/params_gatinner_pretrain/BB_gnnonly_params_'+ str(step) + '_loss_' + str(epoch_loss/count) + '_val_loss_' + str(epoch_val_loss/count) + '.pkl')


end = time.time()
training_time = end - start
print('Training time:  ', training_time)

save(params, 'params_inner_pretrained.pkl')

print('Model saved!')

plt.figure(figsize=(7, 5), dpi=500)
xrange = np.arange(0,steps,1)
plt.plot(xrange, np.asarray(loss_list), label = 'loss')
plt.plot(xrange, np.asarray(val_loss_list), label = 'val_loss')
plt.xlabel('epochs')
plt.ylabel('value')
plt.savefig('loss_gnn_inner_pretrained.png')

## post processing

'''
print('Post processing ..')

graph_test, graph_truth_test = GetGraphs(arrays_test, batch_size)
predicted_graph = network.apply(params, graph_test, dummy_graph)[0]
predicted_radius = predicted_graph.nodes

predicted_radius = jnp.reshape(predicted_radius, (predicted_radius.shape[0], predicted_radius.shape[1]))
radius = predicted_radius.ravel().tolist()
truth = graph_truth_test.nodes.flatten().tolist()

nhit = graph_truth_test.n_node
pad_index = nhit[0][0]
pad_value = radius[pad_index + 1]


mu = []
bkg = []
for i in range(len(truth)):
    
    t = truth[i]
    r = radius[i]
    
    if t ==1:
        mu.append(r)
    if t==0:
        bkg.append(r)

plot_bkg = [val for val in bkg if val != pad_value]

plt.figure(figsize=(7, 5), dpi=500)

plt.hist(plot_bkg, bins=100, range = [0,1.5], label = 'bkg', alpha = 0.5)
plt.hist(mu, bins = 100, range = [0,1.5], label = 'signal', alpha = 0.5)
plt.xlabel('Predicted Value')
plt.ylabel('Entries')
plt.legend()
#plt.savefig('pred_gnn_outer_pretrained.png')


plt.figure(figsize=(7, 5), dpi=500)

plt.hist(plot_bkg, bins=100, range = [0,1.5], label = 'bkg', alpha = 0.5)
plt.hist(mu, bins = 100, range = [0,1.5], label = 'signal', alpha = 0.5)
plt.xlabel('Predicted Value')
plt.ylabel('Entries')
plt.yscale('log')
plt.legend()
#plt.savefig('pred_gnn_log_outer_pretrained.png')
'''




