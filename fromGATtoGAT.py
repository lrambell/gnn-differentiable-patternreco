import numpy as np
import jax.numpy as jnp
import jax
import jaxopt
import jraph


class GraphCreation:
    ## functions for graph creation ##

    #function that compute the node number (hit number) for each graph (event) in the batch
    def get_node_number(arrays, batch_size):
        node_numbers = []
        x = arrays[0]
        
        for i in range(batch_size):
            
            xx = x[i]
            node_numbers.append([xx.shape[0]])
        max_value = np.max(node_numbers)
        #max_value = 384
        
        return node_numbers, max_value

    #function that returns the n_nodes and n_edges vectors required for defining the GraphTuple
    #n_nodes vector is like ([a],[b],[c]) with a,b,c nodes numbers for each graph in the batch
    #n_edges vector is like ([a**2], [b**2], [c**2]) where each element is the number of edges for the graph (fully connected)

    def get_nodes_edges_per_event(arrays, batch_size):
        data_array = GraphCreation.get_node_number(arrays, batch_size)[0]

        hits_per_event = data_array
        edges_per_event = [[nhits[0]**2] for nhits in data_array]
        return hits_per_event, edges_per_event


    #function that creates for each graoh in the batch the vectors defining senders and receivers
    #explicitly batched graph is used so all the vectors are padded to the dimension of the bigger one 

    def create_senders_receivers(arrays, batch_size):
        #nhits = get_nodes_edges_per_event(arrays, batch_size)[0]
        nhits = GraphCreation.get_nodes_edges_per_event(arrays, batch_size)[0]
        senders = []
        receivers = []
        for nhit in nhits:
            n = nhit[0]
            s = jnp.tile(np.arange(n), n).tolist()
            r = jnp.repeat(np.arange(n), n).tolist()
            
            senders.append(s)
            receivers.append(r)

        padded_senders = []
        padded_receivers = []
        #max_value = 384
        max_value = GraphCreation.get_node_number(arrays, batch_size)[1]

        for sender in senders:
            pad_s = sender + [-1] * (max_value**2 - len(sender))
            padded_senders.append(pad_s)

        for receiver in receivers:
            pad_r = receiver + [-1] * (max_value**2 - len(receiver))
            padded_receivers.append(pad_r)

        return padded_senders, padded_receivers



    #function that decorates the target nodes with only the particle id (muon == -13.)
    def decorate_nodes_truth(arrays, batch_size):
        nodes = []
        #max_value = 384
        max_value = GraphCreation.get_node_number(arrays, batch_size)[1]    
        
        for i in range (0, batch_size):        
            id = np.asarray(arrays[3][i])
            id = jnp.where(jnp.absolute(id) == 13, 1, 0)
            padded_id =  jnp.pad(id, (0, max_value - len(id)), mode='constant', constant_values=5)
            node_features = jnp.stack((padded_id), axis=-1)
            nodes.append(node_features)

        return nodes

    #function that decorates the input nodes with (y,z) coordinates
    def decorate_nodes(arrays,batch_size):
        nodes = []
        max_value = GraphCreation.get_node_number(arrays, batch_size)[1]   
        #max_value = 384 
        for i in range (0, batch_size):
            y = np.asarray(arrays[1][i])
            z = np.asarray(arrays[2][i])
            padded_y =  jnp.pad(y, (0, max_value - len(y)), mode='constant', constant_values=5)
            padded_z =  jnp.pad(z, (0, max_value - len(z)), mode='constant', constant_values=5)
        
            node_features = jnp.stack((padded_y,padded_z), axis=-1)
            nodes.append(node_features)

        return nodes

    def GetGraphs(arrays: np.ndarray, batch_size : int) -> jraph.GraphsTuple:
        graph = jraph.GraphsTuple(
            n_node=jnp.array(GraphCreation.get_nodes_edges_per_event(arrays, batch_size)[0]), 
            n_edge=jnp.array(GraphCreation.get_nodes_edges_per_event(arrays, batch_size)[1]), 
            nodes=jnp.array(GraphCreation.decorate_nodes(arrays, batch_size)), 
            edges=None, 
            globals=None,  
            senders=jnp.array(GraphCreation.create_senders_receivers(arrays, batch_size)[0]), 
            receivers=jnp.array(GraphCreation.create_senders_receivers(arrays, batch_size)[1]))
        
        graph_truth = jraph.GraphsTuple(
            n_node=jnp.array(GraphCreation.get_nodes_edges_per_event(arrays, batch_size)[0]), 
            n_edge=jnp.array(GraphCreation.get_nodes_edges_per_event(arrays, batch_size)[1]), 
            nodes=jnp.array(GraphCreation.decorate_nodes_truth(arrays, batch_size)), 
            edges=None, 
            globals=None,  
            senders=jnp.array(GraphCreation.create_senders_receivers(arrays, batch_size)[0]), 
            receivers=jnp.array(GraphCreation.create_senders_receivers(arrays, batch_size)[1]))
        
        return (graph, graph_truth)
    


class ManipulateGraph:
    def shifted_sigmoid(x):
        return 1 / (1 + jnp.exp(-100 * (x - 0.45)))
    
    def cluster_hits(input_graph, output_graph):
        coordinates = input_graph.nodes
        predictions = output_graph.nodes
        
        y_input = coordinates[:,0]
        z_input = coordinates[:,1]
        all_weights = jnp.absolute(predictions).ravel() 
        #all_weights = ManipulateGraph.shifted_sigmoid(jnp.absolute(predictions)).ravel()
        real_weights = jnp.where(y_input != 5., all_weights, 0)
        
        layers = [2.495 , 2.505, 2.605, 2.705, 2.805, 4.205 , 4.305 ,4.405, 4.505]    
        clustered_y = []
        weights_layer_sum = []
        rms_y_layers = []

        y_input = y_input/1e2
        for i in range(len(layers)):

            weights_layer = jnp.where(z_input == layers[i], real_weights, 0)    
            y = jnp.where(z_input==layers[i], y_input, 0) 
            #weights_layer = jnp.where(weights_layer == 1., 1, 0)
            
            weighted_y = weights_layer*y  

            sum_w = jnp.sum(weights_layer)        
            y_mean = jnp.sum(weighted_y) / sum_w

            clustered_y.append(y_mean)
            weights_layer_sum.append(sum_w)
            layer_rms = jnp.sqrt(jnp.sum((weights_layer*((y - y_mean)))**2) / jnp.sum(weights_layer))   
            layer_rms = jnp.where(jnp.nan_to_num(layer_rms) != 0, layer_rms, 1)
            rms_y_layers.append(layer_rms)
            
        rms_y_layers = jnp.asarray(rms_y_layers)
        return clustered_y, layers, weights_layer_sum, rms_y_layers
    
    def inner_input_preprocessing(MLP_arrays, batch_size):
        coordinates = []
        labels = []

        max = GraphCreation.get_node_number(MLP_arrays, batch_size)[1]

        for i in range (0, batch_size):
            
            y = jnp.asarray(MLP_arrays[1][i])
            z = jnp.asarray(MLP_arrays[2][i])
            id = jnp.asarray(MLP_arrays[3][i])
            id = jnp.where(jnp.absolute(id) == 13, 1, 0)

            pad_y =  jnp.pad(y, (0, max - len(y)), mode='constant')
            pad_z =  jnp.pad(z, (0, max - len(z)), mode='constant')
        
            padded_y = jnp.where(pad_y!=0, pad_y, 5)
            padded_z = jnp.where(pad_y!=0, pad_z, 5)
            padded_id =  jnp.pad(id, (0, max - len(id)), mode='constant')

            #funzioni per clustering, estrapolazione 
            input_coords = jnp.stack((padded_y,padded_z), axis=-1)
            truth_labels = jnp.stack((padded_id), axis=-1) 

            coordinates.append(input_coords)
            labels.append(truth_labels)

        return jnp.asarray(coordinates), jnp.asarray(labels)
    
    @jax.jit
    def cartesian_to_polar_GAT(y_cluster, fitted_xcenter, fitted_ycenter):
        GAT_layers = [2.495 , 2.505, 2.605, 2.705, 2.805, 4.205 , 4.305 ,4.405, 4.505]

        x = jnp.asarray(y_cluster)
        y = jnp.asarray(GAT_layers)
        dx = x - fitted_xcenter
        dy = y - fitted_ycenter

        theta = jnp.nan_to_num(jnp.arctan2(dy, dx))   
        theta_ref = jnp.arctan2(fitted_xcenter, fitted_ycenter)
        theta = jnp.where(theta < theta_ref, theta + 2*jnp.pi, theta)

        return jnp.sum(theta)/jnp.count_nonzero(theta), jnp.arctan2(dy, dx)

    @jax.jit
    def find_nearest_y_points(z_layer, y_layer, y_hits_layer, z_layers_hits, labels):
        num_points=5
        selected_y = []
        selected_z = []
        selected_labels = []
        selected_y_distances = []
        for i in range(len(z_layer)):
            z = z_layer[i]
            y = y_layer[i]   
            y_layer_hits = jnp.where((z_layers_hits == z), y_hits_layer, 5)
            y_diffs = y_layer_hits - y
            abs_y_diffs = jnp.absolute(y_diffs)
            sorted_indices = jnp.argsort(abs_y_diffs)

            sorted_array = y_layer_hits[sorted_indices]
            sorted_distances = y_diffs[sorted_indices]            
            
            sorted_label = labels[sorted_indices]

            selected_y_values = sorted_array[:num_points]
            selected_label = sorted_label[:num_points]      
            selected_z_values = z * jnp.ones_like(selected_y_values)
            selected_y_diffs = sorted_distances[:num_points]
        
            
            selected_y.append(selected_y_values)
            selected_z.append(selected_z_values)
            selected_labels.append(selected_label)
            selected_y_distances.append(selected_y_diffs)  
    

        return selected_y, selected_z, selected_labels, selected_y_distances

    @jax.jit
    def from_fit_to_mlp_input(MLP_coordinates, MLP_labels, fitted_xcenter, fitted_ycenter, fitted_radius, y_cluster):
        MLP_layers = [0.405, 0.505, 0.605, 0.705, 0.805, 0.905, 1.005, 1.105]
        outer_layers = [0.405, 4.505]
        theta_mean, theta = ManipulateGraph.cartesian_to_polar_GAT(y_cluster, fitted_xcenter, fitted_ycenter)
        extrapolated_theta_MLP_layers = jnp.arcsin((jnp.asarray(MLP_layers) - fitted_ycenter[:,jnp.newaxis]) / fitted_radius[:,jnp.newaxis])
        mean_extrap_theta_MLP_layers = jnp.mean(extrapolated_theta_MLP_layers, axis=1)
        theta_outer_layers = jnp.arcsin((jnp.asarray(outer_layers) - fitted_ycenter[:,jnp.newaxis]) / fitted_radius[:,jnp.newaxis])
        delta_theta_outer_layers = jnp.absolute(theta_outer_layers[:,0] - theta_outer_layers[:,1])
        good_happy_theta_inner_layers = jnp.where((jnp.absolute(mean_extrap_theta_MLP_layers - theta_mean) < 2 * delta_theta_outer_layers)[:,jnp.newaxis], extrapolated_theta_MLP_layers, jnp.pi - extrapolated_theta_MLP_layers)
        extrapolated_y_MLP_fromtheta = fitted_xcenter + fitted_radius * jnp.sin(good_happy_theta_inner_layers)
        # fitted_xcenter[:,jnp.newaxis] + fitted_radius[:,jnp.newaxis] * jnp.cos(good_happy_theta_inner_layers)  
        MLP_layers_vec = jnp.asarray(MLP_layers) #jnp.tile(jnp.asarray(MLP_layers), (batch_size, 1))
        y_mlp_input = MLP_coordinates[:,0]
        z_mlp_input = MLP_coordinates[:,1]
        mlp_labels = MLP_labels
    
        ymlp, zmlp, lmlp , ydiffmlp = ManipulateGraph.find_nearest_y_points(MLP_layers_vec, extrapolated_y_MLP_fromtheta.ravel(), y_mlp_input, z_mlp_input, mlp_labels)
        return ymlp, zmlp, lmlp, extrapolated_y_MLP_fromtheta, ydiffmlp


class Methods4Fit:
    
    @jax.jit
    def circle_residuals_jax(params, x, y, w, err):
        strip_error = 0.0005 / jnp.sqrt(12)
        """Compute residuals for circle fitting using JAX."""
        A, D, theta = params
        err = strip_error* jnp.ones_like(x)
        P = A * (x**2 + y**2) + jnp.sqrt((1 + 4*A*D)) * (x * jnp.cos(theta) + y * jnp.sin(theta)) + D
        
        residuals = w * (2*P/(1+jnp.sqrt(1+4*A*P)))
        return jnp.sum(w*(residuals/err)**2)
    
    @jax.jit
    def fit_circle_least_squares_jax(x, y, w, err, initial_params=None):
        strip_error = 0.0005 / jnp.sqrt(12)
        """Fit a circle to points using JAX optimization."""
    

        if initial_params is None:
            # Use the centroid and average distance as initial parameters
            x0 = jnp.mean(x)
            y0 = jnp.mean(y)
            r = jnp.mean(jnp.sqrt((x - x0)**2 + (y - y0)**2))
            a0 = 1 / 2*r
            d0 = 0
            theta0 = 0 
            initial_params = jnp.array([a0, d0 ,theta0])

        # Define the cost function without calling it
        err = strip_error* jnp.ones_like(x)
        cost_function = lambda params: Methods4Fit.circle_residuals_jax(params, x, y, w, err)

        # Use JAX's minimize function with the L-BFGS-B optimizer
        solver = jaxopt.LBFGS(fun=cost_function, verbose=False)
        res = solver.run(initial_params)

        # Extract optimized parameters
        optimized_params = res.params

        # Print the value of the cost function with optimized parameters
        #print("Value of the cost function:", circle_residuals_jax(optimized_params, x, y, w, err))
        chisq = Methods4Fit.circle_residuals_jax(optimized_params, x, y, w, err)
        return optimized_params, chisq
    
    def final_params(r):
        A3 = r[0]
        D3 = r[1]
        theta3 = r[2]

        B3 = jnp.sqrt(jnp.absolute(1+4*A3*D3))*jnp.cos(theta3)
        C3 = jnp.sqrt(jnp.absolute(1+4*A3*D3))*jnp.sin(theta3)

        fitted_xcenter = -B3/(2*A3)
        fitted_ycenter = -C3/(2*A3)
        fitted_radius = 1/(2*jnp.abs(A3))
        return fitted_xcenter, fitted_ycenter, fitted_radius



            
